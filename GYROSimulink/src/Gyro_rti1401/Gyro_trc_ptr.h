/*********************** dSPACE target specific file *************************

   Header file Gyro_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:47:11 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_Gyro_trc_ptr_h_
#define RTI_HEADER_Gyro_trc_ptr_h_

/* Include the model header file. */
#include "Gyro.h"
#include "Gyro_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile InputBus *p_0_Gyro_InputBus_0;
EXTERN_C volatile BBSdataBus *p_0_Gyro_BBSdataBus_1;
EXTERN_C volatile GyroBoxBus *p_0_Gyro_GyroBoxBus_2;
EXTERN_C volatile ABSdataBus *p_0_Gyro_ABSdataBus_3;
EXTERN_C volatile IMUdataBus *p_0_Gyro_IMUdataBus_4;
EXTERN_C volatile DSBdataBus *p_0_Gyro_DSBdataBus_5;
EXTERN_C volatile real_T *p_0_Gyro_real_T_6;
EXTERN_C volatile real32_T *p_0_Gyro_real32_T_7;
EXTERN_C volatile uint16_T *p_0_Gyro_uint16_T_8;
EXTERN_C volatile uint8_T *p_0_Gyro_uint8_T_9;
EXTERN_C volatile boolean_T *p_0_Gyro_boolean_T_10;
EXTERN_C volatile real_T *p_0_Gyro_real_T_11;
EXTERN_C volatile real_T *p_0_Gyro_real_T_12;
EXTERN_C volatile real_T *p_0_Gyro_real_T_13;
EXTERN_C volatile real_T *p_0_Gyro_real_T_14;
EXTERN_C volatile real_T *p_1_Gyro_real_T_0;
EXTERN_C volatile int32_T *p_2_Gyro_int32_T_0;
EXTERN_C volatile uint32_T *p_2_Gyro_uint32_T_1;
EXTERN_C volatile int_T *p_2_Gyro_int_T_2;
EXTERN_C volatile uint8_T *p_2_Gyro_uint8_T_3;
EXTERN_C volatile uint8_T *p_2_Gyro_uint8_T_4;
EXTERN_C volatile uint8_T *p_2_Gyro_uint8_T_5;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void Gyro_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_Gyro_trc_ptr_h_ */
