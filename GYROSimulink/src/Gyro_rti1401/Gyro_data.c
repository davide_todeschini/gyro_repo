/*
 * Gyro_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Gyro".
 *
 * Model version              : 1.805
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:47:11 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Gyro.h"
#include "Gyro_private.h"

/* Model block global parameters (default storage) */
real_T rtP_Debounce = 0.0;             /* Variable: Debounce
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_DebounceMaster = 1000.0;    /* Variable: DebounceMaster
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFHighSlope1 = 30.0;        /* Variable: FFHighSlope1
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFHighSlope2 = 30.0;        /* Variable: FFHighSlope2
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFLowerSlope1 = 10.0;       /* Variable: FFLowerSlope1
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFLowerSlope2 = 10.0;       /* Variable: FFLowerSlope2
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFOffset1_minus = -5.0;     /* Variable: FFOffset1_minus
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFOffset1_plus = 5.0;       /* Variable: FFOffset1_plus
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFOffset2_minus = -5.0;     /* Variable: FFOffset2_minus
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFOffset2_plus = 5.0;       /* Variable: FFOffset2_plus
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_FFScale = 0.6;              /* Variable: FFScale
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_GainSchedGain = 1.0;        /* Variable: GainSchedGain
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_GainSchedMax = 1.2;         /* Variable: GainSchedMax
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_GainSchedMin = 0.3;         /* Variable: GainSchedMin
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_GainSched_offset = 0.4;     /* Variable: GainSched_offset
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_K_LQR61[6] = { -300.0, -200.0, 1.5, 2.0, -1.0, -2.0 } ;/* Variable: K_LQR61
                                                                   * Referenced by: '<Root>/Main'
                                                                   */

real_T rtP_K_LQR62[6] = { -300.0, -200.0, 1.5, 2.0, 1.0, 2.0 } ;/* Variable: K_LQR62
                                                                 * Referenced by: '<Root>/Main'
                                                                 */

real_T rtP_Ki_speed_PI1 = 120.0;       /* Variable: Ki_speed_PI1
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_Ki_speed_PI2 = 168.0;       /* Variable: Ki_speed_PI2
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_Kp_speed_PI1 = 3.0;         /* Variable: Kp_speed_PI1
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_Kp_speed_PI2 = 4.0;         /* Variable: Kp_speed_PI2
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_LP20_den[3] = { 1.0, -1.8244176462222566, 0.8388797647220787 } ;/* Variable: LP20_den
                                                                      * Referenced by: '<Root>/Main'
                                                                      */

real_T rtP_LP20_num[3] = { 0.0036155296249555454, 0.0072310592499110907,
  0.0036155296249555454 } ;            /* Variable: LP20_num
                                        * Referenced by: '<Root>/Main'
                                        */

real_T rtP_RampTime = 2.0;             /* Variable: RampTime
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_SpeedThreshold = 5.0;       /* Variable: SpeedThreshold
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_T_log1 = 0.01;              /* Variable: T_log1
                                        * Referenced by: '<S4>/TASK_LOG_GENERATOR'
                                        */
real_T rtP_T_log2 = 0.0;               /* Variable: T_log2
                                        * Referenced by: '<S4>/TASK_LOG_GENERATOR'
                                        */
real_T rtP_Ti_position = 2.0;          /* Variable: Ti_position
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_Ts = 0.001;                 /* Variable: Ts
                                        * Referenced by:
                                        *   '<Root>/Main'
                                        *   '<S4>/TASK_LOG_GENERATOR'
                                        *   '<S28>/F_TASK_GENERATOR'
                                        */
real_T rtP_alpha_IMU = 47.0;           /* Variable: alpha_IMU
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_angle_limit_speed = 0.6;    /* Variable: angle_limit_speed
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_angle_max = 0.65;           /* Variable: angle_max
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_den_der_Tilt[3] = { 1.0, -1.8847907885340196, 0.89318038173410463 } ;/* Variable: den_der_Tilt
                                                                      * Referenced by: '<Root>/Main'
                                                                      */

real_T rtP_f_notch = 6.6;              /* Variable: f_notch
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_kp_position = 1.0;          /* Variable: kp_position
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_max_speed = 1.6;            /* Variable: max_speed
                                        * Referenced by: '<Root>/Main'
                                        */
real_T rtP_num_der_Tilt[3] = { 4.194796600042535, 0.0, -4.194796600042535 } ;/* Variable: num_der_Tilt
                                                                      * Referenced by: '<Root>/Main'
                                                                      */

real_T rtP_xi_notch = 0.7;             /* Variable: xi_notch
                                        * Referenced by: '<Root>/Main'
                                        */

/* Block parameters (default storage) */
P_Gyro_T Gyro_P = {
  /* Expression: 1/0.0625
   * Referenced by: '<S29>/Gain1'
   */
  16.0,

  /* Expression: 255
   * Referenced by: '<S29>/Constant'
   */
  255.0,

  /* Expression: 1/0.0625
   * Referenced by: '<S29>/Gain'
   */
  16.0,

  /* Expression: 0
   * Referenced by: '<S57>/Constant'
   */
  0.0
};
