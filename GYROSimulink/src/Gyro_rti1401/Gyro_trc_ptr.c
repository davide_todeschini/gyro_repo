/***************************************************************************

   Source file Gyro_trc_ptr.c:

   Definition of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:47:11 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/

/* Include header file. */
#include "Gyro_trc_ptr.h"
#include "Gyro.h"
#include "Gyro_private.h"

/* Compiler options to turn off optimization. */
#if !defined(DS_OPTIMIZE_INIT_TRC_POINTERS)
#ifdef _MCCPPC

#pragma options -nOt -nOr -nOi -nOx

#endif

#ifdef __GNUC__

#pragma GCC optimize ("O0")

#endif

#ifdef _MSC_VER

#pragma optimize ("", off)

#endif
#endif

/* Definition of Global pointers to data type transitions (for TRC-file access) */
volatile InputBus *p_0_Gyro_InputBus_0 = NULL;
volatile BBSdataBus *p_0_Gyro_BBSdataBus_1 = NULL;
volatile GyroBoxBus *p_0_Gyro_GyroBoxBus_2 = NULL;
volatile ABSdataBus *p_0_Gyro_ABSdataBus_3 = NULL;
volatile IMUdataBus *p_0_Gyro_IMUdataBus_4 = NULL;
volatile DSBdataBus *p_0_Gyro_DSBdataBus_5 = NULL;
volatile real_T *p_0_Gyro_real_T_6 = NULL;
volatile real32_T *p_0_Gyro_real32_T_7 = NULL;
volatile uint16_T *p_0_Gyro_uint16_T_8 = NULL;
volatile uint8_T *p_0_Gyro_uint8_T_9 = NULL;
volatile boolean_T *p_0_Gyro_boolean_T_10 = NULL;
volatile real_T *p_0_Gyro_real_T_11 = NULL;
volatile real_T *p_0_Gyro_real_T_12 = NULL;
volatile real_T *p_0_Gyro_real_T_13 = NULL;
volatile real_T *p_0_Gyro_real_T_14 = NULL;
volatile real_T *p_1_Gyro_real_T_0 = NULL;
volatile int32_T *p_2_Gyro_int32_T_0 = NULL;
volatile uint32_T *p_2_Gyro_uint32_T_1 = NULL;
volatile int_T *p_2_Gyro_int_T_2 = NULL;
volatile uint8_T *p_2_Gyro_uint8_T_3 = NULL;
volatile uint8_T *p_2_Gyro_uint8_T_4 = NULL;
volatile uint8_T *p_2_Gyro_uint8_T_5 = NULL;

/*
 *  Declare the functions, that initially assign TRC pointers
 */
static void rti_init_trc_pointers_0(void);

/* Global pointers to data type transitions are separated in different functions to avoid overloading */
static void rti_init_trc_pointers_0(void)
{
  p_0_Gyro_InputBus_0 = &Gyro_B.InputBus_d;
  p_0_Gyro_BBSdataBus_1 = &Gyro_B.BBSdataBus_n;
  p_0_Gyro_GyroBoxBus_2 = &Gyro_B.GyroBoxBus_b;
  p_0_Gyro_ABSdataBus_3 = &Gyro_B.ABSdataBus_i;
  p_0_Gyro_IMUdataBus_4 = &Gyro_B.IMUdataBus_m;
  p_0_Gyro_DSBdataBus_5 = &Gyro_B.DSBdataBus_i;
  p_0_Gyro_real_T_6 = &Gyro_B.SFunction1_o1;
  p_0_Gyro_real32_T_7 = &Gyro_B.DataTypeConversion_g;
  p_0_Gyro_uint16_T_8 = &Gyro_B.DataTypeConversion2_a;
  p_0_Gyro_uint8_T_9 = &Gyro_B.DataTypeConversion24_n;
  p_0_Gyro_boolean_T_10 = &Gyro_B.SFunction1_o1_am;
  p_0_Gyro_real_T_11 = &Gyro_B.sf_Counter_k.Count;
  p_0_Gyro_real_T_12 = &Gyro_B.sf_MATLABFunction_l.crc;
  p_0_Gyro_real_T_13 = &Gyro_B.sf_Counter.Count;
  p_0_Gyro_real_T_14 = &Gyro_B.sf_MATLABFunction.crc;
  p_1_Gyro_real_T_0 = &Gyro_P.Gain1_Gain;
  p_2_Gyro_int32_T_0 = &Gyro_DW.sfEvent;
  p_2_Gyro_uint32_T_1 = &Gyro_DW.temporalCounter_i1;
  p_2_Gyro_int_T_2 = &Gyro_DW.SFunction1_IWORK[0];
  p_2_Gyro_uint8_T_3 = &Gyro_DW.is_active_c4_Gyro;
  p_2_Gyro_uint8_T_4 = &Gyro_DW.sf_Counter_k.is_active_c6_Gyro;
  p_2_Gyro_uint8_T_5 = &Gyro_DW.sf_Counter.is_active_c6_Gyro;
}

void Gyro_rti_init_trc_pointers(void)
{
  rti_init_trc_pointers_0();
}
