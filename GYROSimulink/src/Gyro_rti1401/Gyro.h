/*
 * Gyro.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Gyro".
 *
 * Model version              : 1.805
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:47:11 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Gyro_h_
#define RTW_HEADER_Gyro_h_
#include <math.h>
#include <string.h>
#ifndef Gyro_COMMON_INCLUDES_
# define Gyro_COMMON_INCLUDES_
#include <brtenv.h>
#include <rtkernel.h>
#include <rti_assert.h>
#include <rtidefineddatatypes.h>
#include <dsflrecusb.h>
#include <rti_msg_access.h>
#include <rtiusbflightrec_msg.h>
#include <rti_sim_engine_exp.h>
#include <rtican_ds1401.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* Gyro_COMMON_INCLUDES_ */

#include "Gyro_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#define GyroMain_MDLREF_HIDE_CHILD_
#include "GyroMain.h"
#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_zcfcn.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetErrorStatusPointer
# define rtmGetErrorStatusPointer(rtm) ((const char_T **)(&((rtm)->errorStatus)))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

/* Block signals for system '<S32>/MATLAB Function' */
typedef struct {
  real_T crc;                          /* '<S32>/MATLAB Function' */
} B_MATLABFunction_Gyro_T;

/* Block signals for system '<S29>/Counter' */
typedef struct {
  real_T Count;                        /* '<S29>/Counter' */
} B_Counter_Gyro_T;

/* Block states (default storage) for system '<S29>/Counter' */
typedef struct {
  uint8_T is_active_c6_Gyro;           /* '<S29>/Counter' */
} DW_Counter_Gyro_T;

/* Block signals (default storage) */
typedef struct {
  InputBus InputBus_d;                 /* '<S1>/Bus Creator10' */
  BBSdataBus BBSdataBus_n;             /* '<S7>/Bus Creator' */
  GyroBoxBus GyroBoxBus_b;             /* '<S9>/Bus Creator' */
  ABSdataBus ABSdataBus_i;             /* '<S6>/Bus Creator1' */
  IMUdataBus IMUdataBus_m;             /* '<S10>/Bus Creator4' */
  DSBdataBus DSBdataBus_i;             /* '<S8>/Bus Creator' */
  real_T SFunction1_o1;                /* '<S19>/S-Function1' */
  real_T SFunction1_o2;                /* '<S19>/S-Function1' */
  real_T SFunction1_o3;                /* '<S19>/S-Function1' */
  real_T SFunction1_o4;                /* '<S19>/S-Function1' */
  real_T SFunction1_o5;                /* '<S19>/S-Function1' */
  real_T SFunction1_o6;                /* '<S19>/S-Function1' */
  real_T SFunction1_o7;                /* '<S19>/S-Function1' */
  real_T SFunction1_o8;                /* '<S19>/S-Function1' */
  real_T SFunction1_o9;                /* '<S19>/S-Function1' */
  real_T SFunction1_o10;               /* '<S19>/S-Function1' */
  real_T SFunction1_o1_a;              /* '<S21>/S-Function1' */
  real_T SFunction1_o2_n;              /* '<S21>/S-Function1' */
  real_T SFunction1_o3_h;              /* '<S21>/S-Function1' */
  real_T SFunction1_o4_g;              /* '<S21>/S-Function1' */
  real_T SFunction1_o1_b;              /* '<S20>/S-Function1' */
  real_T SFunction1_o2_h;              /* '<S20>/S-Function1' */
  real_T SFunction1_o3_n;              /* '<S20>/S-Function1' */
  real_T SFunction1_o4_e;              /* '<S20>/S-Function1' */
  real_T SFunction1_o1_p;              /* '<S22>/S-Function1' */
  real_T SFunction1_o2_n4;             /* '<S22>/S-Function1' */
  real_T SFunction1_o3_m;              /* '<S22>/S-Function1' */
  real_T SFunction1_o4_m;              /* '<S22>/S-Function1' */
  real_T SFunction1_o1_pi;             /* '<S23>/S-Function1' */
  real_T SFunction1_o2_a;              /* '<S23>/S-Function1' */
  real_T SFunction1_o3_e;              /* '<S23>/S-Function1' */
  real_T SFunction1_o4_a;              /* '<S23>/S-Function1' */
  real_T SFunction1_o5_b;              /* '<S23>/S-Function1' */
  real_T SFunction1_o6_e;              /* '<S23>/S-Function1' */
  real_T SFunction1_o7_a;              /* '<S23>/S-Function1' */
  real_T SFunction1_o1_l;              /* '<S24>/S-Function1' */
  real_T SFunction1_o2_g;              /* '<S24>/S-Function1' */
  real_T SFunction1_o3_ed;             /* '<S24>/S-Function1' */
  real_T SFunction1_o4_o;              /* '<S24>/S-Function1' */
  real_T SFunction1_o5_j;              /* '<S24>/S-Function1' */
  real_T SFunction1_o6_a;              /* '<S24>/S-Function1' */
  real_T SFunction1_o7_j;              /* '<S24>/S-Function1' */
  real_T SFunction1_o8_j;              /* '<S24>/S-Function1' */
  real_T SFunction1_o1_h;              /* '<S25>/S-Function1' */
  real_T SFunction1_o2_f;              /* '<S25>/S-Function1' */
  real_T SFunction1_o3_o;              /* '<S25>/S-Function1' */
  real_T SFunction1_o4_f;              /* '<S25>/S-Function1' */
  real_T SFunction1_o1_n;              /* '<S11>/S-Function1' */
  real_T SFunction1_o2_g4;             /* '<S11>/S-Function1' */
  real_T SFunction1_o3_i;              /* '<S11>/S-Function1' */
  real_T SFunction1_o4_p;              /* '<S11>/S-Function1' */
  real_T SFunction1_o5_a;              /* '<S11>/S-Function1' */
  real_T SFunction1_o6_b;              /* '<S11>/S-Function1' */
  real_T SFunction1_o7_k;              /* '<S11>/S-Function1' */
  real_T SFunction1_o8_m;              /* '<S11>/S-Function1' */
  real_T SFunction1_o1_k;              /* '<S12>/S-Function1' */
  real_T SFunction1_o2_l;              /* '<S12>/S-Function1' */
  real_T SFunction1_o3_l;              /* '<S12>/S-Function1' */
  real_T SFunction1_o4_c;              /* '<S12>/S-Function1' */
  real_T SFunction1_o5_b0;             /* '<S12>/S-Function1' */
  real_T SFunction1_o6_al;             /* '<S12>/S-Function1' */
  real_T SFunction1_o7_h;              /* '<S12>/S-Function1' */
  real_T SFunction1_o8_mz;             /* '<S12>/S-Function1' */
  real_T SFunction1_o9_a;              /* '<S12>/S-Function1' */
  real_T SFunction1_o10_o;             /* '<S12>/S-Function1' */
  real_T SFunction1_o11;               /* '<S12>/S-Function1' */
  real_T SFunction1_o1_l0;             /* '<S13>/S-Function1' */
  real_T SFunction1_o2_i;              /* '<S13>/S-Function1' */
  real_T SFunction1_o3_a;              /* '<S13>/S-Function1' */
  real_T SFunction1_o4_k;              /* '<S13>/S-Function1' */
  real_T SFunction1_o5_bp;             /* '<S13>/S-Function1' */
  real_T SFunction1_o6_p;              /* '<S13>/S-Function1' */
  real_T SFunction1_o7_o;              /* '<S13>/S-Function1' */
  real_T SFunction1_o8_b;              /* '<S13>/S-Function1' */
  real_T SFunction1_o9_e;              /* '<S13>/S-Function1' */
  real_T SFunction1_o1_c;              /* '<S16>/S-Function1' */
  real_T SFunction1_o2_gu;             /* '<S16>/S-Function1' */
  real_T SFunction1_o3_ii;             /* '<S16>/S-Function1' */
  real_T SFunction1_o4_cv;             /* '<S16>/S-Function1' */
  real_T SFunction1_o5_c;              /* '<S16>/S-Function1' */
  real_T SFunction1_o6_n;              /* '<S16>/S-Function1' */
  real_T SFunction1_o7_b;              /* '<S16>/S-Function1' */
  real_T SFunction1_o8_ja;             /* '<S16>/S-Function1' */
  real_T SFunction1_o9_i;              /* '<S16>/S-Function1' */
  real_T SFunction1_o10_j;             /* '<S16>/S-Function1' */
  real_T SFunction1_o11_c;             /* '<S16>/S-Function1' */
  real_T SFunction1_o12;               /* '<S16>/S-Function1' */
  real_T SFunction1_o13;               /* '<S16>/S-Function1' */
  real_T SFunction1_o14;               /* '<S16>/S-Function1' */
  real_T SFunction1_o15;               /* '<S16>/S-Function1' */
  real_T SFunction1_o16;               /* '<S16>/S-Function1' */
  real_T SFunction1_o17;               /* '<S16>/S-Function1' */
  real_T SFunction1_o18;               /* '<S16>/S-Function1' */
  real_T SFunction1_o19;               /* '<S16>/S-Function1' */
  real_T SFunction1_o20;               /* '<S16>/S-Function1' */
  real_T SFunction1_o21;               /* '<S16>/S-Function1' */
  real_T SFunction1_o22;               /* '<S16>/S-Function1' */
  real_T SFunction1_o23;               /* '<S16>/S-Function1' */
  real_T SFunction1_o24;               /* '<S16>/S-Function1' */
  real_T SFunction1_o25;               /* '<S16>/S-Function1' */
  real_T SFunction1_o26;               /* '<S16>/S-Function1' */
  real_T SFunction1_o27;               /* '<S16>/S-Function1' */
  real_T SFunction1_o28;               /* '<S16>/S-Function1' */
  real_T SFunction1_o1_g;              /* '<S14>/S-Function1' */
  real_T SFunction1_o2_e;              /* '<S14>/S-Function1' */
  real_T SFunction1_o3_k;              /* '<S14>/S-Function1' */
  real_T SFunction1_o4_i;              /* '<S14>/S-Function1' */
  real_T SFunction1_o5_o;              /* '<S14>/S-Function1' */
  real_T SFunction1_o6_m;              /* '<S14>/S-Function1' */
  real_T SFunction1_o7_au;             /* '<S14>/S-Function1' */
  real_T SFunction1_o8_b0;             /* '<S14>/S-Function1' */
  real_T SFunction1_o9_d;              /* '<S14>/S-Function1' */
  real_T SFunction1_o10_i;             /* '<S14>/S-Function1' */
  real_T SFunction1_o11_o;             /* '<S14>/S-Function1' */
  real_T SFunction1_o12_n;             /* '<S14>/S-Function1' */
  real_T SFunction1_o13_p;             /* '<S14>/S-Function1' */
  real_T SFunction1_o14_c;             /* '<S14>/S-Function1' */
  real_T SFunction1_o15_h;             /* '<S14>/S-Function1' */
  real_T SFunction1_o1_cv;             /* '<S15>/S-Function1' */
  real_T SFunction1_o2_k;              /* '<S15>/S-Function1' */
  real_T SFunction1_o3_h5;             /* '<S15>/S-Function1' */
  real_T SFunction1_o4_ge;             /* '<S15>/S-Function1' */
  real_T SFunction1_o5_i;              /* '<S15>/S-Function1' */
  real_T SFunction1_o6_f;              /* '<S15>/S-Function1' */
  real_T SFunction1_o7_p;              /* '<S15>/S-Function1' */
  real_T SFunction1_o8_a;              /* '<S15>/S-Function1' */
  real_T SFunction1_o9_p;              /* '<S15>/S-Function1' */
  real_T GYRO_DriverStatus;            /* '<Root>/Main' */
  real_T GYRO_TempMot2;                /* '<Root>/Main' */
  real_T GYRO_TempMot1;                /* '<Root>/Main' */
  real_T GYRO_DriverErr1;              /* '<Root>/Main' */
  real_T GYRO_DriverErr2;              /* '<Root>/Main' */
  real_T GYRO_MotorTempErr1;           /* '<Root>/Main' */
  real_T GYRO_MotorTempErr2;           /* '<Root>/Main' */
  real_T GYRO_MotorVoltErr1;           /* '<Root>/Main' */
  real_T GYRO_MotorVoltErr2;           /* '<Root>/Main' */
  real_T GYRO_ValveSpeed2;             /* '<Root>/Main' */
  real_T GYRO_ValveSpeed1;             /* '<Root>/Main' */
  real_T GYRO_ValvePos2;               /* '<Root>/Main' */
  real_T GYRO_ValvePos1;               /* '<Root>/Main' */
  real_T GYRO_Current2;                /* '<Root>/Main' */
  real_T GYRO_Current1;                /* '<Root>/Main' */
  real_T GYRO_SpinSpeed2;              /* '<Root>/Main' */
  real_T GYRO_SpinSpeed1;              /* '<Root>/Main' */
  real_T GYRO_PistonErr;               /* '<Root>/Main' */
  real_T GYRO_ValvePerc2;              /* '<Root>/Main' */
  real_T GYRO_ValvePerc1;              /* '<Root>/Main' */
  real_T GYRO_PistonPress;             /* '<Root>/Main' */
  real_T IMU_YawRate;                  /* '<Root>/Main' */
  real_T IMU_Ay;                       /* '<Root>/Main' */
  real_T IMU_RollRate;                 /* '<Root>/Main' */
  real_T IMU_Ax;                       /* '<Root>/Main' */
  real_T IMU_Az;                       /* '<Root>/Main' */
  real_T ABS_SPDFrontTime;             /* '<Root>/Main' */
  real_T ABS_SPDRearTime;              /* '<Root>/Main' */
  real_T ABS_BRKFrontPressure;         /* '<Root>/Main' */
  real_T ABS_BRKRearPressure;          /* '<Root>/Main' */
  real_T ABS_BRKMasterFrontPressure;   /* '<Root>/Main' */
  real_T ABS_BRKMasterRearPressure;    /* '<Root>/Main' */
  real_T ABS_Ax;                       /* '<Root>/Main' */
  real_T ABS_Roll;                     /* '<Root>/Main' */
  real_T ABS_Pitch;                    /* '<Root>/Main' */
  real_T ABS_PitchRate;                /* '<Root>/Main' */
  real_T DSB_DTCLevel;                 /* '<Root>/Main' */
  real_T DSB_Flash;                    /* '<Root>/Main' */
  real_T DSB_RidingMode;               /* '<Root>/Main' */
  real_T DSB_But_IndRes;               /* '<Root>/Main' */
  real_T BBS_Data01_CRC;               /* '<Root>/Main' */
  real_T BBS_Data01_CNT;               /* '<Root>/Main' */
  real_T BBS_ExvlPot_ERR2;             /* '<Root>/Main' */
  real_T BBS_ExvlPot_ERR1;             /* '<Root>/Main' */
  real_T BBS_DAVC_STAT;                /* '<Root>/Main' */
  real_T BBS_DWC_STAT;                 /* '<Root>/Main' */
  real_T BBS_DWCLevACK;                /* '<Root>/Main' */
  real_T BBS_DTC_STAT;                 /* '<Root>/Main' */
  real_T BBS_DTCLevACK;                /* '<Root>/Main' */
  real_T BBS_RSpeed;                   /* '<Root>/Main' */
  real_T BBS_RSpeedSTAT;               /* '<Root>/Main' */
  real_T BBS_ExvlCheck;                /* '<Root>/Main' */
  real_T BBS_ExvlMot_ERR;              /* '<Root>/Main' */
  real_T BBS_FSpeedSTAT;               /* '<Root>/Main' */
  real_T BBS_FSpeed;                   /* '<Root>/Main' */
  real_T BBS_Data02_CRC;               /* '<Root>/Main' */
  real_T BBS_Data02_CNT;               /* '<Root>/Main' */
  real_T BBS_TorqReqRel;               /* '<Root>/Main' */
  real_T BBS_TorqReqSTAT;              /* '<Root>/Main' */
  real_T BBS_DTCwork;                  /* '<Root>/Main' */
  real_T BBS_TorqRedFast;              /* '<Root>/Main' */
  real_T BBS_TorqRedSlow;              /* '<Root>/Main' */
  real_T BBS_TorqReqFast;              /* '<Root>/Main' */
  real_T BBS_TorqReqSlow;              /* '<Root>/Main' */
  real_T SpinSpeedRef;                 /* '<Root>/Main' */
  real_T SpinCurrentRef;               /* '<Root>/Main' */
  real_T SpinStartStop;                /* '<Root>/Main' */
  real_T SpinModCont;                  /* '<Root>/Main' */
  real_T TiltModCont;                  /* '<Root>/Main' */
  real_T TiltStartStop;                /* '<Root>/Main' */
  real_T TiltValvePerc1;               /* '<Root>/Main' */
  real_T TiltValvePerc2;               /* '<Root>/Main' */
  real_T TiltPosRef;                   /* '<Root>/Main' */
  real_T TiltSpeedRef;                 /* '<Root>/Main' */
  real_T ComandoPompa;                 /* '<Root>/Main' */
  real_T TiltPosPerc;                  /* '<Root>/Main' */
  real_T DTCwork_Status;               /* '<Root>/Main' */
  real_T GYRO_DriverStatus_g;          /* '<Root>/Main' */
  real_T GYRO_TempMot2_m;              /* '<Root>/Main' */
  real_T GYRO_TempMot1_e;              /* '<Root>/Main' */
  real_T GYRO_DriverErr1_e;            /* '<Root>/Main' */
  real_T GYRO_DriverErr2_l;            /* '<Root>/Main' */
  real_T GYRO_MotorTempErr1_i;         /* '<Root>/Main' */
  real_T GYRO_MotorTempErr2_k;         /* '<Root>/Main' */
  real_T GYRO_MotorVoltErr1_c;         /* '<Root>/Main' */
  real_T GYRO_MotorVoltErr2_n;         /* '<Root>/Main' */
  real_T GYRO_ValveSpeed2_p;           /* '<Root>/Main' */
  real_T GYRO_ValveSpeed1_b;           /* '<Root>/Main' */
  real_T GYRO_ValvePos2_o;             /* '<Root>/Main' */
  real_T GYRO_ValvePos1_p;             /* '<Root>/Main' */
  real_T GYRO_Current2_k;              /* '<Root>/Main' */
  real_T GYRO_Current1_f;              /* '<Root>/Main' */
  real_T GYRO_SpinSpeed2_j;            /* '<Root>/Main' */
  real_T GYRO_SpinSpeed1_o;            /* '<Root>/Main' */
  real_T GYRO_PistonErr_d;             /* '<Root>/Main' */
  real_T GYRO_ValvePerc2_i;            /* '<Root>/Main' */
  real_T GYRO_ValvePerc1_l;            /* '<Root>/Main' */
  real_T GYRO_PistonPress_f;           /* '<Root>/Main' */
  real_T IMU_YawRate_b;                /* '<Root>/Main' */
  real_T IMU_Ay_k;                     /* '<Root>/Main' */
  real_T IMU_RollRate_e;               /* '<Root>/Main' */
  real_T IMU_Ax_b;                     /* '<Root>/Main' */
  real_T IMU_Az_k;                     /* '<Root>/Main' */
  real_T ABS_SPDFrontTime_e;           /* '<Root>/Main' */
  real_T ABS_SPDRearTime_n;            /* '<Root>/Main' */
  real_T ABS_BRKFrontPressure_g;       /* '<Root>/Main' */
  real_T ABS_BRKRearPressure_f;        /* '<Root>/Main' */
  real_T ABS_BRKMasterFrontPressure_p; /* '<Root>/Main' */
  real_T ABS_BRKMasterRearPressure_e;  /* '<Root>/Main' */
  real_T ABS_Ax_l;                     /* '<Root>/Main' */
  real_T ABS_Roll_a;                   /* '<Root>/Main' */
  real_T ABS_Pitch_p;                  /* '<Root>/Main' */
  real_T ABS_PitchRate_n;              /* '<Root>/Main' */
  real_T DSB_DTCLevel_a;               /* '<Root>/Main' */
  real_T DSB_Flash_c;                  /* '<Root>/Main' */
  real_T DSB_RidingMode_k;             /* '<Root>/Main' */
  real_T DSB_But_IndRes_l;             /* '<Root>/Main' */
  real_T BBS_Data01_CRC_l;             /* '<Root>/Main' */
  real_T BBS_Data01_CNT_o;             /* '<Root>/Main' */
  real_T BBS_ExvlPot_ERR2_e;           /* '<Root>/Main' */
  real_T BBS_ExvlPot_ERR1_m;           /* '<Root>/Main' */
  real_T BBS_DAVC_STAT_d;              /* '<Root>/Main' */
  real_T BBS_DWC_STAT_n;               /* '<Root>/Main' */
  real_T BBS_DWCLevACK_k;              /* '<Root>/Main' */
  real_T BBS_DTC_STAT_p;               /* '<Root>/Main' */
  real_T BBS_DTCLevACK_o;              /* '<Root>/Main' */
  real_T BBS_RSpeed_p;                 /* '<Root>/Main' */
  real_T BBS_RSpeedSTAT_f;             /* '<Root>/Main' */
  real_T BBS_ExvlCheck_c;              /* '<Root>/Main' */
  real_T BBS_ExvlMot_ERR_i;            /* '<Root>/Main' */
  real_T BBS_FSpeedSTAT_i;             /* '<Root>/Main' */
  real_T BBS_FSpeed_l;                 /* '<Root>/Main' */
  real_T BBS_Data02_CRC_p;             /* '<Root>/Main' */
  real_T BBS_Data02_CNT_n;             /* '<Root>/Main' */
  real_T BBS_TorqReqRel_a;             /* '<Root>/Main' */
  real_T BBS_TorqReqSTAT_k;            /* '<Root>/Main' */
  real_T BBS_DTCwork_h;                /* '<Root>/Main' */
  real_T BBS_TorqRedFast_b;            /* '<Root>/Main' */
  real_T BBS_TorqRedSlow_c;            /* '<Root>/Main' */
  real_T BBS_TorqReqFast_e;            /* '<Root>/Main' */
  real_T BBS_TorqReqSlow_h;            /* '<Root>/Main' */
  real_T DBG_SpinSpeedRef;             /* '<Root>/Main' */
  real_T DBG_SpinCurrentRef;           /* '<Root>/Main' */
  real_T DBG_SpinStartStop;            /* '<Root>/Main' */
  real_T DBG_SpinModCont;              /* '<Root>/Main' */
  real_T DBG_ScalingFactor_PI;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedRef;             /* '<Root>/Main' */
  real_T DBG_TiltPosRef;               /* '<Root>/Main' */
  real_T DBG_EnableDaPilota;           /* '<Root>/Main' */
  real_T DBG_TiltPercRef1;             /* '<Root>/Main' */
  real_T DBG_TiltModCont;              /* '<Root>/Main' */
  real_T DBG_TiltStartStop;            /* '<Root>/Main' */
  real_T DBG_TiltPercRef2;             /* '<Root>/Main' */
  real_T DBG_ComandoPompa;             /* '<Root>/Main' */
  real_T DBG_TiltPercRef1_PI;          /* '<Root>/Main' */
  real_T DBG_TiltPercRef2_PI;          /* '<Root>/Main' */
  real_T DBG_FF1;                      /* '<Root>/Main' */
  real_T DBG_FF2;                      /* '<Root>/Main' */
  real_T DBG_TiltSpeedRef1;            /* '<Root>/Main' */
  real_T DBG_TiltSpeedRef2;            /* '<Root>/Main' */
  real_T DBG_MasterEnable;             /* '<Root>/Main' */
  real_T DBG_EnableTilt;               /* '<Root>/Main' */
  real_T DBG_EnableSpin;               /* '<Root>/Main' */
  real_T DBG_TiltSpeedEstimate1;       /* '<Root>/Main' */
  real_T DBG_TiltSpeedEstimate2;       /* '<Root>/Main' */
  real_T DBG_RollRate;                 /* '<Root>/Main' */
  real_T DBG_RollRate_GxGz;            /* '<Root>/Main' */
  real_T DBG_Roll;                     /* '<Root>/Main' */
  real_T DBG_TiltSpeedRefH2;           /* '<Root>/Main' */
  real_T DBG_ScalingFactor;            /* '<Root>/Main' */
  real_T DBG_TiltSpeedRefH1;           /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_1;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_6;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_1;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_6;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_2;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_3;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_4;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR62_5;         /* '<Root>/Main' */
  real_T DBG_TiltSpeed2LQR6;           /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_2;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_3;         /* '<Root>/Main' */
  real_T DBG_TiltSpeed1LQR6;           /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_4;         /* '<Root>/Main' */
  real_T DBG_TiltSpeedLQR61_5;         /* '<Root>/Main' */
  real_T DBG_RollSetPoint;             /* '<Root>/Main' */
  real_T A;                            /* '<Root>/Main' */
  real_T SFunction1_o1_h1;             /* '<S17>/S-Function1' */
  real_T SFunction1_o2_c;              /* '<S17>/S-Function1' */
  real_T SFunction1_o3_lt;             /* '<S17>/S-Function1' */
  real_T SFunction1_o4_c5;             /* '<S17>/S-Function1' */
  real_T SFunction1_o5_g;              /* '<S17>/S-Function1' */
  real_T SFunction1_o6_pm;             /* '<S17>/S-Function1' */
  real_T SFunction1_o7_m;              /* '<S17>/S-Function1' */
  real_T SFunction1_o8_mv;             /* '<S17>/S-Function1' */
  real_T SFunction1_o9_f;              /* '<S17>/S-Function1' */
  real_T SFunction1_o10_g;             /* '<S17>/S-Function1' */
  real_T SFunction1_o11_d;             /* '<S17>/S-Function1' */
  real_T SFunction1_o12_m;             /* '<S17>/S-Function1' */
  real_T SFunction1_o1_f;              /* '<S18>/S-Function1' */
  real_T SFunction1_o2_an;             /* '<S18>/S-Function1' */
  real_T SFunction1_o3_g;              /* '<S18>/S-Function1' */
  real_T SFunction1_o4_aq;             /* '<S18>/S-Function1' */
  real_T SFunction1_o5_av;             /* '<S18>/S-Function1' */
  real_T SFunction1_o6_j;              /* '<S18>/S-Function1' */
  real_T SFunction1_o7_n;              /* '<S18>/S-Function1' */
  real_T SFunction1_o8_l;              /* '<S18>/S-Function1' */
  real_T SFunction1_o9_p5;             /* '<S18>/S-Function1' */
  real_T SFunction1_o10_b;             /* '<S18>/S-Function1' */
  real_T SFunction1_o11_e;             /* '<S18>/S-Function1' */
  real_T SFunction1_o12_g;             /* '<S18>/S-Function1' */
  real_T SFunction1_o13_o;             /* '<S18>/S-Function1' */
  real_T SFunction1_o14_f;             /* '<S18>/S-Function1' */
  real_T SFunction1_o15_b;             /* '<S18>/S-Function1' */
  real_T SFunction1_o16_m;             /* '<S18>/S-Function1' */
  real_T SFunction1_o17_p;             /* '<S18>/S-Function1' */
  real_T SFunction1_o18_g;             /* '<S18>/S-Function1' */
  real_T SFunction1_o19_j;             /* '<S18>/S-Function1' */
  real_T DataTypeConversion;           /* '<S5>/Data Type Conversion' */
  real_T Gain1;                        /* '<S29>/Gain1' */
  real_T DataTypeConversion25;         /* '<S29>/Data Type Conversion25' */
  real_T DataTypeConversion23;         /* '<S29>/Data Type Conversion23' */
  real_T DataTypeConversion21;         /* '<S29>/Data Type Conversion21' */
  real_T DataTypeConversion19;         /* '<S29>/Data Type Conversion19' */
  real_T DataTypeConversion17;         /* '<S29>/Data Type Conversion17' */
  real_T DataTypeConversion13;         /* '<S29>/Data Type Conversion13' */
  real_T DataTypeConversion11;         /* '<S29>/Data Type Conversion11' */
  real_T DataTypeConversion1;          /* '<S29>/Data Type Conversion1' */
  real_T DataTypeConversion9;          /* '<S29>/Data Type Conversion9' */
  real_T DataTypeConversion6;          /* '<S29>/Data Type Conversion6' */
  real_T DataTypeConversion4;          /* '<S29>/Data Type Conversion4' */
  real_T DataTypeConversion16;         /* '<S29>/Data Type Conversion16' */
  real_T DataTypeConversion49;         /* '<S29>/Data Type Conversion49' */
  real_T Subtract;                     /* '<S29>/Subtract' */
  real_T Gain;                         /* '<S29>/Gain' */
  real_T DataTypeConversion49_f;       /* '<S30>/Data Type Conversion49' */
  real_T DataTypeConversion1_g;        /* '<S30>/Data Type Conversion1' */
  real_T DataTypeConversion47;         /* '<S30>/Data Type Conversion47' */
  real_T DataTypeConversion4_l;        /* '<S30>/Data Type Conversion4' */
  real_T DataTypeConversion12;         /* '<S30>/Data Type Conversion12' */
  real_T DataTypeConversion6_f;        /* '<S30>/Data Type Conversion6' */
  real_T DataTypeConversion9_e;        /* '<S30>/Data Type Conversion9' */
  real_T DataTypeConversion14;         /* '<S30>/Data Type Conversion14' */
  real32_T DataTypeConversion_g;       /* '<S71>/Data Type Conversion' */
  real32_T DataTypeConversion9_p;      /* '<S71>/Data Type Conversion9' */
  real32_T DataTypeConversion10;       /* '<S71>/Data Type Conversion10' */
  real32_T DataTypeConversion11_j;     /* '<S71>/Data Type Conversion11' */
  real32_T DataTypeConversion12_i;     /* '<S71>/Data Type Conversion12' */
  real32_T DataTypeConversion13_o;     /* '<S71>/Data Type Conversion13' */
  real32_T DataTypeConversion14_m;     /* '<S71>/Data Type Conversion14' */
  real32_T DataTypeConversion15;       /* '<S71>/Data Type Conversion15' */
  real32_T DataTypeConversion16_o;     /* '<S71>/Data Type Conversion16' */
  real32_T DataTypeConversion17_k;     /* '<S71>/Data Type Conversion17' */
  real32_T DataTypeConversion18;       /* '<S71>/Data Type Conversion18' */
  real32_T DataTypeConversion1_i;      /* '<S71>/Data Type Conversion1' */
  real32_T DataTypeConversion19_h;     /* '<S71>/Data Type Conversion19' */
  real32_T DataTypeConversion20;       /* '<S71>/Data Type Conversion20' */
  real32_T DataTypeConversion21_n;     /* '<S71>/Data Type Conversion21' */
  real32_T DataTypeConversion22;       /* '<S71>/Data Type Conversion22' */
  real32_T DataTypeConversion23_c;     /* '<S71>/Data Type Conversion23' */
  real32_T DataTypeConversion24;       /* '<S71>/Data Type Conversion24' */
  real32_T DataTypeConversion25_k;     /* '<S71>/Data Type Conversion25' */
  real32_T DataTypeConversion26;       /* '<S71>/Data Type Conversion26' */
  real32_T DataTypeConversion27;       /* '<S71>/Data Type Conversion27' */
  real32_T DataTypeConversion28;       /* '<S71>/Data Type Conversion28' */
  real32_T DataTypeConversion2;        /* '<S71>/Data Type Conversion2' */
  real32_T DataTypeConversion29;       /* '<S71>/Data Type Conversion29' */
  real32_T DataTypeConversion30;       /* '<S71>/Data Type Conversion30' */
  real32_T DataTypeConversion31;       /* '<S71>/Data Type Conversion31' */
  real32_T DataTypeConversion32;       /* '<S71>/Data Type Conversion32' */
  real32_T DataTypeConversion33;       /* '<S71>/Data Type Conversion33' */
  real32_T DataTypeConversion34;       /* '<S71>/Data Type Conversion34' */
  real32_T DataTypeConversion35;       /* '<S71>/Data Type Conversion35' */
  real32_T DataTypeConversion36;       /* '<S71>/Data Type Conversion36' */
  real32_T DataTypeConversion37;       /* '<S71>/Data Type Conversion37' */
  real32_T DataTypeConversion38;       /* '<S71>/Data Type Conversion38' */
  real32_T DataTypeConversion3;        /* '<S71>/Data Type Conversion3' */
  real32_T DataTypeConversion39;       /* '<S71>/Data Type Conversion39' */
  real32_T DataTypeConversion40;       /* '<S71>/Data Type Conversion40' */
  real32_T DataTypeConversion41;       /* '<S71>/Data Type Conversion41' */
  real32_T DataTypeConversion42;       /* '<S71>/Data Type Conversion42' */
  real32_T DataTypeConversion43;       /* '<S71>/Data Type Conversion43' */
  real32_T DataTypeConversion44;       /* '<S71>/Data Type Conversion44' */
  real32_T DataTypeConversion45;       /* '<S71>/Data Type Conversion45' */
  real32_T DataTypeConversion46;       /* '<S71>/Data Type Conversion46' */
  real32_T DataTypeConversion47_k;     /* '<S71>/Data Type Conversion47' */
  real32_T DataTypeConversion48;       /* '<S71>/Data Type Conversion48' */
  real32_T DataTypeConversion4_m;      /* '<S71>/Data Type Conversion4' */
  real32_T DataTypeConversion49_j;     /* '<S71>/Data Type Conversion49' */
  real32_T DataTypeConversion50;       /* '<S71>/Data Type Conversion50' */
  real32_T DataTypeConversion51;       /* '<S71>/Data Type Conversion51' */
  real32_T DataTypeConversion52;       /* '<S71>/Data Type Conversion52' */
  real32_T DataTypeConversion53;       /* '<S71>/Data Type Conversion53' */
  real32_T DataTypeConversion54;       /* '<S71>/Data Type Conversion54' */
  real32_T DataTypeConversion55;       /* '<S71>/Data Type Conversion55' */
  real32_T DataTypeConversion56;       /* '<S71>/Data Type Conversion56' */
  real32_T DataTypeConversion57;       /* '<S71>/Data Type Conversion57' */
  real32_T DataTypeConversion58;       /* '<S71>/Data Type Conversion58' */
  real32_T DataTypeConversion5;        /* '<S71>/Data Type Conversion5' */
  real32_T DataTypeConversion59;       /* '<S71>/Data Type Conversion59' */
  real32_T DataTypeConversion60;       /* '<S71>/Data Type Conversion60' */
  real32_T DataTypeConversion61;       /* '<S71>/Data Type Conversion61' */
  real32_T DataTypeConversion62;       /* '<S71>/Data Type Conversion62' */
  real32_T DataTypeConversion63;       /* '<S71>/Data Type Conversion63' */
  real32_T DataTypeConversion64;       /* '<S71>/Data Type Conversion64' */
  real32_T DataTypeConversion65;       /* '<S71>/Data Type Conversion65' */
  real32_T DataTypeConversion66;       /* '<S71>/Data Type Conversion66' */
  real32_T DataTypeConversion67;       /* '<S71>/Data Type Conversion67' */
  real32_T DataTypeConversion68;       /* '<S71>/Data Type Conversion68' */
  real32_T DataTypeConversion6_i;      /* '<S71>/Data Type Conversion6' */
  real32_T DataTypeConversion69;       /* '<S71>/Data Type Conversion69' */
  real32_T DataTypeConversion70;       /* '<S71>/Data Type Conversion70' */
  real32_T DataTypeConversion71;       /* '<S71>/Data Type Conversion71' */
  real32_T DataTypeConversion72;       /* '<S71>/Data Type Conversion72' */
  real32_T DataTypeConversion73;       /* '<S71>/Data Type Conversion73' */
  real32_T DataTypeConversion74;       /* '<S71>/Data Type Conversion74' */
  real32_T DataTypeConversion75;       /* '<S71>/Data Type Conversion75' */
  real32_T DataTypeConversion76;       /* '<S71>/Data Type Conversion76' */
  real32_T DataTypeConversion77;       /* '<S71>/Data Type Conversion77' */
  real32_T DataTypeConversion7;        /* '<S71>/Data Type Conversion7' */
  real32_T DataTypeConversion8;        /* '<S71>/Data Type Conversion8' */
  uint16_T DataTypeConversion2_a;      /* '<S29>/Data Type Conversion2' */
  uint16_T DataTypeConversion7_i;      /* '<S29>/Data Type Conversion7' */
  uint16_T DataTypeConversion2_p;      /* '<S32>/Data Type Conversion2' */
  uint16_T Add;                        /* '<S32>/Add' */
  uint16_T DataTypeConversion17_d;     /* '<S32>/Data Type Conversion17' */
  uint16_T Add1;                       /* '<S32>/Add1' */
  uint16_T DataTypeConversion7_e;      /* '<S30>/Data Type Conversion7' */
  uint16_T DataTypeConversion2_c;      /* '<S57>/Data Type Conversion2' */
  uint16_T DataTypeConversion2_b;      /* '<S30>/Data Type Conversion2' */
  uint16_T DataTypeConversion4_k;      /* '<S57>/Data Type Conversion4' */
  uint16_T Add_f;                      /* '<S57>/Add' */
  uint16_T ExtractDesiredBits;         /* '<S34>/Extract Desired Bits' */
  uint16_T ModifyScalingOnly;          /* '<S34>/Modify Scaling Only' */
  uint16_T ShiftArithmetic3;           /* '<S32>/Shift Arithmetic3' */
  uint16_T ExtractDesiredBits_j;       /* '<S54>/Extract Desired Bits' */
  uint16_T ModifyScalingOnly_e;        /* '<S54>/Modify Scaling Only' */
  uint16_T ShiftArithmetic8;           /* '<S32>/Shift Arithmetic8' */
  uint16_T ShiftArithmetic;            /* '<S32>/Shift Arithmetic' */
  uint16_T ExtractDesiredBits_c;       /* '<S59>/Extract Desired Bits' */
  uint16_T ModifyScalingOnly_j;        /* '<S59>/Modify Scaling Only' */
  uint16_T ShiftArithmetic_b;          /* '<S57>/Shift Arithmetic' */
  uint16_T ShiftArithmetic3_g;         /* '<S57>/Shift Arithmetic3' */
  uint16_T ExtractDesiredBits_d;       /* '<S60>/Extract Desired Bits' */
  uint16_T ModifyScalingOnly_jd;       /* '<S60>/Modify Scaling Only' */
  uint16_T ShiftArithmetic1;           /* '<S57>/Shift Arithmetic1' */
  uint8_T DataTypeConversion24_n;      /* '<S29>/Data Type Conversion24' */
  uint8_T DataTypeConversion22_o;      /* '<S29>/Data Type Conversion22' */
  uint8_T DataTypeConversion20_g;      /* '<S29>/Data Type Conversion20' */
  uint8_T DataTypeConversion18_o;      /* '<S29>/Data Type Conversion18' */
  uint8_T DataTypeConversion14_mc;     /* '<S29>/Data Type Conversion14' */
  uint8_T DataTypeConversion12_f;      /* '<S29>/Data Type Conversion12' */
  uint8_T DataTypeConversion10_d;      /* '<S29>/Data Type Conversion10' */
  uint8_T DataTypeConversion8_d;       /* '<S29>/Data Type Conversion8' */
  uint8_T DataTypeConversion5_d;       /* '<S29>/Data Type Conversion5' */
  uint8_T DataTypeConversion3_a;       /* '<S29>/Data Type Conversion3' */
  uint8_T DataTypeConversion15_i;      /* '<S29>/Data Type Conversion15' */
  uint8_T DataTypeConversion8_i;       /* '<S32>/Data Type Conversion8' */
  uint8_T DataTypeConversion9_k;       /* '<S32>/Data Type Conversion9' */
  uint8_T DataTypeConversion4_mf;      /* '<S32>/Data Type Conversion4' */
  uint8_T DataTypeConversion16_e;      /* '<S32>/Data Type Conversion16' */
  uint8_T DataTypeConversion1_a;       /* '<S32>/Data Type Conversion1' */
  uint8_T DataTypeConversion13_i;      /* '<S32>/Data Type Conversion13' */
  uint8_T DataTypeConversion14_j;      /* '<S32>/Data Type Conversion14' */
  uint8_T DataTypeConversion26_d;      /* '<S29>/Data Type Conversion26' */
  uint8_T DataTypeConversion27_j;      /* '<S29>/Data Type Conversion27' */
  uint8_T DataTypeConversion15_h;      /* '<S32>/Data Type Conversion15' */
  uint8_T Add2;                        /* '<S32>/Add2' */
  uint8_T DataTypeConversion19_f;      /* '<S32>/Data Type Conversion19' */
  uint8_T DataTypeConversion18_e;      /* '<S32>/Data Type Conversion18' */
  uint8_T DataTypeConversion20_e;      /* '<S32>/Data Type Conversion20' */
  uint8_T Add3;                        /* '<S32>/Add3' */
  uint8_T DataTypeConversion21_h;      /* '<S32>/Data Type Conversion21' */
  uint8_T ExtractDesiredBits_o;        /* '<S40>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_c;         /* '<S40>/Modify Scaling Only' */
  uint8_T DataTypeConversion10_n;      /* '<S32>/Data Type Conversion10' */
  uint8_T ExtractDesiredBits_e;        /* '<S36>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_a;         /* '<S36>/Modify Scaling Only' */
  uint8_T DataTypeConversion11_j3;     /* '<S32>/Data Type Conversion11' */
  uint8_T ExtractDesiredBits_a;        /* '<S47>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_l;         /* '<S47>/Modify Scaling Only' */
  uint8_T DataTypeConversion12_j;      /* '<S32>/Data Type Conversion12' */
  uint8_T ExtractDesiredBits_g;        /* '<S50>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_p;         /* '<S50>/Modify Scaling Only' */
  uint8_T DataTypeConversion3_l;       /* '<S32>/Data Type Conversion3' */
  uint8_T ExtractDesiredBits_b;        /* '<S49>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_jw;        /* '<S49>/Modify Scaling Only' */
  uint8_T DataTypeConversion5_p;       /* '<S32>/Data Type Conversion5' */
  uint8_T ExtractDesiredBits_k;        /* '<S48>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_cq;        /* '<S48>/Modify Scaling Only' */
  uint8_T DataTypeConversion6_b;       /* '<S32>/Data Type Conversion6' */
  uint8_T ExtractDesiredBits_ej;       /* '<S35>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_c5;        /* '<S35>/Modify Scaling Only' */
  uint8_T DataTypeConversion7_m;       /* '<S32>/Data Type Conversion7' */
  uint8_T DataTypeConversion28_j;      /* '<S29>/Data Type Conversion28' */
  uint8_T DataTypeConversion29_e;      /* '<S29>/Data Type Conversion29' */
  uint8_T ExtractDesiredBits_c1;       /* '<S63>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_et;        /* '<S63>/Modify Scaling Only' */
  uint8_T DataTypeConversion3_b;       /* '<S57>/Data Type Conversion3' */
  uint8_T ExtractDesiredBits_b1;       /* '<S62>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_b;         /* '<S62>/Modify Scaling Only' */
  uint8_T DataTypeConversion5_j;       /* '<S57>/Data Type Conversion5' */
  uint8_T ExtractDesiredBits_p;        /* '<S61>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_n;         /* '<S61>/Modify Scaling Only' */
  uint8_T DataTypeConversion6_m;       /* '<S57>/Data Type Conversion6' */
  uint8_T DataTypeConversion10_e;      /* '<S30>/Data Type Conversion10' */
  uint8_T DataTypeConversion7_e5;      /* '<S57>/Data Type Conversion7' */
  uint8_T DataTypeConversion3_j;       /* '<S30>/Data Type Conversion3' */
  uint8_T DataTypeConversion11_o;      /* '<S57>/Data Type Conversion11' */
  uint8_T DataTypeConversion10_f;      /* '<S57>/Data Type Conversion10' */
  uint8_T DataTypeConversion11_f;      /* '<S30>/Data Type Conversion11' */
  uint8_T DataTypeConversion1_go;      /* '<S57>/Data Type Conversion1' */
  uint8_T ShiftArithmetic7;            /* '<S57>/Shift Arithmetic7' */
  uint8_T DataTypeConversion5_g;       /* '<S30>/Data Type Conversion5' */
  uint8_T DataTypeConversion13_og;     /* '<S57>/Data Type Conversion13' */
  uint8_T ShiftArithmetic5;            /* '<S57>/Shift Arithmetic5' */
  uint8_T DataTypeConversion8_j;       /* '<S30>/Data Type Conversion8' */
  uint8_T DataTypeConversion14_o;      /* '<S57>/Data Type Conversion14' */
  uint8_T ShiftArithmetic6;            /* '<S57>/Shift Arithmetic6' */
  uint8_T DataTypeConversion13_h;      /* '<S30>/Data Type Conversion13' */
  uint8_T DataTypeConversion15_io;     /* '<S57>/Data Type Conversion15' */
  uint8_T Add2_a;                      /* '<S57>/Add2' */
  uint8_T DataTypeConversion12_is;     /* '<S57>/Data Type Conversion12' */
  uint8_T ExtractDesiredBits_ph;       /* '<S46>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_aj;        /* '<S46>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_i;        /* '<S37>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_l1;        /* '<S37>/Modify Scaling Only' */
  uint8_T ShiftArithmetic10;           /* '<S32>/Shift Arithmetic10' */
  uint8_T ExtractDesiredBits_gr;       /* '<S39>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_cb;        /* '<S39>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_bn;       /* '<S45>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_d;         /* '<S45>/Modify Scaling Only' */
  uint8_T ShiftArithmetic4;            /* '<S32>/Shift Arithmetic4' */
  uint8_T ExtractDesiredBits_gv;       /* '<S53>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_a0;        /* '<S53>/Modify Scaling Only' */
  uint8_T ShiftArithmetic1_g;          /* '<S32>/Shift Arithmetic1' */
  uint8_T ExtractDesiredBits_e2;       /* '<S42>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_h;         /* '<S42>/Modify Scaling Only' */
  uint8_T ShiftArithmetic7_j;          /* '<S32>/Shift Arithmetic7' */
  uint8_T ExtractDesiredBits_gh;       /* '<S38>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_g;         /* '<S38>/Modify Scaling Only' */
  uint8_T ShiftArithmetic9;            /* '<S32>/Shift Arithmetic9' */
  uint8_T ExtractDesiredBits_n;        /* '<S41>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_k;         /* '<S41>/Modify Scaling Only' */
  uint8_T ShiftArithmetic11;           /* '<S32>/Shift Arithmetic11' */
  uint8_T ExtractDesiredBits_po;       /* '<S51>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_m;         /* '<S51>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_l;        /* '<S52>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_aq;        /* '<S52>/Modify Scaling Only' */
  uint8_T ShiftArithmetic2;            /* '<S32>/Shift Arithmetic2' */
  uint8_T ExtractDesiredBits_h;        /* '<S43>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_nj;        /* '<S43>/Modify Scaling Only' */
  uint8_T ShiftArithmetic5_j;          /* '<S32>/Shift Arithmetic5' */
  uint8_T ExtractDesiredBits_o5;       /* '<S44>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_m3;        /* '<S44>/Modify Scaling Only' */
  uint8_T ShiftArithmetic6_d;          /* '<S32>/Shift Arithmetic6' */
  boolean_T SFunction1_o1_am;          /* '<S154>/S-Function1' */
  boolean_T SFunction1_o2_cg;          /* '<S154>/S-Function1' */
  B_Counter_Gyro_T sf_Counter_k;       /* '<S30>/Counter' */
  B_MATLABFunction_Gyro_T sf_MATLABFunction_l;/* '<S57>/MATLAB Function' */
  B_Counter_Gyro_T sf_Counter;         /* '<S29>/Counter' */
  B_MATLABFunction_Gyro_T sf_MATLABFunction;/* '<S32>/MATLAB Function' */
} B_Gyro_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  int32_T sfEvent;                     /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i1;         /* '<S4>/TASK_LOG_GENERATOR' */
  uint32_T temporalCounter_i2;         /* '<S4>/TASK_LOG_GENERATOR' */
  uint32_T temporalCounter_i1_o;       /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i2_h;       /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i3;         /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i4;         /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i5;         /* '<S28>/F_TASK_GENERATOR' */
  uint32_T temporalCounter_i6;         /* '<S28>/F_TASK_GENERATOR' */
  uint32_T Add_DWORK1;                 /* '<S32>/Add' */
  int_T SFunction1_IWORK[2];           /* '<S152>/S-Function1' */
  int_T SFunction1_IWORK_o[2];         /* '<S157>/S-Function1' */
  uint8_T is_active_c4_Gyro;           /* '<S4>/TASK_LOG_GENERATOR' */
  uint8_T is_TASK_1;                   /* '<S4>/TASK_LOG_GENERATOR' */
  uint8_T is_TASK_2;                   /* '<S4>/TASK_LOG_GENERATOR' */
  uint8_T is_active_c1_Gyro;           /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_1tick;               /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_10ms;                /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_20ms;                /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_100ms;               /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_200ms;               /* '<S28>/F_TASK_GENERATOR' */
  uint8_T is_TASK_2ms;                 /* '<S28>/F_TASK_GENERATOR' */
  DW_Counter_Gyro_T sf_Counter_k;      /* '<S30>/Counter' */
  DW_Counter_Gyro_T sf_Counter;        /* '<S29>/Counter' */
} DW_Gyro_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState USB_FLIGHT_REC_EJECT_Trig_ZCE;/* '<S5>/USB_FLIGHT_REC_EJECT' */
} PrevZCX_Gyro_T;

/* Parameters (default storage) */
struct P_Gyro_T_ {
  real_T Gain1_Gain;                   /* Expression: 1/0.0625
                                        * Referenced by: '<S29>/Gain1'
                                        */
  real_T Constant_Value;               /* Expression: 255
                                        * Referenced by: '<S29>/Constant'
                                        */
  real_T Gain_Gain;                    /* Expression: 1/0.0625
                                        * Referenced by: '<S29>/Gain'
                                        */
  real_T Constant_Value_f;             /* Expression: 0
                                        * Referenced by: '<S57>/Constant'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Gyro_T {
  const char_T *errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_Gyro_T Gyro_P;

/* Block signals (default storage) */
extern B_Gyro_T Gyro_B;

/* Block states (default storage) */
extern DW_Gyro_T Gyro_DW;

/* External data declarations for dependent source files */

/* Zero-crossing (trigger) state */
extern PrevZCX_Gyro_T Gyro_PrevZCX;

/* Model block global parameters (default storage) */
extern real_T rtP_Debounce;            /* Variable: Debounce
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_DebounceMaster;      /* Variable: DebounceMaster
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFHighSlope1;        /* Variable: FFHighSlope1
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFHighSlope2;        /* Variable: FFHighSlope2
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFLowerSlope1;       /* Variable: FFLowerSlope1
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFLowerSlope2;       /* Variable: FFLowerSlope2
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFOffset1_minus;     /* Variable: FFOffset1_minus
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFOffset1_plus;      /* Variable: FFOffset1_plus
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFOffset2_minus;     /* Variable: FFOffset2_minus
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFOffset2_plus;      /* Variable: FFOffset2_plus
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_FFScale;             /* Variable: FFScale
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_GainSchedGain;       /* Variable: GainSchedGain
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_GainSchedMax;        /* Variable: GainSchedMax
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_GainSchedMin;        /* Variable: GainSchedMin
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_GainSched_offset;    /* Variable: GainSched_offset
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_K_LQR61[6];          /* Variable: K_LQR61
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_K_LQR62[6];          /* Variable: K_LQR62
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_Ki_speed_PI1;        /* Variable: Ki_speed_PI1
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_Ki_speed_PI2;        /* Variable: Ki_speed_PI2
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_Kp_speed_PI1;        /* Variable: Kp_speed_PI1
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_Kp_speed_PI2;        /* Variable: Kp_speed_PI2
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_LP20_den[3];         /* Variable: LP20_den
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_LP20_num[3];         /* Variable: LP20_num
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_RampTime;            /* Variable: RampTime
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_SpeedThreshold;      /* Variable: SpeedThreshold
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_T_log1;              /* Variable: T_log1
                                        * Referenced by: '<S4>/TASK_LOG_GENERATOR'
                                        */
extern real_T rtP_T_log2;              /* Variable: T_log2
                                        * Referenced by: '<S4>/TASK_LOG_GENERATOR'
                                        */
extern real_T rtP_Ti_position;         /* Variable: Ti_position
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_Ts;                  /* Variable: Ts
                                        * Referenced by:
                                        *   '<Root>/Main'
                                        *   '<S4>/TASK_LOG_GENERATOR'
                                        *   '<S28>/F_TASK_GENERATOR'
                                        */
extern real_T rtP_alpha_IMU;           /* Variable: alpha_IMU
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_angle_limit_speed;   /* Variable: angle_limit_speed
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_angle_max;           /* Variable: angle_max
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_den_der_Tilt[3];     /* Variable: den_der_Tilt
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_f_notch;             /* Variable: f_notch
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_kp_position;         /* Variable: kp_position
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_max_speed;           /* Variable: max_speed
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_num_der_Tilt[3];     /* Variable: num_der_Tilt
                                        * Referenced by: '<Root>/Main'
                                        */
extern real_T rtP_xi_notch;            /* Variable: xi_notch
                                        * Referenced by: '<Root>/Main'
                                        */

/* Model entry point functions */
extern void Gyro_initialize(void);
extern void Gyro_output(void);
extern void Gyro_update(void);
extern void Gyro_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Gyro_T *const Gyro_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Gyro'
 * '<S1>'   : 'Gyro/Input_Data'
 * '<S2>'   : 'Gyro/Output_Data'
 * '<S3>'   : 'Gyro/RTI Data'
 * '<S4>'   : 'Gyro/Save_Data'
 * '<S5>'   : 'Gyro/dSpace_Setup'
 * '<S6>'   : 'Gyro/Input_Data/ABS'
 * '<S7>'   : 'Gyro/Input_Data/BBS'
 * '<S8>'   : 'Gyro/Input_Data/DSB'
 * '<S9>'   : 'Gyro/Input_Data/GyroBox'
 * '<S10>'  : 'Gyro/Input_Data/IMU'
 * '<S11>'  : 'Gyro/Input_Data/ABS/ABS_Data01'
 * '<S12>'  : 'Gyro/Input_Data/ABS/ABS_Data02'
 * '<S13>'  : 'Gyro/Input_Data/ABS/ABS_Data03'
 * '<S14>'  : 'Gyro/Input_Data/BBS/BBS_Data01_Copy'
 * '<S15>'  : 'Gyro/Input_Data/BBS/BBS_Data02_Copy'
 * '<S16>'  : 'Gyro/Input_Data/DSB/DSB_Data01'
 * '<S17>'  : 'Gyro/Input_Data/DSB/DSB_Data02'
 * '<S18>'  : 'Gyro/Input_Data/DSB/DSB_Data03'
 * '<S19>'  : 'Gyro/Input_Data/GyroBox/PLC_Data01'
 * '<S20>'  : 'Gyro/Input_Data/GyroBox/PLC_Data02'
 * '<S21>'  : 'Gyro/Input_Data/GyroBox/PLC_Data03'
 * '<S22>'  : 'Gyro/Input_Data/GyroBox/PLC_Data04'
 * '<S23>'  : 'Gyro/Input_Data/IMU/IMU_Data01'
 * '<S24>'  : 'Gyro/Input_Data/IMU/IMU_Data02'
 * '<S25>'  : 'Gyro/Input_Data/IMU/IMU_Data03'
 * '<S26>'  : 'Gyro/Output_Data/MonitorMessages'
 * '<S27>'  : 'Gyro/Output_Data/SpinTiltMessages'
 * '<S28>'  : 'Gyro/Output_Data/Tasks & priority'
 * '<S29>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018'
 * '<S30>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019'
 * '<S31>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/BBS_Data01'
 * '<S32>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02'
 * '<S33>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/Counter'
 * '<S34>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 1'
 * '<S35>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 10'
 * '<S36>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 11'
 * '<S37>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 12'
 * '<S38>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 13'
 * '<S39>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 14'
 * '<S40>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 15'
 * '<S41>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 16'
 * '<S42>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 17'
 * '<S43>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 18'
 * '<S44>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 19'
 * '<S45>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 2'
 * '<S46>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 20'
 * '<S47>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 21'
 * '<S48>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 3'
 * '<S49>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 4'
 * '<S50>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 5'
 * '<S51>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 6'
 * '<S52>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 7'
 * '<S53>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 8'
 * '<S54>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/Extract Bits 9'
 * '<S55>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x018/CRC_Data02/MATLAB Function'
 * '<S56>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/BBS_Data02'
 * '<S57>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02'
 * '<S58>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/Counter'
 * '<S59>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/Extract Bits 1'
 * '<S60>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/Extract Bits 2'
 * '<S61>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/Extract Bits 3'
 * '<S62>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/Extract Bits 4'
 * '<S63>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/Extract Bits 5'
 * '<S64>'  : 'Gyro/Output_Data/MonitorMessages/Msg_x019/CRC_Data02/MATLAB Function'
 * '<S65>'  : 'Gyro/Output_Data/SpinTiltMessages/AME_Data05'
 * '<S66>'  : 'Gyro/Output_Data/SpinTiltMessages/AME_Data06'
 * '<S67>'  : 'Gyro/Output_Data/Tasks & priority/F_TASK_GENERATOR'
 * '<S68>'  : 'Gyro/RTI Data/RTI Data Store'
 * '<S69>'  : 'Gyro/RTI Data/RTI Data Store/RTI Data Store'
 * '<S70>'  : 'Gyro/RTI Data/RTI Data Store/RTI Data Store/RTI Data Store'
 * '<S71>'  : 'Gyro/Save_Data/Log1'
 * '<S72>'  : 'Gyro/Save_Data/Log2'
 * '<S73>'  : 'Gyro/Save_Data/TASK_LOG_GENERATOR'
 * '<S74>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL1'
 * '<S75>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL10'
 * '<S76>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL11'
 * '<S77>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL12'
 * '<S78>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL13'
 * '<S79>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL14'
 * '<S80>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL15'
 * '<S81>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL16'
 * '<S82>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL17'
 * '<S83>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL18'
 * '<S84>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL19'
 * '<S85>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL2'
 * '<S86>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL20'
 * '<S87>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL21'
 * '<S88>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL22'
 * '<S89>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL23'
 * '<S90>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL24'
 * '<S91>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL25'
 * '<S92>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL26'
 * '<S93>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL27'
 * '<S94>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL28'
 * '<S95>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL29'
 * '<S96>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL3'
 * '<S97>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL30'
 * '<S98>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL31'
 * '<S99>'  : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL32'
 * '<S100>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL33'
 * '<S101>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL34'
 * '<S102>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL35'
 * '<S103>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL36'
 * '<S104>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL37'
 * '<S105>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL38'
 * '<S106>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL39'
 * '<S107>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL4'
 * '<S108>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL40'
 * '<S109>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL41'
 * '<S110>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL42'
 * '<S111>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL43'
 * '<S112>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL44'
 * '<S113>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL45'
 * '<S114>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL46'
 * '<S115>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL47'
 * '<S116>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL48'
 * '<S117>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL49'
 * '<S118>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL5'
 * '<S119>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL50'
 * '<S120>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL51'
 * '<S121>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL52'
 * '<S122>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL53'
 * '<S123>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL54'
 * '<S124>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL55'
 * '<S125>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL56'
 * '<S126>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL57'
 * '<S127>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL58'
 * '<S128>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL59'
 * '<S129>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL6'
 * '<S130>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL60'
 * '<S131>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL61'
 * '<S132>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL62'
 * '<S133>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL63'
 * '<S134>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL64'
 * '<S135>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL65'
 * '<S136>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL66'
 * '<S137>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL67'
 * '<S138>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL68'
 * '<S139>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL69'
 * '<S140>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL7'
 * '<S141>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL70'
 * '<S142>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL71'
 * '<S143>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL72'
 * '<S144>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL73'
 * '<S145>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL74'
 * '<S146>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL75'
 * '<S147>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL76'
 * '<S148>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL77'
 * '<S149>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL78'
 * '<S150>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL8'
 * '<S151>' : 'Gyro/Save_Data/Log1/USB_FLIGHT_REC_BL9'
 * '<S152>' : 'Gyro/dSpace_Setup/CAN_TYPE1_SETUP_M1_C1'
 * '<S153>' : 'Gyro/dSpace_Setup/DS1401_POWER_DOWN'
 * '<S154>' : 'Gyro/dSpace_Setup/DS1401_REMOTE_IN_BL1'
 * '<S155>' : 'Gyro/dSpace_Setup/USB_FLIGHT_REC_EJECT'
 * '<S156>' : 'Gyro/dSpace_Setup/USB_FLIGHT_REC_SETUP'
 * '<S157>' : 'Gyro/dSpace_Setup/_CAN_TYPE1_SETUP_M1_C2'
 */
#endif                                 /* RTW_HEADER_Gyro_h_ */
