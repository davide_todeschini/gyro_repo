/*
 * TiltControl_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "TiltControl".
 *
 * Model version              : 1.598
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:46:00 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TiltControl_private_h_
#define RTW_HEADER_TiltControl_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        (*((rtm)->errorStatus))
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   (*((rtm)->errorStatus) = (val))
#endif

#ifndef rtmGetErrorStatusPointer
# define rtmGetErrorStatusPointer(rtm) (rtm)->errorStatus
#endif

#ifndef rtmSetErrorStatusPointer
# define rtmSetErrorStatusPointer(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef TiltControl_MDLREF_HIDE_CHILD_

extern P_TiltControl_T TiltControl_P_g;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/
#endif                                 /* RTW_HEADER_TiltControl_private_h_ */
