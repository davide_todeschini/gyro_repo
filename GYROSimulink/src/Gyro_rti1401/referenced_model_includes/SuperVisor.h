/*
 * Code generation for system model 'SuperVisor'
 * For more details, see corresponding source file SuperVisor.c
 *
 */

#ifndef RTW_HEADER_SuperVisor_h_
#define RTW_HEADER_SuperVisor_h_
#include <math.h>
#include <string.h>
#ifndef SuperVisor_COMMON_INCLUDES_
# define SuperVisor_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* SuperVisor_COMMON_INCLUDES_ */

#include "SuperVisor_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Block signals for model 'SuperVisor' */
#ifndef SuperVisor_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Abs;                          /* '<Root>/Abs' */
  boolean_T Compare;                   /* '<S1>/Compare' */
  boolean_T Compare_i;                 /* '<S2>/Compare' */
  boolean_T Compare_h;                 /* '<S3>/Compare' */
  boolean_T Compare_n;                 /* '<S4>/Compare' */
  boolean_T Compare_g;                 /* '<S5>/Compare' */
  boolean_T Compare_d;                 /* '<S6>/Compare' */
  boolean_T Compare_nm;                /* '<S7>/Compare' */
  boolean_T Compare_l;                 /* '<S8>/Compare' */
  boolean_T Compare_gx;                /* '<S9>/Compare' */
  boolean_T Compare_nf;                /* '<S10>/Compare' */
  boolean_T Compare_k;                 /* '<S11>/Compare' */
  boolean_T LogicalOperator;           /* '<Root>/Logical Operator' */
  boolean_T LogicalOperator1;          /* '<Root>/Logical Operator1' */
} B_SuperVisor_c_T;

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

#ifndef SuperVisor_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_SuperVisor_T_ {
  real_T CompareToConstant_const;      /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S1>/Constant'
                                        */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S2>/Constant'
                                        */
  real_T CompareToConstant10_const;    /* Mask Parameter: CompareToConstant10_const
                                        * Referenced by: '<S3>/Constant'
                                        */
  real_T CompareToConstant2_const;     /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S4>/Constant'
                                        */
  real_T CompareToConstant3_const;     /* Mask Parameter: CompareToConstant3_const
                                        * Referenced by: '<S5>/Constant'
                                        */
  real_T CompareToConstant4_const;     /* Mask Parameter: CompareToConstant4_const
                                        * Referenced by: '<S6>/Constant'
                                        */
  real_T CompareToConstant5_const;     /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S7>/Constant'
                                        */
  real_T CompareToConstant6_const;     /* Mask Parameter: CompareToConstant6_const
                                        * Referenced by: '<S8>/Constant'
                                        */
  real_T CompareToConstant7_const;     /* Mask Parameter: CompareToConstant7_const
                                        * Referenced by: '<S9>/Constant'
                                        */
  real_T CompareToConstant8_const;     /* Mask Parameter: CompareToConstant8_const
                                        * Referenced by: '<S10>/Constant'
                                        */
  real_T CompareToConstant9_const;     /* Mask Parameter: CompareToConstant9_const
                                        * Referenced by: '<S11>/Constant'
                                        */
  DebugBus_SuperVisor DebugBus_SuperVisor_Y0;/* Computed Parameter: DebugBus_SuperVisor_Y0
                                              * Referenced by: '<Root>/DebugBus_SuperVisor'
                                              */
  real_T EnableSpinl01_Y0;             /* Expression: [0]
                                        * Referenced by: '<Root>/EnableSpinl[0-1]'
                                        */
  real_T EnableTilt01_Y0;              /* Expression: [0]
                                        * Referenced by: '<Root>/EnableTilt[0-1]'
                                        */
  real_T ManualTiltEnable_Value;       /* Expression: 0
                                        * Referenced by: '<Root>/ManualTiltEnable'
                                        */
};

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

#ifndef SuperVisor_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_SuperVisor_T {
  const char_T **errorStatus;
};

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

#ifndef SuperVisor_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_SuperVisor_T rtm;
} MdlrefDW_SuperVisor_T;

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

extern void SuperVisor_Init(real_T *rty_EnableSpinl01, real_T *rty_EnableTilt01,
  DebugBus_SuperVisor *rty_DebugBus_SuperVisor);
extern void SuperVisor(const real_T *rtu_DriverStatus01, const real_T
  *rtu_DriverErr101, const real_T *rtu_DriverErr201, const real_T
  *rtu_MotorVoltErr101, const real_T *rtu_MotorVoltErr201, const real_T
  *rtu_MotorTempErr101, const real_T *rtu_MotorTempErr201, const real_T
  *rtu_PistonErr01, const real_T *rtu_Rolldeg, const real_T *rtu_SpinSpeed1rpm,
  const real_T *rtu_SpinSpeed2rpm, real_T *rty_EnableSpinl01, real_T
  *rty_EnableTilt01, DebugBus_SuperVisor *rty_DebugBus_SuperVisor);

/* Model reference registration function */
extern void SuperVisor_initialize(const char_T **rt_errorStatus);

#ifndef SuperVisor_MDLREF_HIDE_CHILD_

extern MdlrefDW_SuperVisor_T SuperVisor_MdlrefDW;

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

#ifndef SuperVisor_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_SuperVisor_c_T SuperVisor_B;

#endif                                 /*SuperVisor_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'SuperVisor'
 * '<S1>'   : 'SuperVisor/Compare To Constant'
 * '<S2>'   : 'SuperVisor/Compare To Constant1'
 * '<S3>'   : 'SuperVisor/Compare To Constant10'
 * '<S4>'   : 'SuperVisor/Compare To Constant2'
 * '<S5>'   : 'SuperVisor/Compare To Constant3'
 * '<S6>'   : 'SuperVisor/Compare To Constant4'
 * '<S7>'   : 'SuperVisor/Compare To Constant5'
 * '<S8>'   : 'SuperVisor/Compare To Constant6'
 * '<S9>'   : 'SuperVisor/Compare To Constant7'
 * '<S10>'  : 'SuperVisor/Compare To Constant8'
 * '<S11>'  : 'SuperVisor/Compare To Constant9'
 * '<S12>'  : 'SuperVisor/DebugBus_Creation'
 */
#endif                                 /* RTW_HEADER_SuperVisor_h_ */
