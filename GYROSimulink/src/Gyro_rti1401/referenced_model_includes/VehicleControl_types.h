/*
 * VehicleControl_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "VehicleControl".
 *
 * Model version              : 1.641
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:46:17 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_VehicleControl_types_h_
#define RTW_HEADER_VehicleControl_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_DebugBus_VehicleControl_
#define DEFINED_TYPEDEF_FOR_DebugBus_VehicleControl_

typedef struct {
  real_T DBG_TiltSpeedRefH2;
  real_T DBG_ScalingFactor;
  real_T DBG_TiltSpeedRefH1;
  real_T DBG_TiltSpeedLQR61_1;
  real_T DBG_TiltSpeedLQR61_6;
  real_T DBG_TiltSpeedLQR62_1;
  real_T DBG_TiltSpeedLQR62_6;
  real_T DBG_TiltSpeedLQR62_2;
  real_T DBG_TiltSpeedLQR62_3;
  real_T DBG_TiltSpeedLQR62_4;
  real_T DBG_TiltSpeedLQR62_5;
  real_T DBG_TiltSpeed2LQR6;
  real_T DBG_TiltSpeedLQR61_2;
  real_T DBG_TiltSpeedLQR61_3;
  real_T DBG_TiltSpeed1LQR6;
  real_T DBG_TiltSpeedLQR61_4;
  real_T DBG_TiltSpeedLQR61_5;
  real_T DBG_RollSetPoint;
} DebugBus_VehicleControl;

#endif

/* Parameters for system: '<S2>/Discrete Time Integrator1' */
typedef struct P_DiscreteTimeIntegrator1_Veh_T_ P_DiscreteTimeIntegrator1_Veh_T;

/* Parameters for system: '<S4>/Int1' */
typedef struct P_Int1_VehicleControl_T_ P_Int1_VehicleControl_T;

/* Parameters (default storage) */
typedef struct P_VehicleControl_T_ P_VehicleControl_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_VehicleControl_T RT_MODEL_VehicleControl_T;

#endif                                 /* RTW_HEADER_VehicleControl_types_h_ */
