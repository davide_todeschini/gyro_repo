/*
 * DashBoard_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "DashBoard".
 *
 * Model version              : 1.380
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:44:53 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DashBoard_private_h_
#define RTW_HEADER_DashBoard_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        (*((rtm)->errorStatus))
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   (*((rtm)->errorStatus) = (val))
#endif

#ifndef rtmGetErrorStatusPointer
# define rtmGetErrorStatusPointer(rtm) (rtm)->errorStatus
#endif

#ifndef rtmSetErrorStatusPointer
# define rtmSetErrorStatusPointer(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef DashBoard_MDLREF_HIDE_CHILD_

extern P_DashBoard_T DashBoard_P_g;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/
#endif                                 /* RTW_HEADER_DashBoard_private_h_ */
