/*
 * DashBoard_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "DashBoard".
 *
 * Model version              : 1.380
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:44:53 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DashBoard_types_h_
#define RTW_HEADER_DashBoard_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_DebugBus_DashBoard_
#define DEFINED_TYPEDEF_FOR_DebugBus_DashBoard_

typedef struct {
  real_T A;
} DebugBus_DashBoard;

#endif

/* Parameters (default storage) */
typedef struct P_DashBoard_T_ P_DashBoard_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_DashBoard_T RT_MODEL_DashBoard_T;

#endif                                 /* RTW_HEADER_DashBoard_types_h_ */
