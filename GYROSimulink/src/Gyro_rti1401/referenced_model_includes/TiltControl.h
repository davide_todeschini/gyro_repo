/*
 * Code generation for system model 'TiltControl'
 * For more details, see corresponding source file TiltControl.c
 *
 */

#ifndef RTW_HEADER_TiltControl_h_
#define RTW_HEADER_TiltControl_h_
#include <string.h>
#ifndef TiltControl_COMMON_INCLUDES_
# define TiltControl_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* TiltControl_COMMON_INCLUDES_ */

#include "TiltControl_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Block signals for model 'TiltControl' */
#ifndef TiltControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Switch2;                      /* '<S6>/Switch2' */
  real_T Saturation2;                  /* '<S6>/Saturation2' */
  real_T Switch3;                      /* '<S6>/Switch3' */
  real_T Saturation3;                  /* '<S6>/Saturation3' */
  real_T Switch;                       /* '<S17>/Switch' */
  real_T Switch2_n;                    /* '<S17>/Switch2' */
  real_T Gain;                         /* '<S3>/Gain' */
  real_T Saturation1;                  /* '<S3>/Saturation1' */
  real_T Add1;                         /* '<S3>/Add1' */
  real_T Add1_m;                       /* '<S5>/Add1' */
  real_T ProportionalGain;             /* '<S5>/Proportional Gain' */
  real_T Delay;                        /* '<S5>/Delay' */
  real_T Switch_h;                     /* '<S5>/Switch' */
  real_T IntegralGain;                 /* '<S5>/Integral Gain' */
  real_T FFHighSlope1;                 /* '<S14>/FFHighSlope1' */
  real_T Switch_f;                     /* '<S14>/Switch' */
  real_T FFScale;                      /* '<S14>/FFScale' */
  real_T Add;                          /* '<S5>/Add' */
  real_T Switch2_p;                    /* '<S7>/Switch2' */
  real_T Saturation2_b;                /* '<S7>/Saturation2' */
  real_T Switch3_l;                    /* '<S7>/Switch3' */
  real_T Saturation3_b;                /* '<S7>/Saturation3' */
  real_T Switch_b;                     /* '<S20>/Switch' */
  real_T Switch2_i;                    /* '<S20>/Switch2' */
  real_T Add2;                         /* '<S5>/Add2' */
  real_T ProportionalGain1;            /* '<S5>/Proportional Gain1' */
  real_T Delay1;                       /* '<S5>/Delay1' */
  real_T Switch1;                      /* '<S5>/Switch1' */
  real_T IntegralGain1;                /* '<S5>/Integral Gain1' */
  real_T FFHighSlope2;                 /* '<S14>/FFHighSlope2' */
  real_T Switch1_o;                    /* '<S14>/Switch1' */
  real_T FFScale1;                     /* '<S14>/FFScale1' */
  real_T Add3;                         /* '<S5>/Add3' */
  real_T Product;                      /* '<S5>/Product' */
  real_T Product1;                     /* '<S5>/Product1' */
  real_T Valve100100;                  /* '<S5>/Saturation' */
  real_T Valve100100_p;                /* '<S5>/Saturation1' */
  real_T Subtract1;                    /* '<S5>/Subtract1' */
  real_T Subtract2;                    /* '<S5>/Subtract2' */
  real_T TiltEnable;                   /* '<S8>/Chart1' */
  real_T TiltEnable_g;                 /* '<S8>/Chart' */
  real_T Gain_h;                       /* '<S7>/Gain' */
  real_T Add4;                         /* '<S7>/Add4' */
  real_T Gain1;                        /* '<S7>/Gain1' */
  real_T Add5;                         /* '<S7>/Add5' */
  real_T Gain_i;                       /* '<S6>/Gain' */
  real_T Add4_g;                       /* '<S6>/Add4' */
  real_T Gain1_o;                      /* '<S6>/Gain1' */
  real_T Add5_g;                       /* '<S6>/Add5' */
  real_T FFLowerSlope;                 /* '<S14>/FFLowerSlope' */
  real_T Add_f;                        /* '<S14>/Add' */
  real_T Min;                          /* '<S14>/Min' */
  real_T FFLowerSlope1;                /* '<S14>/FFLowerSlope1' */
  real_T Subtract;                     /* '<S14>/Subtract' */
  real_T Min1;                         /* '<S14>/Min1' */
  real_T FFLowerSlope2;                /* '<S14>/FFLowerSlope2' */
  real_T Add1_k;                       /* '<S14>/Add1' */
  real_T Min2;                         /* '<S14>/Min2' */
  real_T FFLowerSlope3;                /* '<S14>/FFLowerSlope3' */
  real_T Subtract1_m;                  /* '<S14>/Subtract1' */
  real_T Min3;                         /* '<S14>/Min3' */
  real_T Gain_p;                       /* '<S13>/Gain' */
  real_T Delay_o;                      /* '<S13>/Delay' */
  real_T Add_c;                        /* '<S13>/Add' */
  real_T Gain_k;                       /* '<S12>/Gain' */
  real_T Delay_j;                      /* '<S12>/Delay' */
  real_T Add_m;                        /* '<S12>/Add' */
  boolean_T Compare;                   /* '<S1>/Compare' */
  boolean_T Compare_f;                 /* '<S15>/Compare' */
  boolean_T LowerRelop1;               /* '<S17>/LowerRelop1' */
  boolean_T Compare_n;                 /* '<S16>/Compare' */
  boolean_T UpperRelop;                /* '<S17>/UpperRelop' */
  boolean_T Compare_j;                 /* '<S18>/Compare' */
  boolean_T LowerRelop1_h;             /* '<S20>/LowerRelop1' */
  boolean_T Compare_d;                 /* '<S19>/Compare' */
  boolean_T UpperRelop_j;              /* '<S20>/UpperRelop' */
  boolean_T Compare_g;                 /* '<S23>/Compare' */
  boolean_T LogicalOperator;           /* '<S8>/Logical Operator' */
} B_TiltControl_c_T;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for model 'TiltControl' */
#ifndef TiltControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Delay_DSTATE;                 /* '<S5>/Delay' */
  real_T Delay1_DSTATE;                /* '<S5>/Delay1' */
  real_T Delay_DSTATE_k;               /* '<S13>/Delay' */
  real_T Delay_DSTATE_i;               /* '<S12>/Delay' */
  real_T c;                            /* '<S8>/Chart1' */
  real_T Flash_prev;                   /* '<S8>/Chart1' */
  real_T Flash_start;                  /* '<S8>/Chart1' */
  real_T c_j;                          /* '<S8>/Chart' */
  real_T Flash_prev_e;                 /* '<S8>/Chart' */
  real_T Flash_start_a;                /* '<S8>/Chart' */
  uint8_T is_active_c2_TiltControl;    /* '<S8>/Chart1' */
  uint8_T is_c2_TiltControl;           /* '<S8>/Chart1' */
  uint8_T is_active_c1_TiltControl;    /* '<S8>/Chart' */
  uint8_T is_c1_TiltControl;           /* '<S8>/Chart' */
  boolean_T DTInt2_MODE;               /* '<S5>/DTInt2' */
  boolean_T DTInt1_MODE;               /* '<S5>/DTInt1' */
} DW_TiltControl_f_T;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

#ifndef TiltControl_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_TiltControl_T_ {
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S1>/Constant'
                                        */
  DebugBus_TiltControl DebugBus_TiltControl_Y0;/* Computed Parameter: DebugBus_TiltControl_Y0
                                                * Referenced by: '<Root>/DebugBus_TiltControl'
                                                */
  real_T Constant1_Value;              /* Expression: 0
                                        * Referenced by: '<S5>/Constant1'
                                        */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<S5>/Constant'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S12>/Delay'
                                        */
  real_T Delay_InitialCondition_m;     /* Expression: 0.0
                                        * Referenced by: '<S13>/Delay'
                                        */
  real_T default5_Value;               /* Expression: 0
                                        * Referenced by: '<S8>/default5'
                                        */
  real_T default6_Value;               /* Expression: 1
                                        * Referenced by: '<S8>/default6'
                                        */
  real_T TiltStartStop_Value;          /* Expression: 0
                                        * Referenced by: '<S8>/TiltStartStop'
                                        */
  real_T TiltModControl02_Y0;          /* Expression: [0]
                                        * Referenced by: '<Root>/TiltModControl[0-2]'
                                        */
  real_T TiltStartStop01_Y0;           /* Expression: [0]
                                        * Referenced by: '<Root>/TiltStartStop[0-1]'
                                        */
  real_T ValvePercRef_Y0;              /* Expression: [0]
                                        * Referenced by: '<Root>/ValvePercRef[%]'
                                        */
  real_T ValvePercRef1_Y0;             /* Expression: [0]
                                        * Referenced by: '<Root>/ValvePercRef[%]1'
                                        */
  real_T TiltPosRefRad_Y0;             /* Expression: [0]
                                        * Referenced by: '<Root>/TiltPosRef[Rad]'
                                        */
  real_T TiltSpeedRefRads_Y0;          /* Expression: [0]
                                        * Referenced by: '<Root>/TiltSpeedRef[Rad//s]'
                                        */
  real_T ComandoPompa01_Y0;            /* Expression: [0]
                                        * Referenced by: '<Root>/ComandoPompa[0-1]'
                                        */
  real_T EnableDaPilota_Y0;            /* Expression: [0]
                                        * Referenced by: '<Root>/EnableDaPilota'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<S8>/Switch'
                                        */
  real_T Saturation2_LowerSat;         /* Expression: 0
                                        * Referenced by: '<S6>/Saturation2'
                                        */
  real_T Saturation3_UpperSat;         /* Expression: 0
                                        * Referenced by: '<S6>/Saturation3'
                                        */
  real_T Delay_InitialCondition_j;     /* Expression: 0.0
                                        * Referenced by: '<S5>/Delay'
                                        */
  real_T Switch_Threshold_m;           /* Expression: 0
                                        * Referenced by: '<S14>/Switch'
                                        */
  real_T Saturation2_LowerSat_a;       /* Expression: 0
                                        * Referenced by: '<S7>/Saturation2'
                                        */
  real_T Saturation3_UpperSat_m;       /* Expression: 0
                                        * Referenced by: '<S7>/Saturation3'
                                        */
  real_T Delay1_InitialCondition;      /* Expression: 0.0
                                        * Referenced by: '<S5>/Delay1'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<S14>/Switch1'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 100
                                        * Referenced by: '<S5>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: -100
                                        * Referenced by: '<S5>/Saturation'
                                        */
  real_T Saturation1_UpperSat;         /* Expression: 100
                                        * Referenced by: '<S5>/Saturation1'
                                        */
  real_T Saturation1_LowerSat;         /* Expression: -100
                                        * Referenced by: '<S5>/Saturation1'
                                        */
  real_T speedPolimiPI_Value;          /* Expression: 0
                                        * Referenced by: '<S8>/speedPolimiPI'
                                        */
  real_T default1_Value;               /* Expression: 0
                                        * Referenced by: '<S8>/default1'
                                        */
  real_T TiltSpeedRef_enovia1_Value;   /* Expression: 0
                                        * Referenced by: '<S8>/TiltSpeedRef_enovia1'
                                        */
  real_T TiltSelection_Value;          /* Expression: 1
                                        * Referenced by: '<S8>/TiltSelection'
                                        */
  real_T u707_UpperSat;                /* Expression: 0.7
                                        * Referenced by: '<Root>/[-0.7 0.7]'
                                        */
  real_T u707_LowerSat;                /* Expression: -0.7
                                        * Referenced by: '<Root>/[-0.7 0.7]'
                                        */
  real_T u00100_UpperSat;              /* Expression: 100
                                        * Referenced by: '<Root>/[-100 100]'
                                        */
  real_T u00100_LowerSat;              /* Expression: -100
                                        * Referenced by: '<Root>/[-100 100]'
                                        */
  real_T u001001_UpperSat;             /* Expression: 100
                                        * Referenced by: '<Root>/[-100 100]1'
                                        */
  real_T u001001_LowerSat;             /* Expression: -100
                                        * Referenced by: '<Root>/[-100 100]1'
                                        */
  real_T ComandoPompa_Value;           /* Expression: 0
                                        * Referenced by: '<Root>/Comando Pompa'
                                        */
  uint32_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S12>/Delay'
                                        */
  uint32_T Delay_DelayLength_d;        /* Computed Parameter: Delay_DelayLength_d
                                        * Referenced by: '<S13>/Delay'
                                        */
  uint32_T Delay_DelayLength_h;        /* Computed Parameter: Delay_DelayLength_h
                                        * Referenced by: '<S5>/Delay'
                                        */
  uint32_T Delay1_DelayLength;         /* Computed Parameter: Delay1_DelayLength
                                        * Referenced by: '<S5>/Delay1'
                                        */
};

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

#ifndef TiltControl_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_TiltControl_T {
  const char_T **errorStatus;
};

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

#ifndef TiltControl_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_TiltControl_T rtm;
} MdlrefDW_TiltControl_T;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

/* Model block global parameters (default storage) */
extern real_T rtP_Debounce;            /* Variable: Debounce
                                        * Referenced by: '<S8>/Chart'
                                        */
extern real_T rtP_DebounceMaster;      /* Variable: DebounceMaster
                                        * Referenced by: '<S8>/Chart1'
                                        */
extern real_T rtP_FFHighSlope1;        /* Variable: FFHighSlope1
                                        * Referenced by: '<S14>/FFHighSlope1'
                                        */
extern real_T rtP_FFHighSlope2;        /* Variable: FFHighSlope2
                                        * Referenced by: '<S14>/FFHighSlope2'
                                        */
extern real_T rtP_FFLowerSlope1;       /* Variable: FFLowerSlope1
                                        * Referenced by:
                                        *   '<S14>/FFLowerSlope'
                                        *   '<S14>/FFLowerSlope1'
                                        */
extern real_T rtP_FFLowerSlope2;       /* Variable: FFLowerSlope2
                                        * Referenced by:
                                        *   '<S14>/FFLowerSlope2'
                                        *   '<S14>/FFLowerSlope3'
                                        */
extern real_T rtP_FFOffset1_minus;     /* Variable: FFOffset1_minus
                                        * Referenced by: '<S14>/FFOffset1'
                                        */
extern real_T rtP_FFOffset1_plus;      /* Variable: FFOffset1_plus
                                        * Referenced by: '<S14>/FFOffset'
                                        */
extern real_T rtP_FFOffset2_minus;     /* Variable: FFOffset2_minus
                                        * Referenced by: '<S14>/FFOffset3'
                                        */
extern real_T rtP_FFOffset2_plus;      /* Variable: FFOffset2_plus
                                        * Referenced by: '<S14>/FFOffset2'
                                        */
extern real_T rtP_FFScale;             /* Variable: FFScale
                                        * Referenced by:
                                        *   '<S14>/FFScale'
                                        *   '<S14>/FFScale1'
                                        */
extern real_T rtP_GainSchedGain;       /* Variable: GainSchedGain
                                        * Referenced by: '<S3>/Gain'
                                        */
extern real_T rtP_GainSchedMax;        /* Variable: GainSchedMax
                                        * Referenced by: '<S3>/Saturation1'
                                        */
extern real_T rtP_GainSchedMin;        /* Variable: GainSchedMin
                                        * Referenced by: '<S3>/Saturation1'
                                        */
extern real_T rtP_GainSched_offset;    /* Variable: GainSched_offset
                                        * Referenced by: '<S3>/Constant2'
                                        */
extern real_T rtP_Ki_speed_PI1;        /* Variable: Ki_speed_PI1
                                        * Referenced by: '<S5>/Integral Gain'
                                        */
extern real_T rtP_Ki_speed_PI2;        /* Variable: Ki_speed_PI2
                                        * Referenced by: '<S5>/Integral Gain1'
                                        */
extern real_T rtP_Kp_speed_PI1;        /* Variable: Kp_speed_PI1
                                        * Referenced by: '<S5>/Proportional Gain'
                                        */
extern real_T rtP_Kp_speed_PI2;        /* Variable: Kp_speed_PI2
                                        * Referenced by: '<S5>/Proportional Gain1'
                                        */
extern real_T rtP_SpeedThreshold;      /* Variable: SpeedThreshold
                                        * Referenced by: '<S23>/Constant'
                                        */
extern real_T rtP_Ts;                  /* Variable: Ts
                                        * Referenced by:
                                        *   '<S12>/Gain'
                                        *   '<S13>/Gain'
                                        */
extern real_T rtP_angle_limit_speed;   /* Variable: angle_limit_speed
                                        * Referenced by:
                                        *   '<S6>/Constant1'
                                        *   '<S6>/Constant4'
                                        *   '<S6>/Gain'
                                        *   '<S6>/Gain1'
                                        *   '<S7>/Constant1'
                                        *   '<S7>/Constant4'
                                        *   '<S7>/Gain'
                                        *   '<S7>/Gain1'
                                        *   '<S15>/Constant'
                                        *   '<S16>/Constant'
                                        *   '<S18>/Constant'
                                        *   '<S19>/Constant'
                                        */
extern real_T rtP_angle_max;           /* Variable: angle_max
                                        * Referenced by:
                                        *   '<S6>/Constant1'
                                        *   '<S6>/Constant4'
                                        *   '<S6>/Gain'
                                        *   '<S6>/Gain1'
                                        *   '<S7>/Constant1'
                                        *   '<S7>/Constant4'
                                        *   '<S7>/Gain'
                                        *   '<S7>/Gain1'
                                        */
extern real_T rtP_max_speed;           /* Variable: max_speed
                                        * Referenced by:
                                        *   '<S6>/Constant1'
                                        *   '<S6>/Constant2'
                                        *   '<S6>/Constant3'
                                        *   '<S6>/Constant4'
                                        *   '<S6>/Gain'
                                        *   '<S6>/Gain1'
                                        *   '<S6>/Saturation2'
                                        *   '<S6>/Saturation3'
                                        *   '<S7>/Constant1'
                                        *   '<S7>/Constant2'
                                        *   '<S7>/Constant3'
                                        *   '<S7>/Constant4'
                                        *   '<S7>/Gain'
                                        *   '<S7>/Gain1'
                                        *   '<S7>/Saturation2'
                                        *   '<S7>/Saturation3'
                                        */
extern void TiltControl_Init(real_T *rty_TiltModControl02, real_T
  *rty_TiltStartStop01, real_T *rty_ValvePercRef, real_T *rty_ValvePercRef1,
  real_T *rty_TiltPosRefRad, real_T *rty_TiltSpeedRefRads, real_T
  *rty_ComandoPompa01, real_T *rty_EnableDaPilota, DebugBus_TiltControl
  *rty_DebugBus_TiltControl);
extern void TiltControl_Start(void);
extern void TiltControl_Disable(void);
extern void TiltControl(const real_T *rtu_ValveSpeed1rads, const real_T
  *rtu_ValveSpeed2rads, const real_T *rtu_ValvePos1rad, const real_T
  *rtu_ValvePos2rad, const real_T *rtu_TiltSpeedRef1rads, const real_T
  *rtu_TiltSpeedRef2rads, const real_T *rtu_DSB_Flash, const real_T
  *rtu_BBS_RSpeedKmh, const real_T *rtu_OffButton, real_T *rty_TiltModControl02,
  real_T *rty_TiltStartStop01, real_T *rty_ValvePercRef, real_T
  *rty_ValvePercRef1, real_T *rty_TiltPosRefRad, real_T *rty_TiltSpeedRefRads,
  real_T *rty_ComandoPompa01, real_T *rty_EnableDaPilota, DebugBus_TiltControl
  *rty_DebugBus_TiltControl);

/* Model reference registration function */
extern void TiltControl_initialize(const char_T **rt_errorStatus);

#ifndef TiltControl_MDLREF_HIDE_CHILD_

extern MdlrefDW_TiltControl_T TiltControl_MdlrefDW;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

#ifndef TiltControl_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_TiltControl_c_T TiltControl_B;

/* Block states (default storage) */
extern DW_TiltControl_f_T TiltControl_DW;

#endif                                 /*TiltControl_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'TiltControl'
 * '<S1>'   : 'TiltControl/Compare To Constant1'
 * '<S2>'   : 'TiltControl/DebugBus_Creation'
 * '<S3>'   : 'TiltControl/GainScheduling'
 * '<S4>'   : 'TiltControl/RTI Data'
 * '<S5>'   : 'TiltControl/Speed controller_PI'
 * '<S6>'   : 'TiltControl/SpeedSaturation1'
 * '<S7>'   : 'TiltControl/SpeedSaturation2'
 * '<S8>'   : 'TiltControl/SwitchController'
 * '<S9>'   : 'TiltControl/RTI Data/RTI Data Store'
 * '<S10>'  : 'TiltControl/RTI Data/RTI Data Store/RTI Data Store'
 * '<S11>'  : 'TiltControl/RTI Data/RTI Data Store/RTI Data Store/RTI Data Store'
 * '<S12>'  : 'TiltControl/Speed controller_PI/DTInt1'
 * '<S13>'  : 'TiltControl/Speed controller_PI/DTInt2'
 * '<S14>'  : 'TiltControl/Speed controller_PI/FF'
 * '<S15>'  : 'TiltControl/SpeedSaturation1/Compare To Constant'
 * '<S16>'  : 'TiltControl/SpeedSaturation1/Compare To Constant1'
 * '<S17>'  : 'TiltControl/SpeedSaturation1/Saturation Dynamic'
 * '<S18>'  : 'TiltControl/SpeedSaturation2/Compare To Constant'
 * '<S19>'  : 'TiltControl/SpeedSaturation2/Compare To Constant1'
 * '<S20>'  : 'TiltControl/SpeedSaturation2/Saturation Dynamic'
 * '<S21>'  : 'TiltControl/SwitchController/Chart'
 * '<S22>'  : 'TiltControl/SwitchController/Chart1'
 * '<S23>'  : 'TiltControl/SwitchController/Compare To Constant'
 */
#endif                                 /* RTW_HEADER_TiltControl_h_ */
