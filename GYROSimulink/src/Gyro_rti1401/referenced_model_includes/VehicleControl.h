/*
 * Code generation for system model 'VehicleControl'
 * For more details, see corresponding source file VehicleControl.c
 *
 */

#ifndef RTW_HEADER_VehicleControl_h_
#define RTW_HEADER_VehicleControl_h_
#include <string.h>
#ifndef VehicleControl_COMMON_INCLUDES_
# define VehicleControl_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* VehicleControl_COMMON_INCLUDES_ */

#include "VehicleControl_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Block signals for system '<S2>/Discrete Time Integrator1' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Gain;                         /* '<S8>/Gain' */
  real_T Delay;                        /* '<S8>/Delay' */
  real_T Add;                          /* '<S8>/Add' */
  real_T Saturation;                   /* '<S8>/Saturation' */
} B_DiscreteTimeIntegrator1_Veh_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for system '<S2>/Discrete Time Integrator1' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Delay_DSTATE;                 /* '<S8>/Delay' */
  boolean_T DiscreteTimeIntegrator1_MODE;/* '<S2>/Discrete Time Integrator1' */
} DW_DiscreteTimeIntegrator1_Ve_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Block signals for system '<S4>/Int1' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Gain;                         /* '<S10>/Gain' */
  real_T Delay;                        /* '<S10>/Delay' */
  real_T Add;                          /* '<S10>/Add' */
  real_T Saturation;                   /* '<S10>/Saturation' */
} B_Int1_VehicleControl_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for system '<S4>/Int1' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Delay_DSTATE;                 /* '<S10>/Delay' */
  boolean_T Int1_MODE;                 /* '<S4>/Int1' */
} DW_Int1_VehicleControl_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Block signals for model 'VehicleControl' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Sum1;                         /* '<S2>/Sum1' */
  real_T DeadZone1;                    /* '<S2>/Dead Zone1' */
  real_T Divide1;                      /* '<S2>/Divide1' */
  real_T Product1;                     /* '<S2>/Product1' */
  real_T Sum;                          /* '<S2>/Sum' */
  real_T DeadZone;                     /* '<S2>/Dead Zone' */
  real_T Divide;                       /* '<S2>/Divide' */
  real_T Product2;                     /* '<S2>/Product2' */
  real_T Sum_h;                        /* '<S7>/Sum' */
  real_T Gain;                         /* '<S4>/Gain' */
  real_T Sum1_l;                       /* '<S7>/Sum1' */
  real_T Gain1;                        /* '<S4>/Gain1' */
  real_T Subtract1;                    /* '<S4>/Subtract1' */
  real_T Gain4;                        /* '<S4>/Gain4' */
  real_T Gain2;                        /* '<S4>/Gain2' */
  real_T Gain3;                        /* '<S4>/Gain3' */
  real_T Subtract;                     /* '<S4>/Subtract' */
  real_T Gain5;                        /* '<S4>/Gain5' */
  real_T Gain6;                        /* '<S4>/Gain6' */
  real_T Add;                          /* '<S4>/Add' */
  real_T Gain7;                        /* '<S4>/Gain7' */
  real_T Gain8;                        /* '<S4>/Gain8' */
  real_T Gain9;                        /* '<S4>/Gain9' */
  real_T Gain10;                       /* '<S4>/Gain10' */
  real_T Gain11;                       /* '<S4>/Gain11' */
  real_T Gain12;                       /* '<S4>/Gain12' */
  real_T Add1;                         /* '<S4>/Add1' */
  real_T Product1_j;                   /* '<S4>/Product1' */
  real_T Product2_j;                   /* '<S4>/Product2' */
  real_T In1;                          /* '<S15>/In1' */
  real_T TiltRef1;                     /* '<Root>/Riferimento a 0 volani' */
  real_T TiltRef2;                     /* '<Root>/Riferimento a 0 volani' */
  real_T Saturation;                   /* '<Root>/Saturation' */
  real_T Kp_pos;                       /* '<S2>/Kp_pos' */
  real_T Add3;                         /* '<S2>/Add3' */
  real_T Saturation1;                  /* '<Root>/Saturation1' */
  real_T Kp_pos1;                      /* '<S2>/Kp_pos1' */
  real_T Add1_h;                       /* '<S2>/Add1' */
  boolean_T Compare;                   /* '<S1>/Compare' */
  boolean_T LogicalOperator3;          /* '<Root>/Logical Operator3' */
  boolean_T LogicalOperator2;          /* '<Root>/Logical Operator2' */
  boolean_T LogicalOperator2_d;        /* '<S7>/Logical Operator2' */
  boolean_T LogicalOperator1;          /* '<Root>/Logical Operator1' */
  B_Int1_VehicleControl_T int_g;       /* '<S4>/int' */
  B_Int1_VehicleControl_T Int1;        /* '<S4>/Int1' */
  B_DiscreteTimeIntegrator1_Veh_T DiscreteTimeIntegrator3;/* '<S2>/Discrete Time Integrator3' */
  B_DiscreteTimeIntegrator1_Veh_T DiscreteTimeIntegrator1;/* '<S2>/Discrete Time Integrator1' */
} B_VehicleControl_c_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for model 'VehicleControl' */
#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  real_T InitialTilt1;                 /* '<Root>/Riferimento a 0 volani' */
  real_T InitialTilt2;                 /* '<Root>/Riferimento a 0 volani' */
  real_T s1;                           /* '<Root>/Riferimento a 0 volani' */
  real_T c;                            /* '<Root>/Riferimento a 0 volani' */
  real_T s2;                           /* '<Root>/Riferimento a 0 volani' */
  uint8_T is_active_c3_VehicleControl; /* '<Root>/Riferimento a 0 volani' */
  uint8_T is_c3_VehicleControl;        /* '<Root>/Riferimento a 0 volani' */
  uint8_T is_StabilizzazioneDisattiva; /* '<Root>/Riferimento a 0 volani' */
  DW_Int1_VehicleControl_T int_g;      /* '<S4>/int' */
  DW_Int1_VehicleControl_T Int1;       /* '<S4>/Int1' */
  DW_DiscreteTimeIntegrator1_Ve_T DiscreteTimeIntegrator3;/* '<S2>/Discrete Time Integrator3' */
  DW_DiscreteTimeIntegrator1_Ve_T DiscreteTimeIntegrator1;/* '<S2>/Discrete Time Integrator1' */
} DW_VehicleControl_f_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

/* Parameters for system: '<S2>/Discrete Time Integrator1' */
struct P_DiscreteTimeIntegrator1_Veh_T_ {
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S8>/Delay'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 10
                                        * Referenced by: '<S8>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: -10
                                        * Referenced by: '<S8>/Saturation'
                                        */
  uint32_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S8>/Delay'
                                        */
};

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

/* Parameters for system: '<S4>/Int1' */
struct P_Int1_VehicleControl_T_ {
  real_T out_int_Y0;                   /* Expression: 0
                                        * Referenced by: '<S10>/out_int'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S10>/Delay'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 100
                                        * Referenced by: '<S10>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: -100
                                        * Referenced by: '<S10>/Saturation'
                                        */
  uint32_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S10>/Delay'
                                        */
};

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_VehicleControl_T_ {
  real_T CompareToConstant3_const;     /* Mask Parameter: CompareToConstant3_const
                                        * Referenced by: '<S1>/Constant'
                                        */
  DebugBus_VehicleControl DebugBus_VehicleControl_Y0;/* Computed Parameter: DebugBus_VehicleControl_Y0
                                                      * Referenced by: '<Root>/DebugBus_VehicleControl'
                                                      */
  real_T Saturation1_UpperSat;         /* Expression: 1.6
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  real_T Saturation1_LowerSat;         /* Expression: -1.6
                                        * Referenced by: '<Root>/Saturation1'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 1.6
                                        * Referenced by: '<Root>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: -1.6
                                        * Referenced by: '<Root>/Saturation'
                                        */
  real_T TiltSpeedRef1rads_Y0;         /* Expression: [0]
                                        * Referenced by: '<Root>/TiltSpeedRef1 [rad//s]'
                                        */
  real_T TiltSpeedRef2rads_Y0;         /* Expression: [0]
                                        * Referenced by: '<Root>/TiltSpeedRef2 [rad//s]'
                                        */
  real_T DeadZone1_Start;              /* Expression: -0.05
                                        * Referenced by: '<S2>/Dead Zone1'
                                        */
  real_T DeadZone1_End;                /* Expression: 0.05
                                        * Referenced by: '<S2>/Dead Zone1'
                                        */
  real_T DeadZone_Start;               /* Expression: -0.05
                                        * Referenced by: '<S2>/Dead Zone'
                                        */
  real_T DeadZone_End;                 /* Expression: 0.05
                                        * Referenced by: '<S2>/Dead Zone'
                                        */
  real_T RollRateSetPoint_Value;       /* Expression: 0
                                        * Referenced by: '<S7>/RollRateSetPoint'
                                        */
  real_T Gain4_Gain;                   /* Expression: 0.5
                                        * Referenced by: '<S4>/Gain4'
                                        */
  real_T Scaling_factor01_Value;       /* Expression: 0.5
                                        * Referenced by: '<Root>/Scaling_factor[0-1]'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<Root>/Switch1'
                                        */
  real_T Switch2_Threshold;            /* Expression: 0
                                        * Referenced by: '<Root>/Switch2'
                                        */
  P_Int1_VehicleControl_T int_g;       /* '<S4>/int' */
  P_Int1_VehicleControl_T Int1;        /* '<S4>/Int1' */
  P_DiscreteTimeIntegrator1_Veh_T DiscreteTimeIntegrator3;/* '<S2>/Discrete Time Integrator3' */
  P_DiscreteTimeIntegrator1_Veh_T DiscreteTimeIntegrator1;/* '<S2>/Discrete Time Integrator1' */
};

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_VehicleControl_T {
  const char_T **errorStatus;
};

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_VehicleControl_T rtm;
} MdlrefDW_VehicleControl_T;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/* Model block global parameters (default storage) */
extern real_T rtP_K_LQR61[6];          /* Variable: K_LQR61
                                        * Referenced by:
                                        *   '<S4>/Gain'
                                        *   '<S4>/Gain1'
                                        *   '<S4>/Gain2'
                                        *   '<S4>/Gain3'
                                        *   '<S4>/Gain5'
                                        *   '<S4>/Gain6'
                                        */
extern real_T rtP_K_LQR62[6];          /* Variable: K_LQR62
                                        * Referenced by:
                                        *   '<S4>/Gain10'
                                        *   '<S4>/Gain11'
                                        *   '<S4>/Gain12'
                                        *   '<S4>/Gain7'
                                        *   '<S4>/Gain8'
                                        *   '<S4>/Gain9'
                                        */
extern real_T rtP_RampTime;            /* Variable: RampTime
                                        * Referenced by: '<Root>/Riferimento a 0 volani'
                                        */
extern real_T rtP_Ti_position;         /* Variable: Ti_position
                                        * Referenced by:
                                        *   '<S2>/Constant2'
                                        *   '<S2>/Constant4'
                                        */
extern real_T rtP_Ts;                  /* Variable: Ts
                                        * Referenced by:
                                        *   '<Root>/Riferimento a 0 volani'
                                        *   '<S8>/Gain'
                                        *   '<S9>/Gain'
                                        *   '<S10>/Gain'
                                        *   '<S11>/Gain'
                                        */
extern real_T rtP_kp_position;         /* Variable: kp_position
                                        * Referenced by:
                                        *   '<S2>/Constant1'
                                        *   '<S2>/Constant3'
                                        *   '<S2>/Kp_pos'
                                        *   '<S2>/Kp_pos1'
                                        */
extern void VehicleControl_Init(real_T *rty_TiltSpeedRef1rads, real_T
  *rty_TiltSpeedRef2rads, DebugBus_VehicleControl *rty_DebugBus_VehicleControl);
extern void VehicleControl_Start(void);
extern void VehicleControl_Disable(void);
extern void VehicleControl(const real_T *rtu_ValvePos1rad, const real_T
  *rtu_ValvePos2rad, const real_T *rtu_Rollrad, const real_T *rtu_RollRaterads,
  const real_T *rtu_TiltStartStop, const real_T *rtu_EnableStabilizzazione,
  real_T *rty_TiltSpeedRef1rads, real_T *rty_TiltSpeedRef2rads,
  DebugBus_VehicleControl *rty_DebugBus_VehicleControl);

/* Model reference registration function */
extern void VehicleControl_initialize(const char_T **rt_errorStatus);

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

extern void Ve_DiscreteTimeIntegrator1_Init(DW_DiscreteTimeIntegrator1_Ve_T
  *localDW, P_DiscreteTimeIntegrator1_Veh_T *localP);
extern void V_DiscreteTimeIntegrator1_Reset(DW_DiscreteTimeIntegrator1_Ve_T
  *localDW, P_DiscreteTimeIntegrator1_Veh_T *localP);
extern void V_DiscreteTimeIntegrator1_Start(DW_DiscreteTimeIntegrator1_Ve_T
  *localDW);
extern void DiscreteTimeIntegrator1_Disable(DW_DiscreteTimeIntegrator1_Ve_T
  *localDW);
extern void DiscreteTimeIntegrator1_Update(B_DiscreteTimeIntegrator1_Veh_T
  *localB, DW_DiscreteTimeIntegrator1_Ve_T *localDW);
extern void Vehicle_DiscreteTimeIntegrator1(boolean_T rtu_Enable, real_T
  rtu_int_in, B_DiscreteTimeIntegrator1_Veh_T *localB,
  DW_DiscreteTimeIntegrator1_Ve_T *localDW, P_DiscreteTimeIntegrator1_Veh_T
  *localP);
extern void VehicleControl_Int1_Init(B_Int1_VehicleControl_T *localB,
  DW_Int1_VehicleControl_T *localDW, P_Int1_VehicleControl_T *localP);
extern void VehicleControl_Int1_Reset(DW_Int1_VehicleControl_T *localDW,
  P_Int1_VehicleControl_T *localP);
extern void VehicleControl_Int1_Start(DW_Int1_VehicleControl_T *localDW);
extern void VehicleControl_Int1_Disable(DW_Int1_VehicleControl_T *localDW);
extern void VehicleControl_Int1_Update(B_Int1_VehicleControl_T *localB,
  DW_Int1_VehicleControl_T *localDW);
extern void VehicleControl_Int1(boolean_T rtu_Enable, real_T rtu_in_int,
  B_Int1_VehicleControl_T *localB, DW_Int1_VehicleControl_T *localDW,
  P_Int1_VehicleControl_T *localP);

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

extern MdlrefDW_VehicleControl_T VehicleControl_MdlrefDW;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

#ifndef VehicleControl_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_VehicleControl_c_T VehicleControl_B;

/* Block states (default storage) */
extern DW_VehicleControl_f_T VehicleControl_DW;

#endif                                 /*VehicleControl_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'VehicleControl'
 * '<S1>'   : 'VehicleControl/Compare To Constant3'
 * '<S2>'   : 'VehicleControl/Controllo Posizione'
 * '<S3>'   : 'VehicleControl/DebugBus_Creation'
 * '<S4>'   : 'VehicleControl/LQR6'
 * '<S5>'   : 'VehicleControl/RTI Data'
 * '<S6>'   : 'VehicleControl/Riferimento a 0 volani'
 * '<S7>'   : 'VehicleControl/RollSetPoint'
 * '<S8>'   : 'VehicleControl/Controllo Posizione/Discrete Time Integrator1'
 * '<S9>'   : 'VehicleControl/Controllo Posizione/Discrete Time Integrator3'
 * '<S10>'  : 'VehicleControl/LQR6/Int1'
 * '<S11>'  : 'VehicleControl/LQR6/int'
 * '<S12>'  : 'VehicleControl/RTI Data/RTI Data Store'
 * '<S13>'  : 'VehicleControl/RTI Data/RTI Data Store/RTI Data Store'
 * '<S14>'  : 'VehicleControl/RTI Data/RTI Data Store/RTI Data Store/RTI Data Store'
 * '<S15>'  : 'VehicleControl/RollSetPoint/RollSetPoint Smooth'
 */
#endif                                 /* RTW_HEADER_VehicleControl_h_ */
