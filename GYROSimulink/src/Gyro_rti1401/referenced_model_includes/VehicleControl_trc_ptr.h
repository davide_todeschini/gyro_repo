/*********************** dSPACE target specific file *************************

   Header file VehicleControl_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:46:17 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_VehicleControl_trc_ptr_h_
#define RTI_HEADER_VehicleControl_trc_ptr_h_

/* Include the model header file. */
#include "VehicleControl.h"
#include "VehicleControl_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile real_T *p_0_VehicleControl_real_T_0;
EXTERN_C volatile boolean_T *p_0_VehicleControl_boolean_T_1;
EXTERN_C volatile real_T *p_0_VehicleControl_real_T_2;
EXTERN_C volatile real_T *p_0_VehicleControl_real_T_3;
EXTERN_C volatile real_T *p_0_VehicleControl_real_T_4;
EXTERN_C volatile real_T *p_0_VehicleControl_real_T_5;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_0;
EXTERN_C volatile DebugBus_VehicleControl
  *p_2_VehicleControl_DebugBus_VehicleControl_1;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_2;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_3;
EXTERN_C volatile uint32_T *p_2_VehicleControl_uint32_T_4;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_5;
EXTERN_C volatile uint32_T *p_2_VehicleControl_uint32_T_6;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_7;
EXTERN_C volatile uint32_T *p_2_VehicleControl_uint32_T_8;
EXTERN_C volatile real_T *p_2_VehicleControl_real_T_9;
EXTERN_C volatile uint32_T *p_2_VehicleControl_uint32_T_10;
EXTERN_C volatile real_T *p_3_VehicleControl_real_T_0;
EXTERN_C volatile uint8_T *p_3_VehicleControl_uint8_T_1;
EXTERN_C volatile real_T *p_3_VehicleControl_real_T_2;
EXTERN_C volatile boolean_T *p_3_VehicleControl_boolean_T_3;
EXTERN_C volatile real_T *p_3_VehicleControl_real_T_4;
EXTERN_C volatile boolean_T *p_3_VehicleControl_boolean_T_5;
EXTERN_C volatile real_T *p_3_VehicleControl_real_T_6;
EXTERN_C volatile boolean_T *p_3_VehicleControl_boolean_T_7;
EXTERN_C volatile real_T *p_3_VehicleControl_real_T_8;
EXTERN_C volatile boolean_T *p_3_VehicleControl_boolean_T_9;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void VehicleControl_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_VehicleControl_trc_ptr_h_ */
