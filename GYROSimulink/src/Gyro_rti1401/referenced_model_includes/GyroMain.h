/*
 * Code generation for system model 'GyroMain'
 * For more details, see corresponding source file GyroMain.c
 *
 */

#ifndef RTW_HEADER_GyroMain_h_
#define RTW_HEADER_GyroMain_h_
#include <string.h>
#ifndef GyroMain_COMMON_INCLUDES_
# define GyroMain_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* GyroMain_COMMON_INCLUDES_ */

#include "GyroMain_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define VehicleControl_MDLREF_HIDE_CHILD_
#include "VehicleControl.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define TiltControl_MDLREF_HIDE_CHILD_
#include "TiltControl.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define SuperVisor_MDLREF_HIDE_CHILD_
#include "SuperVisor.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define SpinControl_MDLREF_HIDE_CHILD_
#include "SpinControl.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define DataProcessing_MDLREF_HIDE_CHILD_
#include "DataProcessing.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_
#define DashBoard_MDLREF_HIDE_CHILD_
#include "DashBoard.h"
#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"

/* Block signals for model 'GyroMain' */
#ifndef GyroMain_MDLREF_HIDE_CHILD_

typedef struct {
  DebugBus DebugBus_a;                 /* '<S1>/Bus Creator1' */
  OutputBus OutputBus_m;               /* '<S2>/Bus Creator1' */
  DebugBus_VehicleControl VehicleControl_o3;/* '<Root>/VehicleControl' */
  DebugBus_TiltControl TiltControl_o9; /* '<Root>/TiltControl' */
  DebugBus_DataProcessing DataProcessing_o3;/* '<Root>/DataProcessing' */
  DebugBus_SpinControl SpinControl_o5; /* '<Root>/SpinControl' */
  DebugBus_SuperVisor SuperVisor_o3;   /* '<Root>/SuperVisor' */
  real_T deg2rad;                      /* '<S3>/deg2rad' */
  real_T deg2rad1;                     /* '<S3>/deg2rad1' */
  real_T Gain1;                        /* '<S3>/Gain1' */
  real_T Gain2;                        /* '<S3>/Gain2' */
  real_T Gain3;                        /* '<S3>/Gain3' */
  real_T deg2rad2;                     /* '<S3>/deg2rad2' */
  real_T DashBoard_o1;                 /* '<Root>/DashBoard' */
  real_T DashBoard_o2;                 /* '<Root>/DashBoard' */
  real_T VehicleControl_o1;            /* '<Root>/VehicleControl' */
  real_T VehicleControl_o2;            /* '<Root>/VehicleControl' */
  real_T DataProcessing_o1;            /* '<Root>/DataProcessing' */
  real_T DataProcessing_o2;            /* '<Root>/DataProcessing' */
  real_T SuperVisor_o1;                /* '<Root>/SuperVisor' */
  real_T SuperVisor_o2;                /* '<Root>/SuperVisor' */
  real_T TiltControl_o1;               /* '<Root>/TiltControl' */
  real_T TiltControl_o2;               /* '<Root>/TiltControl' */
  real_T TiltControl_o3;               /* '<Root>/TiltControl' */
  real_T TiltControl_o4;               /* '<Root>/TiltControl' */
  real_T TiltControl_o5;               /* '<Root>/TiltControl' */
  real_T TiltControl_o6;               /* '<Root>/TiltControl' */
  real_T TiltControl_o7;               /* '<Root>/TiltControl' */
  real_T TiltControl_o8;               /* '<Root>/TiltControl' */
  real_T SpinControl_o1;               /* '<Root>/SpinControl' */
  real_T SpinControl_o2;               /* '<Root>/SpinControl' */
  real_T SpinControl_o3;               /* '<Root>/SpinControl' */
  real_T SpinControl_o4;               /* '<Root>/SpinControl' */
  DebugBus_DashBoard DashBoard_o3;     /* '<Root>/DashBoard' */
  int8_T inputevents[4];               /* '<S4>/PRIORITY_GENERATTOR' */
} B_GyroMain_c_T;

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for model 'GyroMain' */
#ifndef GyroMain_MDLREF_HIDE_CHILD_

typedef struct {
  int32_T sfEvent;                     /* '<S4>/TASK_GENERATOR' */
  int32_T sfEvent_g;                   /* '<S4>/PRIORITY_GENERATTOR' */
  uint8_T is_active_c3_GyroMain;       /* '<S4>/TASK_GENERATOR' */
  uint8_T is_TASK_1Ts;                 /* '<S4>/TASK_GENERATOR' */
  uint8_T is_TASK_2Ts;                 /* '<S4>/TASK_GENERATOR' */
  uint8_T is_TASK_10Ts;                /* '<S4>/TASK_GENERATOR' */
  uint8_T is_TASK_100Ts;               /* '<S4>/TASK_GENERATOR' */
  uint8_T temporalCounter_i1;          /* '<S4>/TASK_GENERATOR' */
  uint8_T temporalCounter_i2;          /* '<S4>/TASK_GENERATOR' */
  uint8_T temporalCounter_i3;          /* '<S4>/TASK_GENERATOR' */
  uint8_T temporalCounter_i4;          /* '<S4>/TASK_GENERATOR' */
  uint8_T is_active_c2_GyroMain;       /* '<S4>/PRIORITY_GENERATTOR' */
  boolean_T PRIORITY_GENERATTOR_MODE[4];/* '<S4>/PRIORITY_GENERATTOR' */
} DW_GyroMain_f_T;

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_GyroMain_T_ {
  real_T deg2rad_Gain;                 /* Expression: pi/180
                                        * Referenced by: '<S3>/deg2rad'
                                        */
  real_T deg2rad1_Gain;                /* Expression: pi/180
                                        * Referenced by: '<S3>/deg2rad1'
                                        */
  real_T Gain1_Gain;                   /* Expression: 9.81
                                        * Referenced by: '<S3>/Gain1'
                                        */
  real_T Gain2_Gain;                   /* Expression: 9.81
                                        * Referenced by: '<S3>/Gain2'
                                        */
  real_T Gain3_Gain;                   /* Expression: 9.81
                                        * Referenced by: '<S3>/Gain3'
                                        */
  real_T deg2rad2_Gain;                /* Expression: pi/180
                                        * Referenced by: '<S3>/deg2rad2'
                                        */
};

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_GyroMain_T {
  const char_T **errorStatus;
};

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_GyroMain_T rtm;
} MdlrefDW_GyroMain_T;

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

/* Model block global parameters (default storage) */
extern real_T rtP_Debounce;            /* Variable: Debounce
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_DebounceMaster;      /* Variable: DebounceMaster
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFHighSlope1;        /* Variable: FFHighSlope1
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFHighSlope2;        /* Variable: FFHighSlope2
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFLowerSlope1;       /* Variable: FFLowerSlope1
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFLowerSlope2;       /* Variable: FFLowerSlope2
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFOffset1_minus;     /* Variable: FFOffset1_minus
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFOffset1_plus;      /* Variable: FFOffset1_plus
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFOffset2_minus;     /* Variable: FFOffset2_minus
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFOffset2_plus;      /* Variable: FFOffset2_plus
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_FFScale;             /* Variable: FFScale
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_GainSchedGain;       /* Variable: GainSchedGain
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_GainSchedMax;        /* Variable: GainSchedMax
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_GainSchedMin;        /* Variable: GainSchedMin
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_GainSched_offset;    /* Variable: GainSched_offset
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_K_LQR61[6];          /* Variable: K_LQR61
                                        * Referenced by: '<Root>/VehicleControl'
                                        */
extern real_T rtP_K_LQR62[6];          /* Variable: K_LQR62
                                        * Referenced by: '<Root>/VehicleControl'
                                        */
extern real_T rtP_Ki_speed_PI1;        /* Variable: Ki_speed_PI1
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_Ki_speed_PI2;        /* Variable: Ki_speed_PI2
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_Kp_speed_PI1;        /* Variable: Kp_speed_PI1
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_Kp_speed_PI2;        /* Variable: Kp_speed_PI2
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_LP20_den[3];         /* Variable: LP20_den
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_LP20_num[3];         /* Variable: LP20_num
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_RampTime;            /* Variable: RampTime
                                        * Referenced by: '<Root>/VehicleControl'
                                        */
extern real_T rtP_SpeedThreshold;      /* Variable: SpeedThreshold
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_Ti_position;         /* Variable: Ti_position
                                        * Referenced by: '<Root>/VehicleControl'
                                        */
extern real_T rtP_Ts;                  /* Variable: Ts
                                        * Referenced by:
                                        *   '<Root>/DashBoard'
                                        *   '<Root>/TiltControl'
                                        *   '<Root>/VehicleControl'
                                        *   '<S4>/TASK_GENERATOR'
                                        */
extern real_T rtP_alpha_IMU;           /* Variable: alpha_IMU
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_angle_limit_speed;   /* Variable: angle_limit_speed
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_angle_max;           /* Variable: angle_max
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_den_der_Tilt[3];     /* Variable: den_der_Tilt
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_f_notch;             /* Variable: f_notch
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_kp_position;         /* Variable: kp_position
                                        * Referenced by: '<Root>/VehicleControl'
                                        */
extern real_T rtP_max_speed;           /* Variable: max_speed
                                        * Referenced by: '<Root>/TiltControl'
                                        */
extern real_T rtP_num_der_Tilt[3];     /* Variable: num_der_Tilt
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern real_T rtP_xi_notch;            /* Variable: xi_notch
                                        * Referenced by: '<Root>/DataProcessing'
                                        */
extern void GyroMain_Init(void);
extern void GyroMain_Reset(void);
extern void GyroMain_Enable(void);
extern void GyroMain_Start(void);
extern void GyroMain_Disable(void);
extern void GyroMain(const InputBus *rtu_InputBus, real_T
                     *rty_OutputBus_InputBus_GyroB_er, real_T
                     *rty_OutputBus_InputBus_GyroB_gd, real_T
                     *rty_OutputBus_InputBus_GyroB_cp, real_T
                     *rty_OutputBus_InputBus_GyroB_e4, real_T
                     *rty_OutputBus_InputBus_GyroB_ng, real_T
                     *rty_OutputBus_InputBus_GyroB_f3, real_T
                     *rty_OutputBus_InputBus_GyroB_kk, real_T
                     *rty_OutputBus_InputBus_GyroB_hn, real_T
                     *rty_OutputBus_InputBus_Gyro_kwu, real_T
                     *rty_OutputBus_InputBus_Gyro_kaf, real_T
                     *rty_OutputBus_InputBus_Gyr_kaad, real_T
                     *rty_OutputBus_InputBus_Gyro_elt, real_T
                     *rty_OutputBus_InputBus_Gyro_cjp, real_T
                     *rty_OutputBus_InputBus_Gyro_ezu, real_T
                     *rty_OutputBus_InputBus_GyroB_or, real_T
                     *rty_OutputBus_InputBus_Gyro_pdp, real_T
                     *rty_OutputBus_InputBus_GyroB_dx, real_T
                     *rty_OutputBus_InputBus_Gyro_dmm, real_T
                     *rty_OutputBus_InputBus_Gyro_gyq, real_T
                     *rty_OutputBus_InputBus_GyroB_m2, real_T
                     *rty_OutputBus_InputBus_Gyro_pdn, real_T
                     *rty_OutputBus_InputBus_IMUdat_i, real_T
                     *rty_OutputBus_InputBus_IMUda_pr, real_T
                     *rty_OutputBus_InputBus_IMUd_peh, real_T
                     *rty_OutputBus_InputBus_IMUda_o3, real_T
                     *rty_OutputBus_InputBus_IMUda_e4, real_T
                     *rty_OutputBus_InputBus_ABSda_mc, real_T
                     *rty_OutputBus_InputBus_ABSda_ju, real_T
                     *rty_OutputBus_InputBus_ABSda_po, real_T
                     *rty_OutputBus_InputBus_ABSda_m3, real_T
                     *rty_OutputBus_InputBus_ABSd_p2o, real_T
                     *rty_OutputBus_InputBus_ABSda_i3, real_T
                     *rty_OutputBus_InputBus_ABSda_ka, real_T
                     *rty_OutputBus_InputBus_ABSda_hs, real_T
                     *rty_OutputBus_InputBus_ABSda_bo, real_T
                     *rty_OutputBus_InputBus_ABSda_ng, real_T
                     *rty_OutputBus_InputBus_DSBda_gy, real_T
                     *rty_OutputBus_InputBus_DSBda_go, real_T
                     *rty_OutputBus_InputBus_DSBda_ep, real_T
                     *rty_OutputBus_InputBus_DSBda_kf, real_T
                     *rty_OutputBus_InputBus_BBSda_bn, real_T
                     *rty_OutputBus_InputBus_BBSda_c4, real_T
                     *rty_OutputBus_InputBus_BBSda_lb, real_T
                     *rty_OutputBus_InputBus_BBSda_p4, real_T
                     *rty_OutputBus_InputBus_BBSd_pyn, real_T
                     *rty_OutputBus_InputBus_BBSda_en, real_T
                     *rty_OutputBus_InputBus_BBSd_pxs, real_T
                     *rty_OutputBus_InputBus_BBSda_mk, real_T
                     *rty_OutputBus_InputBus_BBSda_nk, real_T
                     *rty_OutputBus_InputBus_BBSda_fp, real_T
                     *rty_OutputBus_InputBus_BBSda_df, real_T
                     *rty_OutputBus_InputBus_BBSd_mwr, real_T
                     *rty_OutputBus_InputBus_BBSda_h3, real_T
                     *rty_OutputBus_InputBus_BBSd_hzz, real_T
                     *rty_OutputBus_InputBus_BBSd_h23, real_T
                     *rty_OutputBus_InputBus_BBSd_hs5, real_T
                     *rty_OutputBus_InputBus_BBSda_i3, real_T
                     *rty_OutputBus_InputBus_BBSd_h5p, real_T
                     *rty_OutputBus_InputBus_BBSd_h1w, real_T
                     *rty_OutputBus_InputBus_BBSd_igq, real_T
                     *rty_OutputBus_InputBus_BBSd_dzo, real_T
                     *rty_OutputBus_InputBus_BBSda_a5, real_T
                     *rty_OutputBus_InputBus_BBSd_ido, real_T
                     *rty_OutputBus_InputBus_BBSda_bx, real_T
                     *rty_OutputBus_SpinSpeedRef, real_T
                     *rty_OutputBus_SpinCurrentRef, real_T
                     *rty_OutputBus_SpinStartStop, real_T
                     *rty_OutputBus_SpinModCont, real_T
                     *rty_OutputBus_TiltModCont, real_T
                     *rty_OutputBus_TiltStartStop, real_T
                     *rty_OutputBus_TiltValvePerc1, real_T
                     *rty_OutputBus_TiltValvePerc2, real_T
                     *rty_OutputBus_TiltPosRef, real_T
                     *rty_OutputBus_TiltSpeedRef, real_T
                     *rty_OutputBus_ComandoPompa, real_T
                     *rty_OutputBus_TiltPosPerc, real_T
                     *rty_OutputBus_DTCwork_Status, real_T
                     *rty_DebugBus_InputBus_GyroBo_ib, real_T
                     *rty_DebugBus_InputBus_GyroBo_ba, real_T
                     *rty_DebugBus_InputBus_GyroBo_ir, real_T
                     *rty_DebugBus_InputBus_GyroB_bxx, real_T
                     *rty_DebugBus_InputBus_GyroBo_da, real_T
                     *rty_DebugBus_InputBus_GyroBo_gl, real_T
                     *rty_DebugBus_InputBus_GyroB_i1k, real_T
                     *rty_DebugBus_InputBus_GyroBo_h2, real_T
                     *rty_DebugBus_InputBus_GyroB_blc, real_T
                     *rty_DebugBus_InputBus_GyroB_lrp, real_T
                     *rty_DebugBus_InputBus_GyroB_gob, real_T
                     *rty_DebugBus_InputBus_GyroBo_fq, real_T
                     *rty_DebugBus_InputBus_GyroBo_as, real_T
                     *rty_DebugBus_InputBus_GyroB_gz0, real_T
                     *rty_DebugBus_InputBus_GyroBo_cr, real_T
                     *rty_DebugBus_InputBus_GyroB_ghq, real_T
                     *rty_DebugBus_InputBus_GyroBo_ew, real_T
                     *rty_DebugBus_InputBus_GyroB_lrq, real_T
                     *rty_DebugBus_InputBus_GyroB_l2i, real_T
                     *rty_DebugBus_InputBus_GyroB_ftb, real_T
                     *rty_DebugBus_InputBus_GyroB_esb, real_T
                     *rty_DebugBus_InputBus_IMUdata_c, real_T
                     *rty_DebugBus_InputBus_IMUdat_gy, real_T
                     *rty_DebugBus_InputBus_IMUdat_lp, real_T
                     *rty_DebugBus_InputBus_IMUdat_bp, real_T
                     *rty_DebugBus_InputBus_IMUdat_i1, real_T
                     *rty_DebugBus_InputBus_ABSdata_a, real_T
                     *rty_DebugBus_InputBus_ABSdat_om, real_T
                     *rty_DebugBus_InputBus_ABSdat_iq, real_T
                     *rty_DebugBus_InputBus_ABSdat_ex, real_T
                     *rty_DebugBus_InputBus_ABSdat_de, real_T
                     *rty_DebugBus_InputBus_ABSda_do4, real_T
                     *rty_DebugBus_InputBus_ABSdat_lj, real_T
                     *rty_DebugBus_InputBus_ABSdat_kg, real_T
                     *rty_DebugBus_InputBus_ABSdat_n2, real_T
                     *rty_DebugBus_InputBus_ABSdat_ho, real_T
                     *rty_DebugBus_InputBus_DSBdata_g, real_T
                     *rty_DebugBus_InputBus_DSBdat_mx, real_T
                     *rty_DebugBus_InputBus_DSBdat_li, real_T
                     *rty_DebugBus_InputBus_DSBdat_as, real_T
                     *rty_DebugBus_InputBus_BBSdat_ik, real_T
                     *rty_DebugBus_InputBus_BBSdat_iu, real_T
                     *rty_DebugBus_InputBus_BBSdat_n5, real_T
                     *rty_DebugBus_InputBus_BBSdat_al, real_T
                     *rty_DebugBus_InputBus_BBSdat_og, real_T
                     *rty_DebugBus_InputBus_BBSda_auw, real_T
                     *rty_DebugBus_InputBus_BBSdat_lc, real_T
                     *rty_DebugBus_InputBus_BBSdat_e0, real_T
                     *rty_DebugBus_InputBus_BBSdat_do, real_T
                     *rty_DebugBus_InputBus_BBSdat_ga, real_T
                     *rty_DebugBus_InputBus_BBSdat_hb, real_T
                     *rty_DebugBus_InputBus_BBSda_hst, real_T
                     *rty_DebugBus_InputBus_BBSdat_cl, real_T
                     *rty_DebugBus_InputBus_BBSdat_pg, real_T
                     *rty_DebugBus_InputBus_BBSda_nvo, real_T
                     *rty_DebugBus_InputBus_BBSdat_ks, real_T
                     *rty_DebugBus_InputBus_BBSdat_bs, real_T
                     *rty_DebugBus_InputBus_BBSdat_j3, real_T
                     *rty_DebugBus_InputBus_BBSda_cgm, real_T
                     *rty_DebugBus_InputBus_BBSda_kpt, real_T
                     *rty_DebugBus_InputBus_BBSdat_mh, real_T
                     *rty_DebugBus_InputBus_BBSda_bab, real_T
                     *rty_DebugBus_InputBus_BBSda_kdz, real_T
                     *rty_DebugBus_InputBus_BBSda_aaj, real_T
                     *rty_DebugBus_DebugBus_SpinCon_d, real_T
                     *rty_DebugBus_DebugBus_SpinCo_lq, real_T
                     *rty_DebugBus_DebugBus_SpinCo_cb, real_T
                     *rty_DebugBus_DebugBus_SpinCo_bq, real_T
                     *rty_DebugBus_DebugBus_TiltCon_o, real_T
                     *rty_DebugBus_DebugBus_TiltCo_mk, real_T
                     *rty_DebugBus_DebugBus_TiltCo_gq, real_T
                     *rty_DebugBus_DebugBus_TiltCo_bl, real_T
                     *rty_DebugBus_DebugBus_TiltCo_au, real_T
                     *rty_DebugBus_DebugBus_TiltCo_ni, real_T
                     *rty_DebugBus_DebugBus_TiltC_a1t, real_T
                     *rty_DebugBus_DebugBus_TiltCo_ip, real_T
                     *rty_DebugBus_DebugBus_TiltCo_dw, real_T
                     *rty_DebugBus_DebugBus_TiltC_ize, real_T
                     *rty_DebugBus_DebugBus_TiltC_nls, real_T
                     *rty_DebugBus_DebugBus_TiltC_nw3, real_T
                     *rty_DebugBus_DebugBus_TiltCo_jt, real_T
                     *rty_DebugBus_DebugBus_TiltCo_kn, real_T
                     *rty_DebugBus_DebugBus_TiltC_acj, real_T
                     *rty_DebugBus_DebugBus_TiltCo_cr, real_T
                     *rty_DebugBus_DebugBus_SuperVi_k, real_T
                     *rty_DebugBus_DebugBus_SuperV_ld, real_T
                     *rty_DebugBus_DebugBus_DataPr_du, real_T
                     *rty_DebugBus_DebugBus_DataPr_in, real_T
                     *rty_DebugBus_DebugBus_DataPr_dp, real_T
                     *rty_DebugBus_DebugBus_DataPr_pw, real_T
                     *rty_DebugBus_DebugBus_DataPr_ny, real_T
                     *rty_DebugBus_DebugBus_Vehicl_ab, real_T
                     *rty_DebugBus_DebugBus_Vehicl_bo, real_T
                     *rty_DebugBus_DebugBus_Vehicl_nw, real_T
                     *rty_DebugBus_DebugBus_Vehicl_jn, real_T
                     *rty_DebugBus_DebugBus_Vehicl_l5, real_T
                     *rty_DebugBus_DebugBus_Vehic_bgs, real_T
                     *rty_DebugBus_DebugBus_Vehicl_ai, real_T
                     *rty_DebugBus_DebugBus_Vehic_jls, real_T
                     *rty_DebugBus_DebugBus_Vehic_n45, real_T
                     *rty_DebugBus_DebugBus_Vehicl_fu, real_T
                     *rty_DebugBus_DebugBus_Vehic_ad3, real_T
                     *rty_DebugBus_DebugBus_Vehic_aw5, real_T
                     *rty_DebugBus_DebugBus_Vehicl_eq, real_T
                     *rty_DebugBus_DebugBus_Vehicl_pv, real_T
                     *rty_DebugBus_DebugBus_Vehic_bkb, real_T
                     *rty_DebugBus_DebugBus_Vehicl_kk, real_T
                     *rty_DebugBus_DebugBus_Vehicl_hi, real_T
                     *rty_DebugBus_DebugBus_Vehic_e54, real_T
                     *rty_DebugBus_DebugBus_DashBoa_g);

/* Model reference registration function */
extern void GyroMain_initialize(const char_T **rt_errorStatus);

#ifndef GyroMain_MDLREF_HIDE_CHILD_

extern void GyroMa_PRIORITY_GENERATTOR_Init(void);
extern void Gyro_PRIORITY_GENERATTOR_Enable(int32_T controlPortIdx);
extern void GyroM_PRIORITY_GENERATTOR_Start(void);
extern void Gyr_PRIORITY_GENERATTOR_Disable(int32_T controlPortIdx);
extern void GyroMain_PRIORITY_GENERATTOR(int32_T controlPortIdx);

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_

extern MdlrefDW_GyroMain_T GyroMain_MdlrefDW;

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

#ifndef GyroMain_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_GyroMain_c_T GyroMain_B;

/* Block states (default storage) */
extern DW_GyroMain_f_T GyroMain_DW;

#endif                                 /*GyroMain_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'GyroMain'
 * '<S1>'   : 'GyroMain/DebugBus_Creation'
 * '<S2>'   : 'GyroMain/OutputBus_Creation'
 * '<S3>'   : 'GyroMain/Rename_Layer'
 * '<S4>'   : 'GyroMain/Tasks & priority'
 * '<S5>'   : 'GyroMain/Tasks & priority/PRIORITY_GENERATTOR'
 * '<S6>'   : 'GyroMain/Tasks & priority/TASK_GENERATOR'
 */
#endif                                 /* RTW_HEADER_GyroMain_h_ */
