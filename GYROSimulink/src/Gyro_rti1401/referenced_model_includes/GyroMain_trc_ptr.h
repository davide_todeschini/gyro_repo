/*********************** dSPACE target specific file *************************

   Header file GyroMain_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:46:38 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_GyroMain_trc_ptr_h_
#define RTI_HEADER_GyroMain_trc_ptr_h_

/* Include the model header file. */
#include "GyroMain.h"
#include "GyroMain_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile DebugBus *p_0_GyroMain_DebugBus_0;
EXTERN_C volatile OutputBus *p_0_GyroMain_OutputBus_1;
EXTERN_C volatile DebugBus_VehicleControl
  *p_0_GyroMain_DebugBus_VehicleControl_2;
EXTERN_C volatile DebugBus_TiltControl *p_0_GyroMain_DebugBus_TiltControl_3;
EXTERN_C volatile DebugBus_DataProcessing
  *p_0_GyroMain_DebugBus_DataProcessing_4;
EXTERN_C volatile DebugBus_SpinControl *p_0_GyroMain_DebugBus_SpinControl_5;
EXTERN_C volatile DebugBus_SuperVisor *p_0_GyroMain_DebugBus_SuperVisor_6;
EXTERN_C volatile real_T *p_0_GyroMain_real_T_7;
EXTERN_C volatile DebugBus_DashBoard *p_0_GyroMain_DebugBus_DashBoard_8;
EXTERN_C volatile int8_T *p_0_GyroMain_int8_T_9;
EXTERN_C volatile real_T *p_2_GyroMain_real_T_0;
EXTERN_C volatile int32_T *p_3_GyroMain_int32_T_0;
EXTERN_C volatile uint8_T *p_3_GyroMain_uint8_T_1;
EXTERN_C volatile boolean_T *p_3_GyroMain_boolean_T_2;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void GyroMain_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_GyroMain_trc_ptr_h_ */
