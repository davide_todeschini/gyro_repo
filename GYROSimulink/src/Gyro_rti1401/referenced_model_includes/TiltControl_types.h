/*
 * TiltControl_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "TiltControl".
 *
 * Model version              : 1.598
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:46:00 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_TiltControl_types_h_
#define RTW_HEADER_TiltControl_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_DebugBus_TiltControl_
#define DEFINED_TYPEDEF_FOR_DebugBus_TiltControl_

typedef struct {
  real_T DBG_ScalingFactor_PI;
  real_T DBG_TiltSpeedRef;
  real_T DBG_TiltPosRef;
  real_T DBG_EnableDaPilota;
  real_T DBG_TiltPercRef1;
  real_T DBG_TiltModCont;
  real_T DBG_TiltStartStop;
  real_T DBG_TiltPercRef2;
  real_T DBG_ComandoPompa;
  real_T DBG_TiltPercRef1_PI;
  real_T DBG_TiltPercRef2_PI;
  real_T DBG_FF1;
  real_T DBG_FF2;
  real_T DBG_TiltSpeedRef1;
  real_T DBG_TiltSpeedRef2;
  real_T DBG_MasterEnable;
} DebugBus_TiltControl;

#endif

/* Parameters (default storage) */
typedef struct P_TiltControl_T_ P_TiltControl_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_TiltControl_T RT_MODEL_TiltControl_T;

#endif                                 /* RTW_HEADER_TiltControl_types_h_ */
