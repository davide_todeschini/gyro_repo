/*
 * Code generation for system model 'DataProcessing'
 * For more details, see corresponding source file DataProcessing.c
 *
 */

#ifndef RTW_HEADER_DataProcessing_h_
#define RTW_HEADER_DataProcessing_h_
#include <math.h>
#include <string.h>
#ifndef DataProcessing_COMMON_INCLUDES_
# define DataProcessing_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* DataProcessing_COMMON_INCLUDES_ */

#include "DataProcessing_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"

/* Block signals for model 'DataProcessing' */
#ifndef DataProcessing_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Gain1;                        /* '<Root>/Gain1' */
  real_T Gain;                         /* '<Root>/Gain' */
  real_T Gain_a;                       /* '<S2>/Gain' */
  real_T U0;                           /* '<S2>/U0' */
  real_T Divide;                       /* '<S2>/Divide' */
  real_T U1;                           /* '<S2>/U-1' */
  real_T UnitDelay;                    /* '<S2>/Unit Delay' */
  real_T Divide1;                      /* '<S2>/Divide1' */
  real_T U2;                           /* '<S2>/U-2' */
  real_T UnitDelay1;                   /* '<S2>/Unit Delay1' */
  real_T Divide2;                      /* '<S2>/Divide2' */
  real_T Add;                          /* '<S2>/Add' */
  real_T Y1;                           /* '<S2>/Y-1' */
  real_T UnitDelay4;                   /* '<S2>/Unit Delay4' */
  real_T Divide4;                      /* '<S2>/Divide4' */
  real_T Y2;                           /* '<S2>/Y-2' */
  real_T UnitDelay3;                   /* '<S2>/Unit Delay3' */
  real_T Divide3;                      /* '<S2>/Divide3' */
  real_T Add1;                         /* '<S2>/Add1' */
  real_T Sum;                          /* '<S2>/Sum' */
  real_T Gain_m;                       /* '<S3>/Gain' */
  real_T U0_e;                         /* '<S3>/U0' */
  real_T Divide_o;                     /* '<S3>/Divide' */
  real_T U1_a;                         /* '<S3>/U-1' */
  real_T UnitDelay_e;                  /* '<S3>/Unit Delay' */
  real_T Divide1_h;                    /* '<S3>/Divide1' */
  real_T U2_k;                         /* '<S3>/U-2' */
  real_T UnitDelay1_o;                 /* '<S3>/Unit Delay1' */
  real_T Divide2_p;                    /* '<S3>/Divide2' */
  real_T Add_n;                        /* '<S3>/Add' */
  real_T Y1_m;                         /* '<S3>/Y-1' */
  real_T UnitDelay4_j;                 /* '<S3>/Unit Delay4' */
  real_T Divide4_f;                    /* '<S3>/Divide4' */
  real_T Y2_k;                         /* '<S3>/Y-2' */
  real_T UnitDelay3_a;                 /* '<S3>/Unit Delay3' */
  real_T Divide3_h;                    /* '<S3>/Divide3' */
  real_T Add1_i;                       /* '<S3>/Add1' */
  real_T Sum_f;                        /* '<S3>/Sum' */
} B_DataProcessing_c_T;

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for model 'DataProcessing' */
#ifndef DataProcessing_MDLREF_HIDE_CHILD_

typedef struct {
  real_T UnitDelay_DSTATE;             /* '<S2>/Unit Delay' */
  real_T UnitDelay1_DSTATE;            /* '<S2>/Unit Delay1' */
  real_T UnitDelay4_DSTATE;            /* '<S2>/Unit Delay4' */
  real_T UnitDelay3_DSTATE;            /* '<S2>/Unit Delay3' */
  real_T LP20_filter3_states[2];       /* '<Root>/LP20_filter3' */
  real_T UnitDelay_DSTATE_n;           /* '<S3>/Unit Delay' */
  real_T UnitDelay1_DSTATE_p;          /* '<S3>/Unit Delay1' */
  real_T UnitDelay4_DSTATE_b;          /* '<S3>/Unit Delay4' */
  real_T UnitDelay3_DSTATE_h;          /* '<S3>/Unit Delay3' */
  real_T LP20_filter2_states[2];       /* '<Root>/LP20_filter2' */
  real_T derivativewith2ndorderPBat15Hz_[2];/* '<Root>/derivative with 2nd order PB at 15Hz' */
  real_T derivativewith2ndorderPBat15Hz1[2];/* '<Root>/derivative with 2nd order PB at 15Hz1' */
  real_T LP20_filter3_tmp;             /* '<Root>/LP20_filter3' */
  real_T LP20_filter2_tmp;             /* '<Root>/LP20_filter2' */
  real_T derivativewith2ndorderPBat15H_j;/* '<Root>/derivative with 2nd order PB at 15Hz' */
  real_T derivativewith2ndorderPBat15H_n;/* '<Root>/derivative with 2nd order PB at 15Hz1' */
} DW_DataProcessing_f_T;

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

#ifndef DataProcessing_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_DataProcessing_T_ {
  DebugBus_DataProcessing DebugBus_DataProcessing_Y0;/* Computed Parameter: DebugBus_DataProcessing_Y0
                                                      * Referenced by: '<Root>/DebugBus_DataProcessing'
                                                      */
  real_T RollRaterads_Y0;              /* Expression: [0]
                                        * Referenced by: '<Root>/RollRate [rad//s]'
                                        */
  real_T DBG_rollrad_Y0;               /* Expression: [0]
                                        * Referenced by: '<Root>/DBG_roll [rad]'
                                        */
  real_T Gain_Gain;                    /* Expression: -sqrt(2)
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Gain_Gain_f;                  /* Expression: 2*pi
                                        * Referenced by: '<S2>/Gain'
                                        */
  real_T UnitDelay_InitialCondition;   /* Expression: 0
                                        * Referenced by: '<S2>/Unit Delay'
                                        */
  real_T UnitDelay1_InitialCondition;  /* Expression: 0
                                        * Referenced by: '<S2>/Unit Delay1'
                                        */
  real_T UnitDelay4_InitialCondition;  /* Expression: 0
                                        * Referenced by: '<S2>/Unit Delay4'
                                        */
  real_T UnitDelay3_InitialCondition;  /* Expression: 0
                                        * Referenced by: '<S2>/Unit Delay3'
                                        */
  real_T LP20_filter3_InitialStates;   /* Expression: 0
                                        * Referenced by: '<Root>/LP20_filter3'
                                        */
  real_T Gain_Gain_n;                  /* Expression: 2*pi
                                        * Referenced by: '<S3>/Gain'
                                        */
  real_T UnitDelay_InitialCondition_i; /* Expression: 0
                                        * Referenced by: '<S3>/Unit Delay'
                                        */
  real_T UnitDelay1_InitialCondition_p;/* Expression: 0
                                        * Referenced by: '<S3>/Unit Delay1'
                                        */
  real_T UnitDelay4_InitialCondition_p;/* Expression: 0
                                        * Referenced by: '<S3>/Unit Delay4'
                                        */
  real_T UnitDelay3_InitialCondition_a;/* Expression: 0
                                        * Referenced by: '<S3>/Unit Delay3'
                                        */
  real_T LP20_filter2_InitialStates;   /* Expression: 0
                                        * Referenced by: '<Root>/LP20_filter2'
                                        */
  real_T derivativewith2ndorderPBat15Hz_;/* Expression: 0
                                          * Referenced by: '<Root>/derivative with 2nd order PB at 15Hz'
                                          */
  real_T derivativewith2ndorderPBat15Hz1;/* Expression: 0
                                          * Referenced by: '<Root>/derivative with 2nd order PB at 15Hz1'
                                          */
};

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

#ifndef DataProcessing_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_DataProcessing_T {
  const char_T **errorStatus;
};

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

#ifndef DataProcessing_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_DataProcessing_T rtm;
} MdlrefDW_DataProcessing_T;

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

/* Model block global parameters (default storage) */
extern real_T rtP_LP20_den[3];         /* Variable: LP20_den
                                        * Referenced by:
                                        *   '<Root>/LP20_filter2'
                                        *   '<Root>/LP20_filter3'
                                        */
extern real_T rtP_LP20_num[3];         /* Variable: LP20_num
                                        * Referenced by:
                                        *   '<Root>/LP20_filter2'
                                        *   '<Root>/LP20_filter3'
                                        */
extern real_T rtP_alpha_IMU;           /* Variable: alpha_IMU
                                        * Referenced by: '<Root>/Gain1'
                                        */
extern real_T rtP_den_der_Tilt[3];     /* Variable: den_der_Tilt
                                        * Referenced by:
                                        *   '<Root>/derivative with 2nd order PB at 15Hz'
                                        *   '<Root>/derivative with 2nd order PB at 15Hz1'
                                        */
extern real_T rtP_f_notch;             /* Variable: f_notch
                                        * Referenced by:
                                        *   '<S2>/FrequencyNotch'
                                        *   '<S3>/FrequencyNotch'
                                        */
extern real_T rtP_num_der_Tilt[3];     /* Variable: num_der_Tilt
                                        * Referenced by:
                                        *   '<Root>/derivative with 2nd order PB at 15Hz'
                                        *   '<Root>/derivative with 2nd order PB at 15Hz1'
                                        */
extern real_T rtP_xi_notch;            /* Variable: xi_notch
                                        * Referenced by:
                                        *   '<S2>/FrequencyNotch1'
                                        *   '<S3>/FrequencyNotch1'
                                        */
extern void DataProcessing_Init(real_T *rty_RollRaterads, real_T
  *rty_DBG_rollrad, DebugBus_DataProcessing *rty_DebugBus_DataProcessing);
extern void DataProcessing(const real_T *rtu_IMU_Gzrads, const real_T
  *rtu_Rollrad, const real_T *rtu_TiltPosition1Rad, const real_T
  *rtu_TiltPosition2Rad, real_T *rty_RollRaterads, real_T *rty_DBG_rollrad,
  DebugBus_DataProcessing *rty_DebugBus_DataProcessing);

/* Model reference registration function */
extern void DataProcessing_initialize(const char_T **rt_errorStatus);

#ifndef DataProcessing_MDLREF_HIDE_CHILD_

extern MdlrefDW_DataProcessing_T DataProcessing_MdlrefDW;

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

#ifndef DataProcessing_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_DataProcessing_c_T DataProcessing_B;

/* Block states (default storage) */
extern DW_DataProcessing_f_T DataProcessing_DW;

#endif                                 /*DataProcessing_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'DataProcessing'
 * '<S1>'   : 'DataProcessing/DebugBus_Creation'
 * '<S2>'   : 'DataProcessing/Notch filter'
 * '<S3>'   : 'DataProcessing/Notch filter1'
 */
#endif                                 /* RTW_HEADER_DataProcessing_h_ */
