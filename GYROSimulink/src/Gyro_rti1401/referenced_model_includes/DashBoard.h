/*
 * Code generation for system model 'DashBoard'
 * For more details, see corresponding source file DashBoard.c
 *
 */

#ifndef RTW_HEADER_DashBoard_h_
#define RTW_HEADER_DashBoard_h_
#include <string.h>
#ifndef DashBoard_COMMON_INCLUDES_
# define DashBoard_COMMON_INCLUDES_
#include <rti_assert.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* DashBoard_COMMON_INCLUDES_ */

#include "DashBoard_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Block signals for model 'DashBoard' */
#ifndef DashBoard_MDLREF_HIDE_CHILD_

typedef struct {
  real_T Sum;                          /* '<Root>/Sum' */
  real_T TiltPosMean;                  /* '<Root>/Gain' */
  real_T radTo;                        /* '<Root>/radTo%' */
  real_T Sum1;                         /* '<Root>/Sum1' */
  real_T StatoSpia;                    /* '<Root>/Chart' */
  real_T Switch1;                      /* '<Root>/Switch1' */
} B_DashBoard_c_T;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

/* Block states (default storage) for model 'DashBoard' */
#ifndef DashBoard_MDLREF_HIDE_CHILD_

typedef struct {
  real_T cont;                         /* '<Root>/Chart' */
  uint8_T is_active_c3_DashBoard;      /* '<Root>/Chart' */
  uint8_T is_c3_DashBoard;             /* '<Root>/Chart' */
} DW_DashBoard_f_T;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

#ifndef DashBoard_MDLREF_HIDE_CHILD_

/* Parameters (default storage) */
struct P_DashBoard_T_ {
  DebugBus_DashBoard DebugBus_DashBoard_Y0;/* Computed Parameter: DebugBus_DashBoard_Y0
                                            * Referenced by: '<Root>/DebugBus_DashBoard'
                                            */
  real_T Constant1_Value;              /* Expression: 0
                                        * Referenced by: '<Root>/Constant1'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<Root>/Switch1'
                                        */
  real_T Constant_Value;               /* Expression: 1
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T PosizioneVolani_Y0;           /* Expression: [0]
                                        * Referenced by: '<Root>/PosizioneVolani[%]'
                                        */
  real_T StatoSpia_Y0;                 /* Expression: [0]
                                        * Referenced by: '<Root>/StatoSpia '
                                        */
  real_T Constant2_Value;              /* Expression: 50
                                        * Referenced by: '<Root>/Constant2'
                                        */
  real_T Gain_Gain;                    /* Expression: 0.5
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T radTo_Gain;                   /* Expression: 50/0.65
                                        * Referenced by: '<Root>/radTo%'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 100
                                        * Referenced by: '<Root>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: 0
                                        * Referenced by: '<Root>/Saturation'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<Root>/Switch'
                                        */
};

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

#ifndef DashBoard_MDLREF_HIDE_CHILD_

/* Real-time Model Data Structure */
struct tag_RTM_DashBoard_T {
  const char_T **errorStatus;
};

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

#ifndef DashBoard_MDLREF_HIDE_CHILD_

typedef struct {
  RT_MODEL_DashBoard_T rtm;
} MdlrefDW_DashBoard_T;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

/* Model block global parameters (default storage) */
extern real_T rtP_Ts;                  /* Variable: Ts
                                        * Referenced by: '<Root>/Chart'
                                        */
extern void DashBoard_Init(real_T *rty_PosizioneVolani, real_T *rty_StatoSpia,
  DebugBus_DashBoard *rty_DebugBus_DashBoard);
extern void DashBoard(const real_T *rtu_ValvePos1rad, const real_T
                      *rtu_ValvePos2rad, const real_T *rtu_PulsanteAbilitazione,
                      const real_T *rtu_VolaniAbilitati, real_T
                      *rty_PosizioneVolani, real_T *rty_StatoSpia,
                      DebugBus_DashBoard *rty_DebugBus_DashBoard);

/* Model reference registration function */
extern void DashBoard_initialize(const char_T **rt_errorStatus);

#ifndef DashBoard_MDLREF_HIDE_CHILD_

extern MdlrefDW_DashBoard_T DashBoard_MdlrefDW;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

#ifndef DashBoard_MDLREF_HIDE_CHILD_

/* Block signals (default storage) */
extern B_DashBoard_c_T DashBoard_B;

/* Block states (default storage) */
extern DW_DashBoard_f_T DashBoard_DW;

#endif                                 /*DashBoard_MDLREF_HIDE_CHILD_*/

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'DashBoard'
 * '<S1>'   : 'DashBoard/Chart'
 * '<S2>'   : 'DashBoard/DebugBus_Creation'
 */
#endif                                 /* RTW_HEADER_DashBoard_h_ */
