/*********************** dSPACE target specific file *************************

   Header file SpinControl_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:45:29 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_SpinControl_trc_ptr_h_
#define RTI_HEADER_SpinControl_trc_ptr_h_

/* Include the model header file. */
#include "SpinControl.h"
#include "SpinControl_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile real_T *p_0_SpinControl_real_T_0;
EXTERN_C volatile DebugBus_SpinControl *p_1_SpinControl_DebugBus_SpinControl_0;
EXTERN_C volatile real_T *p_1_SpinControl_real_T_1;
EXTERN_C volatile boolean_T *p_2_SpinControl_boolean_T_0;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void SpinControl_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_SpinControl_trc_ptr_h_ */
