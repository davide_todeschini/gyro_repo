/*
 * DataProcessing_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "DataProcessing".
 *
 * Model version              : 1.367
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:45:15 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DataProcessing_types_h_
#define RTW_HEADER_DataProcessing_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_DebugBus_DataProcessing_
#define DEFINED_TYPEDEF_FOR_DebugBus_DataProcessing_

typedef struct {
  real_T DBG_TiltSpeedEstimate1;
  real_T DBG_TiltSpeedEstimate2;
  real_T DBG_RollRate;
  real_T DBG_RollRate_GxGz;
  real_T DBG_Roll;
} DebugBus_DataProcessing;

#endif

/* Parameters (default storage) */
typedef struct P_DataProcessing_T_ P_DataProcessing_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_DataProcessing_T RT_MODEL_DataProcessing_T;

#endif                                 /* RTW_HEADER_DataProcessing_types_h_ */
