/*********************** dSPACE target specific file *************************

   Header file TiltControl_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:46:00 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_TiltControl_trc_ptr_h_
#define RTI_HEADER_TiltControl_trc_ptr_h_

/* Include the model header file. */
#include "TiltControl.h"
#include "TiltControl_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile real_T *p_0_TiltControl_real_T_0;
EXTERN_C volatile boolean_T *p_0_TiltControl_boolean_T_1;
EXTERN_C volatile real_T *p_2_TiltControl_real_T_0;
EXTERN_C volatile DebugBus_TiltControl *p_2_TiltControl_DebugBus_TiltControl_1;
EXTERN_C volatile real_T *p_2_TiltControl_real_T_2;
EXTERN_C volatile uint32_T *p_2_TiltControl_uint32_T_3;
EXTERN_C volatile real_T *p_3_TiltControl_real_T_0;
EXTERN_C volatile uint8_T *p_3_TiltControl_uint8_T_1;
EXTERN_C volatile boolean_T *p_3_TiltControl_boolean_T_2;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void TiltControl_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_TiltControl_trc_ptr_h_ */
