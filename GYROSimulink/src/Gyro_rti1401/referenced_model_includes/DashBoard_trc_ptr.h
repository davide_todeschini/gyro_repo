/*********************** dSPACE target specific file *************************

   Header file DashBoard_trc_ptr.h:

   Declaration of function that initializes the global TRC pointers

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:44:53 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/
#ifndef RTI_HEADER_DashBoard_trc_ptr_h_
#define RTI_HEADER_DashBoard_trc_ptr_h_

/* Include the model header file. */
#include "DashBoard.h"
#include "DashBoard_private.h"
#ifdef EXTERN_C
#undef EXTERN_C
#endif

#ifdef __cplusplus
#define EXTERN_C                       extern "C"
#else
#define EXTERN_C                       extern
#endif

/*
 *  Declare the global TRC pointers
 */
EXTERN_C volatile real_T *p_0_DashBoard_real_T_0;
EXTERN_C volatile DebugBus_DashBoard *p_2_DashBoard_DebugBus_DashBoard_0;
EXTERN_C volatile real_T *p_2_DashBoard_real_T_1;
EXTERN_C volatile real_T *p_3_DashBoard_real_T_0;
EXTERN_C volatile uint8_T *p_3_DashBoard_uint8_T_1;

/*
 *  Declare the general function for TRC pointer initialization
 */
EXTERN_C void DashBoard_rti_init_trc_pointers(void);

#endif                                 /* RTI_HEADER_DashBoard_trc_ptr_h_ */
