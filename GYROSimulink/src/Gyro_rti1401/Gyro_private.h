/*
 * Gyro_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Gyro".
 *
 * Model version              : 1.805
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:47:11 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Gyro_private_h_
#define RTW_HEADER_Gyro_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "Gyro.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

/* ...  variable for information on a CAN channel */
extern can_tp1_canChannel* can_type1_channel_M1_C1;

/* ...  variable for information on a CAN channel */
extern can_tp1_canChannel* can_type1_channel_M1_C2;

/* ... definition of message variable for the RTICAN blocks */
#define CANTP1_M1_NUMMSG               19

extern can_tp1_canMsg* can_type1_msg_M1[CANTP1_M1_NUMMSG];

/* ... variable for taskqueue error checking                  */
extern Int32 rtican_type1_tq_error[CAN_TYPE1_NUM_MODULES]
  [CAN_TYPE1_NUM_TASKQUEUES];

/* Declaration of user indices (CAN_Type1_M1) */
#define CANTP1_M1_C1_TX_STD_0X18       0
#define TX_C1_STD_0X18                 0
#undef TX_C1_STD_0X18
#define CANTP1_M1_C1_TX_STD_0X19       1
#define TX_C1_STD_0X19                 1
#undef TX_C1_STD_0X19
#define CANTP1_M1_C1_RX_STD_0X174      2
#define RX_C1_STD_0X174                2
#undef RX_C1_STD_0X174
#define CANTP1_M1_C1_RX_STD_0X178      3
#define RX_C1_STD_0X178                3
#undef RX_C1_STD_0X178
#define CANTP1_M1_C1_RX_STD_0X17C      4
#define RX_C1_STD_0X17C                4
#undef RX_C1_STD_0X17C
#define CANTP1_M1_C1_RX_STD_0X22       5
#define RX_C1_STD_0X22                 5
#undef RX_C1_STD_0X22
#define CANTP1_M1_C1_RX_STD_0X23       6
#define RX_C1_STD_0X23                 6
#undef RX_C1_STD_0X23
#define CANTP1_M1_C1_RX_STD_0X188      7
#define RX_C1_STD_0X188                7
#undef RX_C1_STD_0X188
#define CANTP1_M1_C1_RX_STD_0X20       8
#define RX_C1_STD_0X20                 8
#undef RX_C1_STD_0X20
#define CANTP1_M1_C1_RX_STD_0X118      9
#define RX_C1_STD_0X118                9
#undef RX_C1_STD_0X118
#define CANTP1_M1_C1_RX_STD_0X119      10
#define RX_C1_STD_0X119                10
#undef RX_C1_STD_0X119
#define CANTP1_M1_C1_RX_STD_0X300      11
#define RX_C1_STD_0X300                11
#undef RX_C1_STD_0X300
#define CANTP1_M1_C1_RX_STD_0X230      12
#define RX_C1_STD_0X230                12
#undef RX_C1_STD_0X230
#define CANTP1_M1_C2_TX_STD_0X2B0      13
#define TX_C2_STD_0X2B0                13
#undef TX_C2_STD_0X2B0
#define CANTP1_M1_C2_TX_STD_0X2B1      14
#define TX_C2_STD_0X2B1                14
#undef TX_C2_STD_0X2B1
#define CANTP1_M1_C2_RX_STD_0X1B0      15
#define RX_C2_STD_0X1B0                15
#undef RX_C2_STD_0X1B0
#define CANTP1_M1_C2_RX_STD_0X1B2      16
#define RX_C2_STD_0X1B2                16
#undef RX_C2_STD_0X1B2
#define CANTP1_M1_C2_RX_STD_0X1B1      17
#define RX_C2_STD_0X1B1                17
#undef RX_C2_STD_0X1B1
#define CANTP1_M1_C2_RX_STD_0X1B3      18
#define RX_C2_STD_0X1B3                18
#undef RX_C2_STD_0X1B3

/* predefine needed TX-definition code to support TX-Custom code */
extern can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C1_STD;
extern can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C1_XTD;

/* predefine needed TX-definition code to support TX-Custom code */
extern can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C2_STD;
extern can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C2_XTD;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_DriverStatus_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValveSpeed2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValveSpeed1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValvePos2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValvePos1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_Current2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_Current1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_SpinSpeed2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_SpinSpeed1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_PistonErr_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValvePerc2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_TempMot2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_ValvePerc1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_PistonPress_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 IMU_YawRate_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 IMU_Ay_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 IMU_RollRate_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 IMU_Ax_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 IMU_Az_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 ABS_Roll_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DSB_DTCLevel_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DSB_Flash_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_TempMot1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DSB_RidingMode_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_SpinSpeedRef_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_SpinCurrentRef_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_SpinStartStop_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_SpinModCont_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedRef_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltPosRef_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltModCont_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltStartStop_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_EnableTilt_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_DriverErr1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_EnableSpin_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_RollRate_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_RollRate_GxGz_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 ABS_Pitch_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 ABS_PitchRate_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedEstimate1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedEstimate2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltPercRef1_PI_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltPercRef2_PI_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltPercRef1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_DriverErr2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltPercRef2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_ComandoPompa_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_ScalingFactor_PI_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedRef1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedRef2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_FF1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_FF2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_Roll_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedRefH2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_ScalingFactor_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_MotorTempErr1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedRefH1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_RollSetPoint_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_6_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_6_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_3_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_4_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR62_5_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_MotorTempErr2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeed2LQR6_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_2_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_3_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeed1LQR6_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_4_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_TiltSpeedLQR61_5_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 BBS_RSpeed_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_EnableDaPilota_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 DBG_MasterEnable_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_MotorVoltErr1_Index_UsbFlRec;

/* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
extern UInt16 GYRO_MotorVoltErr2_Index_UsbFlRec;
extern void Gyro_MATLABFunction(uint8_T rtu_byte0, uint8_T rtu_byte1, uint8_T
  rtu_byte2, uint8_T rtu_byte3, uint8_T rtu_byte4, uint8_T rtu_byte5, uint8_T
  rtu_byte6, B_MATLABFunction_Gyro_T *localB);
extern void Gyro_Counter_Init(B_Counter_Gyro_T *localB, DW_Counter_Gyro_T
  *localDW);
extern void Gyro_Counter(B_Counter_Gyro_T *localB, DW_Counter_Gyro_T *localDW);
extern void Gyro_MonitorMessages_Init(void);
extern void Gyro_MonitorMessages(void);
extern void Gyro_SpinTiltMessages(void);
extern void Gyro_Log1(void);
extern void Gyro_MonitorMessages_Term(void);
extern void Gyro_SpinTiltMessages_Term(void);

#endif                                 /* RTW_HEADER_Gyro_private_h_ */
