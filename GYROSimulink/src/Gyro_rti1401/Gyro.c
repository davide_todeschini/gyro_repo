/*
 * Gyro.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Gyro".
 *
 * Model version              : 1.805
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:47:11 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Gyro_trc_ptr.h"
#include "Gyro.h"
#include "Gyro_private.h"
#include "rt_powd_snf.h"
#include "rt_roundd_snf.h"

/* Named constants for Chart: '<S28>/F_TASK_GENERATOR' */
#define Gyro_CALL_EVENT_i              (-1)
#define Gyro_IN_NO_ACTIVE_CHILD_a      ((uint8_T)0U)
#define Gyro_IN_Start_up_1             ((uint8_T)1U)
#define Gyro_IN_Start_up_10            ((uint8_T)1U)
#define Gyro_IN_Start_up_100           ((uint8_T)1U)
#define Gyro_IN_Start_up_2             ((uint8_T)1U)
#define Gyro_IN_Start_up_20            ((uint8_T)1U)
#define Gyro_IN_Start_up_200           ((uint8_T)1U)
#define Gyro_IN_State_1                ((uint8_T)2U)
#define Gyro_IN_State_10               ((uint8_T)2U)
#define Gyro_IN_State_100              ((uint8_T)2U)
#define Gyro_IN_State_2                ((uint8_T)2U)
#define Gyro_IN_State_20               ((uint8_T)2U)
#define Gyro_IN_State_200              ((uint8_T)2U)

/* Named constants for Chart: '<S4>/TASK_LOG_GENERATOR' */
#define Gyro_IN_Start_up               ((uint8_T)1U)

/* Block signals (default storage) */
B_Gyro_T Gyro_B;

/* Block states (default storage) */
DW_Gyro_T Gyro_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_Gyro_T Gyro_PrevZCX;

/* Real-time model */
RT_MODEL_Gyro_T Gyro_M_;
RT_MODEL_Gyro_T *const Gyro_M = &Gyro_M_;

/* Forward declaration for local functions */
static void Gyro_TASK_10ms(void);
static void Gyro_TASK_20ms(void);
static void Gyro_TASK_100ms(void);
static void Gyro_TASK_200ms(void);
static void Gyro_TASK_1tick(void);
static void Gyro_TASK_2ms(void);
static void Gyro_enter_internal_TASK_10ms(void);
static void Gyro_enter_internal_TASK_20ms(void);
static void Gyro_enter_internal_TASK_100ms(void);
static void Gyro_enter_internal_TASK_200ms(void);
static void Gyro_enter_internal_TASK_1tick(void);
static void Gyro_enter_internal_TASK_2ms(void);

/*
 * Output and update for atomic system:
 *    '<S32>/MATLAB Function'
 *    '<S57>/MATLAB Function'
 */
void Gyro_MATLABFunction(uint8_T rtu_byte0, uint8_T rtu_byte1, uint8_T rtu_byte2,
  uint8_T rtu_byte3, uint8_T rtu_byte4, uint8_T rtu_byte5, uint8_T rtu_byte6,
  B_MATLABFunction_Gyro_T *localB)
{
  uint8_T byte_array[7];
  real_T Crc8_ucr;
  int32_T b_index;
  int8_T x[8];
  int32_T skj;
  static const char_T Crc8Tab[512] = { '0', '1', '3', '2', '7', '6', '4', '5',
    'E', 'F', 'D', 'C', '9', '8', 'A', 'B', 'C', 'D', 'F', 'E', 'B', 'A', '8',
    '9', '2', '3', '1', '0', '5', '4', '6', '7', '8', '9', 'B', 'A', 'F', 'E',
    'C', 'D', '6', '7', '5', '4', '1', '0', '2', '3', '4', '5', '7', '6', '3',
    '2', '0', '1', 'A', 'B', '9', '8', 'D', 'C', 'E', 'F', '1', '0', '2', '3',
    '6', '7', '5', '4', 'F', 'E', 'C', 'D', '8', '9', 'B', 'A', 'D', 'C', 'E',
    'F', 'A', 'B', '9', '8', '3', '2', '0', '1', '4', '5', '7', '6', '9', '8',
    'A', 'B', 'E', 'F', 'D', 'C', '7', '6', '4', '5', '0', '1', '3', '2', '5',
    '4', '6', '7', '2', '3', '1', '0', 'B', 'A', '8', '9', 'C', 'D', 'F', 'E',
    '2', '3', '1', '0', '5', '4', '6', '7', 'C', 'D', 'F', 'E', 'B', 'A', '8',
    '9', 'E', 'F', 'D', 'C', '9', '8', 'A', 'B', '0', '1', '3', '2', '7', '6',
    '4', '5', 'A', 'B', '9', '8', 'D', 'C', 'E', 'F', '4', '5', '7', '6', '3',
    '2', '0', '1', '6', '7', '5', '4', '1', '0', '2', '3', '8', '9', 'B', 'A',
    'F', 'E', 'C', 'D', '3', '2', '0', '1', '4', '5', '7', '6', 'D', 'C', 'E',
    'F', 'A', 'B', '9', '8', 'F', 'E', 'C', 'D', '8', '9', 'B', 'A', '1', '0',
    '2', '3', '6', '7', '5', '4', 'B', 'A', '8', '9', 'C', 'D', 'F', 'E', '5',
    '4', '6', '7', '2', '3', '1', '0', '7', '6', '4', '5', '0', '1', '3', '2',
    '9', '8', 'A', 'B', 'E', 'F', 'D', 'C', '0', 'D', 'A', '7', '4', '9', 'E',
    '3', '8', '5', '2', 'F', 'C', '1', '6', 'B', 'D', '0', '7', 'A', '9', '4',
    '3', 'E', '5', '8', 'F', '2', '1', 'C', 'B', '6', '7', 'A', 'D', '0', '3',
    'E', '9', '4', 'F', '2', '5', '8', 'B', '6', '1', 'C', 'A', '7', '0', 'D',
    'E', '3', '4', '9', '2', 'F', '8', '5', '6', 'B', 'C', '1', '3', 'E', '9',
    '4', '7', 'A', 'D', '0', 'B', '6', '1', 'C', 'F', '2', '5', '8', 'E', '3',
    '4', '9', 'A', '7', '0', 'D', '6', 'B', 'C', '1', '2', 'F', '8', '5', '4',
    '9', 'E', '3', '0', 'D', 'A', '7', 'C', '1', '6', 'B', '8', '5', '2', 'F',
    '9', '4', '3', 'E', 'D', '0', '7', 'A', '1', 'C', 'B', '6', '5', '8', 'F',
    '2', '6', 'B', 'C', '1', '2', 'F', '8', '5', 'E', '3', '4', '9', 'A', '7',
    '0', 'D', 'B', '6', '1', 'C', 'F', '2', '5', '8', '3', 'E', '9', '4', '7',
    'A', 'D', '0', '1', 'C', 'B', '6', '5', '8', 'F', '2', '9', '4', '3', 'E',
    'D', '0', '7', 'A', 'C', '1', '6', 'B', '8', '5', '2', 'F', '4', '9', 'E',
    '3', '0', 'D', 'A', '7', '5', '8', 'F', '2', '1', 'C', 'B', '6', 'D', '0',
    '7', 'A', '9', '4', '3', 'E', '8', '5', '2', 'F', 'C', '1', '6', 'B', '0',
    'D', 'A', '7', '4', '9', 'E', '3', '2', 'F', '8', '5', '6', 'B', 'C', '1',
    'A', '7', '0', 'D', 'E', '3', '4', '9', 'F', '2', '5', '8', 'B', '6', '1',
    'C', '7', 'A', 'D', '0', '3', 'E', '9', '4' };

  boolean_T bin_crc;
  char_T y_idx_0;
  char_T y_idx_1;
  uint8_T tmp;

  /* MATLAB Function 'Output_Data/MonitorMessages/Msg_x018/CRC_Data02/MATLAB Function': '<S55>:1' */
  /* '<S55>:1:8' */
  /* '<S55>:1:9' */
  byte_array[0] = rtu_byte0;
  byte_array[1] = rtu_byte1;
  byte_array[2] = rtu_byte2;
  byte_array[3] = rtu_byte3;
  byte_array[4] = rtu_byte4;
  byte_array[5] = rtu_byte5;
  byte_array[6] = rtu_byte6;

  /* '<S55>:1:12' */
  Crc8_ucr = 255.0;

  /* '<S55>:1:13' */
  for (b_index = 0; b_index < 7; b_index++) {
    /* '<S55>:1:15' */
    /* '<S55>:1:16' */
    /* '<S55>:1:27' */
    /* '<S55>:1:59' */
    skj = (int32_T)Crc8_ucr;
    tmp = (uint8_T)skj;
    skj = (int32_T)((tmp ^ byte_array[b_index]) + 1U);
    if ((uint32_T)skj > 255U) {
      skj = 255;
    }

    y_idx_0 = Crc8Tab[skj - 1];
    y_idx_1 = Crc8Tab[skj + 255];
    Crc8_ucr = 0.0;
    if (y_idx_1 != 48) {
      if (y_idx_1 <= 57) {
        skj = y_idx_1;
      } else {
        skj = y_idx_1 - 7;
      }

      skj -= 48;
      Crc8_ucr = skj;
    }

    if (y_idx_0 != 48) {
      if (y_idx_0 <= 57) {
        skj = y_idx_0;
      } else {
        skj = y_idx_0 - 7;
      }

      skj -= 48;
      Crc8_ucr += (real_T)skj * 16.0;
    }

    /* '<S55>:1:17' */
  }

  /* '<S55>:1:72' */
  /* '<S55>:1:73' */
  for (b_index = 0; b_index < 8; b_index++) {
    /* '<S55>:1:73' */
    /* '<S55>:1:74' */
    if (floor(Crc8_ucr / rt_powd_snf(2.0, 7.0 + -(real_T)b_index)) == 1.0) {
      /* '<S55>:1:75' */
      /* '<S55>:1:76' */
      Crc8_ucr -= rt_powd_snf(2.0, 7.0 + -(real_T)b_index);

      /* '<S55>:1:77' */
      x[7 - b_index] = 1;
    } else {
      /* '<S55>:1:79' */
      x[7 - b_index] = 0;
    }
  }

  /* '<S55>:1:82' */
  b_index = x[0];
  x[0] = x[7];
  x[7] = (int8_T)b_index;
  b_index = x[1];
  x[1] = x[6];
  x[6] = (int8_T)b_index;
  b_index = x[2];
  x[2] = x[5];
  x[5] = (int8_T)b_index;
  b_index = x[3];
  x[3] = x[4];
  x[4] = (int8_T)b_index;

  /* '<S55>:1:21' */
  /* '<S55>:1:22' */
  /* '<S55>:1:64' */
  Crc8_ucr = 0.0;

  /* '<S55>:1:65' */
  for (b_index = 0; b_index < 8; b_index++) {
    bin_crc = (x[b_index] == 0);

    /* '<S55>:1:65' */
    /* '<S55>:1:66' */
    Crc8_ucr += rt_powd_snf(2.0, 8.0 - (1.0 + (real_T)b_index)) * (real_T)
      bin_crc;
  }

  /* '<S55>:1:68' */
  localB->crc = Crc8_ucr;
}

/*
 * System initialize for atomic system:
 *    '<S29>/Counter'
 *    '<S30>/Counter'
 */
void Gyro_Counter_Init(B_Counter_Gyro_T *localB, DW_Counter_Gyro_T *localDW)
{
  localDW->is_active_c6_Gyro = 0U;
  localB->Count = 0.0;
}

/*
 * Output and update for atomic system:
 *    '<S29>/Counter'
 *    '<S30>/Counter'
 */
void Gyro_Counter(B_Counter_Gyro_T *localB, DW_Counter_Gyro_T *localDW)
{
  /* Chart: '<S29>/Counter' */
  /* Gateway: Output_Data/MonitorMessages/Msg_x018/Counter */
  /* During: Output_Data/MonitorMessages/Msg_x018/Counter */
  if (localDW->is_active_c6_Gyro == 0U) {
    /* Entry: Output_Data/MonitorMessages/Msg_x018/Counter */
    localDW->is_active_c6_Gyro = 1U;

    /* Entry Internal: Output_Data/MonitorMessages/Msg_x018/Counter */
    /* Transition: '<S33>:2' */
    localB->Count = 0.0;
  } else {
    /* During 'COUNTER': '<S33>:1' */
    if (localB->Count == 15.0) {
      /* Transition: '<S33>:6' */
      localB->Count = 0.0;
    } else {
      localB->Count++;
    }
  }

  /* End of Chart: '<S29>/Counter' */
}

/* System initialize for function-call system: '<S2>/MonitorMessages' */
void Gyro_MonitorMessages_Init(void)
{
  /* SystemInitialize for Chart: '<S29>/Counter' */
  Gyro_Counter_Init(&Gyro_B.sf_Counter, &Gyro_DW.sf_Counter);

  /* SystemInitialize for Chart: '<S30>/Counter' */
  Gyro_Counter_Init(&Gyro_B.sf_Counter_k, &Gyro_DW.sf_Counter_k);
}

/* Output and update for function-call system: '<S2>/MonitorMessages' */
void Gyro_MonitorMessages(void)
{
  uint16_T tmp;
  uint8_T tmp_0;
  real_T tmp_1;

  /* Gain: '<S29>/Gain1' */
  Gyro_B.Gain1 = Gyro_P.Gain1_Gain * Gyro_B.TiltPosPerc;

  /* DataTypeConversion: '<S29>/Data Type Conversion24' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_ExvlPot_ERR2);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion24_n = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion24' */

  /* DataTypeConversion: '<S29>/Data Type Conversion25' */
  Gyro_B.DataTypeConversion25 = Gyro_B.DataTypeConversion24_n;

  /* DataTypeConversion: '<S29>/Data Type Conversion22' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_ExvlPot_ERR1);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion22_o = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion22' */

  /* DataTypeConversion: '<S29>/Data Type Conversion23' */
  Gyro_B.DataTypeConversion23 = Gyro_B.DataTypeConversion22_o;

  /* DataTypeConversion: '<S29>/Data Type Conversion20' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_DAVC_STAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion20_g = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion20' */

  /* DataTypeConversion: '<S29>/Data Type Conversion21' */
  Gyro_B.DataTypeConversion21 = Gyro_B.DataTypeConversion20_g;

  /* DataTypeConversion: '<S29>/Data Type Conversion18' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_DWC_STAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion18_o = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion18' */

  /* DataTypeConversion: '<S29>/Data Type Conversion19' */
  Gyro_B.DataTypeConversion19 = Gyro_B.DataTypeConversion18_o;

  /* DataTypeConversion: '<S29>/Data Type Conversion14' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_DWCLevACK);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion14_mc = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion14' */

  /* DataTypeConversion: '<S29>/Data Type Conversion17' */
  Gyro_B.DataTypeConversion17 = Gyro_B.DataTypeConversion14_mc;

  /* DataTypeConversion: '<S29>/Data Type Conversion12' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_DTC_STAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion12_f = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion12' */

  /* DataTypeConversion: '<S29>/Data Type Conversion13' */
  Gyro_B.DataTypeConversion13 = Gyro_B.DataTypeConversion12_f;

  /* DataTypeConversion: '<S29>/Data Type Conversion10' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_DTCLevACK);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion10_d = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion10' */

  /* DataTypeConversion: '<S29>/Data Type Conversion11' */
  Gyro_B.DataTypeConversion11 = Gyro_B.DataTypeConversion10_d;

  /* DataTypeConversion: '<S29>/Data Type Conversion2' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_RSpeed);
  if (tmp_1 < 65536.0) {
    if (tmp_1 >= 0.0) {
      tmp = (uint16_T)tmp_1;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint16_T;
  }

  Gyro_B.DataTypeConversion2_a = tmp;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion2' */

  /* DataTypeConversion: '<S29>/Data Type Conversion1' */
  Gyro_B.DataTypeConversion1 = Gyro_B.DataTypeConversion2_a;

  /* DataTypeConversion: '<S29>/Data Type Conversion8' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_RSpeedSTAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion8_d = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion8' */

  /* DataTypeConversion: '<S29>/Data Type Conversion9' */
  Gyro_B.DataTypeConversion9 = Gyro_B.DataTypeConversion8_d;

  /* DataTypeConversion: '<S29>/Data Type Conversion5' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_ExvlCheck);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion5_d = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion5' */

  /* DataTypeConversion: '<S29>/Data Type Conversion6' */
  Gyro_B.DataTypeConversion6 = Gyro_B.DataTypeConversion5_d;

  /* DataTypeConversion: '<S29>/Data Type Conversion3' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_ExvlMot_ERR);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion3_a = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion3' */

  /* DataTypeConversion: '<S29>/Data Type Conversion4' */
  Gyro_B.DataTypeConversion4 = Gyro_B.DataTypeConversion3_a;

  /* DataTypeConversion: '<S29>/Data Type Conversion15' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_FSpeedSTAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion15_i = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion15' */

  /* DataTypeConversion: '<S29>/Data Type Conversion16' */
  Gyro_B.DataTypeConversion16 = Gyro_B.DataTypeConversion15_i;

  /* DataTypeConversion: '<S29>/Data Type Conversion7' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_FSpeed);
  if (tmp_1 < 65536.0) {
    if (tmp_1 >= 0.0) {
      tmp = (uint16_T)tmp_1;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint16_T;
  }

  Gyro_B.DataTypeConversion7_i = tmp;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion7' */

  /* DataTypeConversion: '<S29>/Data Type Conversion49' */
  Gyro_B.DataTypeConversion49 = Gyro_B.DataTypeConversion7_i;

  /* S-Function (rti_commonblock): '<S31>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN TX Message Block: "BBS_Data01" Id:24 */
  {
    UInt32 CAN_Msg[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    Float32 delayTime = 0.0;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read(can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18]->timestamp);
    }

    /* ... Encode Simulink signals of TX and RM blocks*/
    {
      rtican_Signal_t CAN_Sgn;

      /* ...... "BBS_Data01_CRC" (0|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.BBS_Data01_CRC ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[7] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_Data01_CNT" (8|4, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.BBS_Data01_CNT ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000000F;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_ExvlPot_ERR2" (12|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion25 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 4;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_ExvlPot_ERR1" (13|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion23 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 5;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DAVC_STAT" (14|2, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion21 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000003;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 6;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DWC_STAT" (22|2, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion19 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000003;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 6;
      CAN_Msg[5] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DWCLevACK" (24|3, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion17 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000007;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DTC_STAT" (27|2, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion13 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000003;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 3;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DTCLevACK" (29|3, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion11 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000007;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 5;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_RSpeed" (32|13, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion1 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00001FFF;
      CAN_Msg[3] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "BBS_RSpeedSTAT" (45|2, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion9 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000003;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 5;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_ExvlCheck" (47|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion6 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 7;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_ExvlMot_ERR" (48|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion4 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_FSpeedSTAT" (49|2, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion16 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000003;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 1;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_FSpeed" (51|13, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion49 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00001FFF;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 3;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[0] |= CAN_Sgn.SgnBytes.Byte1;
    }

    /* ... Write the data to the CAN microcontroller and trigger the sending of the message */
    can_tp1_msg_send(can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18], 8, &(CAN_Msg[0]),
                     delayTime);
  }

  /* DataTypeConversion: '<S32>/Data Type Conversion2' */
  tmp_1 = floor(Gyro_B.DataTypeConversion49);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 65536.0);
  }

  Gyro_B.DataTypeConversion2_p = (uint16_T)(tmp_1 < 0.0 ? (int32_T)(uint16_T)
    -(int16_T)(uint16_T)-tmp_1 : (int32_T)(uint16_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion2' */

  /* DataTypeConversion: '<S34>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits = (uint16_T)(Gyro_B.DataTypeConversion2_p & 8191);

  /* DataTypeConversion: '<S34>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly = Gyro_B.ExtractDesiredBits;

  /* ArithShift: '<S32>/Shift Arithmetic3' */
  Gyro_B.ShiftArithmetic3 = (uint16_T)((uint16_T)(Gyro_B.ModifyScalingOnly << 3)
    & 8191);

  /* DataTypeConversion: '<S32>/Data Type Conversion8' */
  tmp_1 = floor(Gyro_B.DataTypeConversion16);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion8_i = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion8' */

  /* DataTypeConversion: '<S45>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_bn = (uint8_T)(Gyro_B.DataTypeConversion8_i & 3);

  /* DataTypeConversion: '<S45>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_d = Gyro_B.ExtractDesiredBits_bn;

  /* ArithShift: '<S32>/Shift Arithmetic4' */
  Gyro_B.ShiftArithmetic4 = (uint8_T)((uint8_T)(Gyro_B.ModifyScalingOnly_d << 1)
    & 3);

  /* DataTypeConversion: '<S32>/Data Type Conversion9' */
  tmp_1 = floor(Gyro_B.DataTypeConversion4);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion9_k = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion9' */

  /* DataTypeConversion: '<S51>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_po = (uint8_T)(Gyro_B.DataTypeConversion9_k & 1);

  /* DataTypeConversion: '<S51>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_m = Gyro_B.ExtractDesiredBits_po;

  /* Sum: '<S32>/Add' */
  Gyro_B.Add = (uint16_T)((uint32_T)(uint16_T)((uint32_T)Gyro_B.ShiftArithmetic3
    + Gyro_B.ShiftArithmetic4) + Gyro_B.ModifyScalingOnly_m);

  /* DataTypeConversion: '<S32>/Data Type Conversion4' */
  tmp_1 = floor(Gyro_B.DataTypeConversion6);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion4_mf = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion4' */

  /* DataTypeConversion: '<S52>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_l = (uint8_T)(Gyro_B.DataTypeConversion4_mf & 1);

  /* DataTypeConversion: '<S52>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_aq = Gyro_B.ExtractDesiredBits_l;

  /* ArithShift: '<S32>/Shift Arithmetic2' */
  Gyro_B.ShiftArithmetic2 = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion16' */
  tmp_1 = floor(Gyro_B.DataTypeConversion9);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion16_e = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion16' */

  /* DataTypeConversion: '<S53>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_gv = (uint8_T)(Gyro_B.DataTypeConversion16_e & 3);

  /* DataTypeConversion: '<S53>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_a0 = Gyro_B.ExtractDesiredBits_gv;

  /* ArithShift: '<S32>/Shift Arithmetic1' */
  Gyro_B.ShiftArithmetic1_g = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion17' */
  tmp_1 = floor(Gyro_B.DataTypeConversion1);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 65536.0);
  }

  Gyro_B.DataTypeConversion17_d = (uint16_T)(tmp_1 < 0.0 ? (int32_T)(uint16_T)
    -(int16_T)(uint16_T)-tmp_1 : (int32_T)(uint16_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion17' */

  /* DataTypeConversion: '<S54>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_j = (uint16_T)(Gyro_B.DataTypeConversion17_d & 8191);

  /* DataTypeConversion: '<S54>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_e = Gyro_B.ExtractDesiredBits_j;

  /* ArithShift: '<S32>/Shift Arithmetic8' */
  Gyro_B.ShiftArithmetic8 = (uint16_T)((uint32_T)Gyro_B.ModifyScalingOnly_e >> 8);

  /* Sum: '<S32>/Add1' */
  Gyro_B.Add1 = (uint16_T)(((uint32_T)Gyro_B.ShiftArithmetic2 +
    Gyro_B.ShiftArithmetic1_g) + Gyro_B.ShiftArithmetic8);

  /* DataTypeConversion: '<S32>/Data Type Conversion1' */
  tmp_1 = floor(Gyro_B.DataTypeConversion21);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion1_a = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion1' */

  /* DataTypeConversion: '<S42>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_e2 = (uint8_T)(Gyro_B.DataTypeConversion1_a & 3);

  /* DataTypeConversion: '<S42>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_h = Gyro_B.ExtractDesiredBits_e2;

  /* ArithShift: '<S32>/Shift Arithmetic7' */
  Gyro_B.ShiftArithmetic7_j = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion13' */
  tmp_1 = floor(Gyro_B.DataTypeConversion23);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion13_i = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion13' */

  /* DataTypeConversion: '<S43>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_h = (uint8_T)(Gyro_B.DataTypeConversion13_i & 1);

  /* DataTypeConversion: '<S43>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_nj = Gyro_B.ExtractDesiredBits_h;

  /* ArithShift: '<S32>/Shift Arithmetic5' */
  Gyro_B.ShiftArithmetic5_j = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion14' */
  tmp_1 = floor(Gyro_B.DataTypeConversion25);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion14_j = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion14' */

  /* DataTypeConversion: '<S44>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_o5 = (uint8_T)(Gyro_B.DataTypeConversion14_j & 1);

  /* DataTypeConversion: '<S44>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_m3 = Gyro_B.ExtractDesiredBits_o5;

  /* ArithShift: '<S32>/Shift Arithmetic6' */
  Gyro_B.ShiftArithmetic6_d = 0U;

  /* Chart: '<S29>/Counter' */
  Gyro_Counter(&Gyro_B.sf_Counter, &Gyro_DW.sf_Counter);

  /* DataTypeConversion: '<S29>/Data Type Conversion26' */
  tmp_1 = rt_roundd_snf(Gyro_B.sf_Counter.Count);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion26_d = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion26' */

  /* DataTypeConversion: '<S29>/Data Type Conversion27' */
  Gyro_B.DataTypeConversion27_j = Gyro_B.DataTypeConversion26_d;

  /* DataTypeConversion: '<S32>/Data Type Conversion15' */
  Gyro_B.DataTypeConversion15_h = Gyro_B.DataTypeConversion27_j;

  /* DataTypeConversion: '<S46>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_ph = (uint8_T)(Gyro_B.DataTypeConversion15_h & 15);

  /* DataTypeConversion: '<S46>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_aj = Gyro_B.ExtractDesiredBits_ph;

  /* Sum: '<S32>/Add2' */
  Gyro_B.Add2 = (uint8_T)((uint32_T)(uint8_T)((uint32_T)(uint8_T)((uint32_T)
    Gyro_B.ShiftArithmetic7_j + Gyro_B.ShiftArithmetic5_j) +
    Gyro_B.ShiftArithmetic6_d) + Gyro_B.ModifyScalingOnly_aj);

  /* DataTypeConversion: '<S32>/Data Type Conversion19' */
  tmp_1 = floor(Gyro_B.DataTypeConversion11);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion19_f = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion19' */

  /* DataTypeConversion: '<S37>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_i = (uint8_T)(Gyro_B.DataTypeConversion19_f & 7);

  /* DataTypeConversion: '<S37>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_l1 = Gyro_B.ExtractDesiredBits_i;

  /* ArithShift: '<S32>/Shift Arithmetic10' */
  Gyro_B.ShiftArithmetic10 = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion18' */
  tmp_1 = floor(Gyro_B.DataTypeConversion13);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion18_e = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion18' */

  /* DataTypeConversion: '<S38>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_gh = (uint8_T)(Gyro_B.DataTypeConversion18_e & 3);

  /* DataTypeConversion: '<S38>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_g = Gyro_B.ExtractDesiredBits_gh;

  /* ArithShift: '<S32>/Shift Arithmetic9' */
  Gyro_B.ShiftArithmetic9 = 0U;

  /* DataTypeConversion: '<S32>/Data Type Conversion20' */
  tmp_1 = floor(Gyro_B.DataTypeConversion17);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion20_e = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion20' */

  /* DataTypeConversion: '<S39>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_gr = (uint8_T)(Gyro_B.DataTypeConversion20_e & 7);

  /* DataTypeConversion: '<S39>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_cb = Gyro_B.ExtractDesiredBits_gr;

  /* Sum: '<S32>/Add3' */
  Gyro_B.Add3 = (uint8_T)((uint32_T)(uint8_T)((uint32_T)Gyro_B.ShiftArithmetic10
    + Gyro_B.ShiftArithmetic9) + Gyro_B.ModifyScalingOnly_cb);

  /* DataTypeConversion: '<S32>/Data Type Conversion21' */
  tmp_1 = floor(Gyro_B.DataTypeConversion19);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion21_h = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S32>/Data Type Conversion21' */

  /* DataTypeConversion: '<S41>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_n = (uint8_T)(Gyro_B.DataTypeConversion21_h & 3);

  /* DataTypeConversion: '<S41>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_k = Gyro_B.ExtractDesiredBits_n;

  /* ArithShift: '<S32>/Shift Arithmetic11' */
  Gyro_B.ShiftArithmetic11 = 0U;

  /* DataTypeConversion: '<S40>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_o = Gyro_B.ShiftArithmetic11;

  /* DataTypeConversion: '<S40>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_c = Gyro_B.ExtractDesiredBits_o;

  /* DataTypeConversion: '<S32>/Data Type Conversion10' */
  Gyro_B.DataTypeConversion10_n = Gyro_B.ModifyScalingOnly_c;

  /* DataTypeConversion: '<S36>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_e = Gyro_B.Add3;

  /* DataTypeConversion: '<S36>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_a = Gyro_B.ExtractDesiredBits_e;

  /* DataTypeConversion: '<S32>/Data Type Conversion11' */
  Gyro_B.DataTypeConversion11_j3 = Gyro_B.ModifyScalingOnly_a;

  /* DataTypeConversion: '<S47>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_a = Gyro_B.Add2;

  /* DataTypeConversion: '<S47>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_l = Gyro_B.ExtractDesiredBits_a;

  /* DataTypeConversion: '<S32>/Data Type Conversion12' */
  Gyro_B.DataTypeConversion12_j = Gyro_B.ModifyScalingOnly_l;

  /* ArithShift: '<S32>/Shift Arithmetic' */
  Gyro_B.ShiftArithmetic = (uint16_T)((uint32_T)Gyro_B.ModifyScalingOnly >> 5);

  /* DataTypeConversion: '<S50>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_g = (uint8_T)Gyro_B.ShiftArithmetic;

  /* DataTypeConversion: '<S50>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_p = Gyro_B.ExtractDesiredBits_g;

  /* DataTypeConversion: '<S32>/Data Type Conversion3' */
  Gyro_B.DataTypeConversion3_l = Gyro_B.ModifyScalingOnly_p;

  /* DataTypeConversion: '<S49>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_b = (uint8_T)Gyro_B.Add;

  /* DataTypeConversion: '<S49>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_jw = Gyro_B.ExtractDesiredBits_b;

  /* DataTypeConversion: '<S32>/Data Type Conversion5' */
  Gyro_B.DataTypeConversion5_p = Gyro_B.ModifyScalingOnly_jw;

  /* DataTypeConversion: '<S48>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_k = (uint8_T)Gyro_B.Add1;

  /* DataTypeConversion: '<S48>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_cq = Gyro_B.ExtractDesiredBits_k;

  /* DataTypeConversion: '<S32>/Data Type Conversion6' */
  Gyro_B.DataTypeConversion6_b = Gyro_B.ModifyScalingOnly_cq;

  /* DataTypeConversion: '<S35>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_ej = (uint8_T)Gyro_B.ModifyScalingOnly_e;

  /* DataTypeConversion: '<S35>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_c5 = Gyro_B.ExtractDesiredBits_ej;

  /* DataTypeConversion: '<S32>/Data Type Conversion7' */
  Gyro_B.DataTypeConversion7_m = Gyro_B.ModifyScalingOnly_c5;

  /* MATLAB Function: '<S32>/MATLAB Function' */
  Gyro_MATLABFunction(Gyro_B.DataTypeConversion3_l, Gyro_B.DataTypeConversion5_p,
                      Gyro_B.DataTypeConversion6_b, Gyro_B.DataTypeConversion7_m,
                      Gyro_B.DataTypeConversion11_j3,
                      Gyro_B.DataTypeConversion10_n,
                      Gyro_B.DataTypeConversion12_j, &Gyro_B.sf_MATLABFunction);

  /* Sum: '<S29>/Subtract' incorporates:
   *  Constant: '<S29>/Constant'
   */
  Gyro_B.Subtract = Gyro_P.Constant_Value - Gyro_B.sf_MATLABFunction.crc;

  /* DataTypeConversion: '<S29>/Data Type Conversion28' */
  tmp_1 = rt_roundd_snf(Gyro_B.Subtract);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion28_j = tmp_0;

  /* End of DataTypeConversion: '<S29>/Data Type Conversion28' */

  /* DataTypeConversion: '<S29>/Data Type Conversion29' */
  Gyro_B.DataTypeConversion29_e = Gyro_B.DataTypeConversion28_j;

  /* Gain: '<S29>/Gain' */
  Gyro_B.Gain = Gyro_P.Gain_Gain * 0.0;

  /* DataTypeConversion: '<S30>/Data Type Conversion7' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqReqSlow);
  if (tmp_1 < 65536.0) {
    if (tmp_1 >= 0.0) {
      tmp = (uint16_T)tmp_1;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint16_T;
  }

  Gyro_B.DataTypeConversion7_e = tmp;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion7' */

  /* DataTypeConversion: '<S30>/Data Type Conversion49' */
  Gyro_B.DataTypeConversion49_f = Gyro_B.DataTypeConversion7_e;

  /* DataTypeConversion: '<S57>/Data Type Conversion2' */
  tmp_1 = floor(Gyro_B.DataTypeConversion49_f);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 65536.0);
  }

  Gyro_B.DataTypeConversion2_c = (uint16_T)(tmp_1 < 0.0 ? (int32_T)(uint16_T)
    -(int16_T)(uint16_T)-tmp_1 : (int32_T)(uint16_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion2' */

  /* DataTypeConversion: '<S59>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_c = (uint16_T)(Gyro_B.DataTypeConversion2_c & 4095);

  /* DataTypeConversion: '<S59>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_j = Gyro_B.ExtractDesiredBits_c;

  /* ArithShift: '<S57>/Shift Arithmetic' */
  Gyro_B.ShiftArithmetic_b = (uint16_T)((uint32_T)Gyro_B.ModifyScalingOnly_j >>
    4);

  /* DataTypeConversion: '<S63>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_c1 = (uint8_T)Gyro_B.ShiftArithmetic_b;

  /* DataTypeConversion: '<S63>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_et = Gyro_B.ExtractDesiredBits_c1;

  /* DataTypeConversion: '<S57>/Data Type Conversion3' */
  Gyro_B.DataTypeConversion3_b = Gyro_B.ModifyScalingOnly_et;

  /* ArithShift: '<S57>/Shift Arithmetic3' */
  Gyro_B.ShiftArithmetic3_g = (uint16_T)((uint16_T)(Gyro_B.ModifyScalingOnly_j <<
    4) & 4095);

  /* DataTypeConversion: '<S30>/Data Type Conversion2' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqReqFast);
  if (tmp_1 < 65536.0) {
    if (tmp_1 >= 0.0) {
      tmp = (uint16_T)tmp_1;
    } else {
      tmp = 0U;
    }
  } else {
    tmp = MAX_uint16_T;
  }

  Gyro_B.DataTypeConversion2_b = tmp;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion2' */

  /* DataTypeConversion: '<S30>/Data Type Conversion1' */
  Gyro_B.DataTypeConversion1_g = Gyro_B.DataTypeConversion2_b;

  /* DataTypeConversion: '<S57>/Data Type Conversion4' */
  tmp_1 = floor(Gyro_B.DataTypeConversion1_g);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 65536.0);
  }

  Gyro_B.DataTypeConversion4_k = (uint16_T)(tmp_1 < 0.0 ? (int32_T)(uint16_T)
    -(int16_T)(uint16_T)-tmp_1 : (int32_T)(uint16_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion4' */

  /* DataTypeConversion: '<S60>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_d = (uint16_T)(Gyro_B.DataTypeConversion4_k & 4095);

  /* DataTypeConversion: '<S60>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_jd = Gyro_B.ExtractDesiredBits_d;

  /* ArithShift: '<S57>/Shift Arithmetic1' */
  Gyro_B.ShiftArithmetic1 = (uint16_T)((uint32_T)Gyro_B.ModifyScalingOnly_jd >>
    8);

  /* Sum: '<S57>/Add' */
  Gyro_B.Add_f = (uint16_T)((uint32_T)Gyro_B.ShiftArithmetic3_g +
    Gyro_B.ShiftArithmetic1);

  /* DataTypeConversion: '<S62>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_b1 = (uint8_T)Gyro_B.Add_f;

  /* DataTypeConversion: '<S62>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_b = Gyro_B.ExtractDesiredBits_b1;

  /* DataTypeConversion: '<S57>/Data Type Conversion5' */
  Gyro_B.DataTypeConversion5_j = Gyro_B.ModifyScalingOnly_b;

  /* DataTypeConversion: '<S61>/Extract Desired Bits' */
  Gyro_B.ExtractDesiredBits_p = (uint8_T)Gyro_B.DataTypeConversion4_k;

  /* DataTypeConversion: '<S61>/Modify Scaling Only' */
  Gyro_B.ModifyScalingOnly_n = Gyro_B.ExtractDesiredBits_p;

  /* DataTypeConversion: '<S57>/Data Type Conversion6' */
  Gyro_B.DataTypeConversion6_m = Gyro_B.ModifyScalingOnly_n;

  /* DataTypeConversion: '<S30>/Data Type Conversion10' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqRedSlow);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion10_e = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion10' */

  /* DataTypeConversion: '<S30>/Data Type Conversion47' */
  Gyro_B.DataTypeConversion47 = Gyro_B.DataTypeConversion10_e;

  /* DataTypeConversion: '<S57>/Data Type Conversion7' */
  tmp_1 = floor(Gyro_B.DataTypeConversion47);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion7_e5 = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion7' */

  /* DataTypeConversion: '<S30>/Data Type Conversion3' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqRedFast);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion3_j = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion3' */

  /* DataTypeConversion: '<S30>/Data Type Conversion4' */
  Gyro_B.DataTypeConversion4_l = Gyro_B.DataTypeConversion3_j;

  /* DataTypeConversion: '<S57>/Data Type Conversion11' */
  tmp_1 = floor(Gyro_B.DataTypeConversion4_l);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion11_o = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion11' */

  /* DataTypeConversion: '<S57>/Data Type Conversion10' incorporates:
   *  Constant: '<S57>/Constant'
   */
  tmp_1 = floor(Gyro_P.Constant_Value_f);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion10_f = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion10' */

  /* DataTypeConversion: '<S30>/Data Type Conversion11' */
  tmp_1 = rt_roundd_snf(Gyro_B.DTCwork_Status);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion11_f = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion11' */

  /* DataTypeConversion: '<S30>/Data Type Conversion12' */
  Gyro_B.DataTypeConversion12 = Gyro_B.DataTypeConversion11_f;

  /* DataTypeConversion: '<S57>/Data Type Conversion1' */
  tmp_1 = floor(Gyro_B.DataTypeConversion12);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion1_go = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion1' */

  /* ArithShift: '<S57>/Shift Arithmetic7' */
  Gyro_B.ShiftArithmetic7 = (uint8_T)(Gyro_B.DataTypeConversion1_go << 6);

  /* DataTypeConversion: '<S30>/Data Type Conversion5' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqReqSTAT);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion5_g = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion5' */

  /* DataTypeConversion: '<S30>/Data Type Conversion6' */
  Gyro_B.DataTypeConversion6_f = Gyro_B.DataTypeConversion5_g;

  /* DataTypeConversion: '<S57>/Data Type Conversion13' */
  tmp_1 = floor(Gyro_B.DataTypeConversion6_f);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion13_og = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion13' */

  /* ArithShift: '<S57>/Shift Arithmetic5' */
  Gyro_B.ShiftArithmetic5 = (uint8_T)(Gyro_B.DataTypeConversion13_og << 5);

  /* DataTypeConversion: '<S30>/Data Type Conversion8' */
  tmp_1 = rt_roundd_snf(Gyro_B.BBS_TorqReqRel);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion8_j = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion8' */

  /* DataTypeConversion: '<S30>/Data Type Conversion9' */
  Gyro_B.DataTypeConversion9_e = Gyro_B.DataTypeConversion8_j;

  /* DataTypeConversion: '<S57>/Data Type Conversion14' */
  tmp_1 = floor(Gyro_B.DataTypeConversion9_e);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion14_o = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion14' */

  /* ArithShift: '<S57>/Shift Arithmetic6' */
  Gyro_B.ShiftArithmetic6 = (uint8_T)(Gyro_B.DataTypeConversion14_o << 4);

  /* Chart: '<S30>/Counter' */
  Gyro_Counter(&Gyro_B.sf_Counter_k, &Gyro_DW.sf_Counter_k);

  /* DataTypeConversion: '<S30>/Data Type Conversion13' */
  tmp_1 = rt_roundd_snf(Gyro_B.sf_Counter_k.Count);
  if (tmp_1 < 256.0) {
    if (tmp_1 >= 0.0) {
      tmp_0 = (uint8_T)tmp_1;
    } else {
      tmp_0 = 0U;
    }
  } else {
    tmp_0 = MAX_uint8_T;
  }

  Gyro_B.DataTypeConversion13_h = tmp_0;

  /* End of DataTypeConversion: '<S30>/Data Type Conversion13' */

  /* DataTypeConversion: '<S30>/Data Type Conversion14' */
  Gyro_B.DataTypeConversion14 = Gyro_B.DataTypeConversion13_h;

  /* DataTypeConversion: '<S57>/Data Type Conversion15' */
  tmp_1 = floor(Gyro_B.DataTypeConversion14);
  if (rtIsNaN(tmp_1) || rtIsInf(tmp_1)) {
    tmp_1 = 0.0;
  } else {
    tmp_1 = fmod(tmp_1, 256.0);
  }

  Gyro_B.DataTypeConversion15_io = (uint8_T)(tmp_1 < 0.0 ? (int32_T)(uint8_T)
    -(int8_T)(uint8_T)-tmp_1 : (int32_T)(uint8_T)tmp_1);

  /* End of DataTypeConversion: '<S57>/Data Type Conversion15' */

  /* Sum: '<S57>/Add2' */
  Gyro_B.Add2_a = (uint8_T)((uint32_T)(uint8_T)((uint32_T)(uint8_T)((uint32_T)
    Gyro_B.ShiftArithmetic7 + Gyro_B.ShiftArithmetic5) + Gyro_B.ShiftArithmetic6)
    + Gyro_B.DataTypeConversion15_io);

  /* DataTypeConversion: '<S57>/Data Type Conversion12' */
  Gyro_B.DataTypeConversion12_is = Gyro_B.Add2_a;

  /* MATLAB Function: '<S57>/MATLAB Function' */
  Gyro_MATLABFunction(Gyro_B.DataTypeConversion3_b, Gyro_B.DataTypeConversion5_j,
                      Gyro_B.DataTypeConversion6_m,
                      Gyro_B.DataTypeConversion7_e5,
                      Gyro_B.DataTypeConversion11_o,
                      Gyro_B.DataTypeConversion10_f,
                      Gyro_B.DataTypeConversion12_is,
                      &Gyro_B.sf_MATLABFunction_l);

  /* S-Function (rti_commonblock): '<S56>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN TX Message Block: "BBS_Data02" Id:25 */
  {
    UInt32 CAN_Msg[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    Float32 delayTime = 0.0;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read(can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19]->timestamp);
    }

    /* ... Encode Simulink signals of TX and RM blocks*/
    {
      rtican_Signal_t CAN_Sgn;

      /* ...... "BBS_Data02_CRC" (0|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.sf_MATLABFunction_l.crc ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[7] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_Data02_CNT" (8|4, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion14 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000000F;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_TorqReqRel" (12|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion9_e ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 4;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_TorqReqSTAT" (13|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion6_f ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 5;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_DTCwork" (14|1, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion12 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000001;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 6;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_TorqRedFast" (24|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion4_l ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_TorqRedSlow" (32|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion47 ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[3] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "BBS_TorqReqFast" (40|12, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion1_g ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000FFF;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "BBS_TorqReqSlow" (52|12, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.DataTypeConversion49_f ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x00000FFF;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 4;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[0] |= CAN_Sgn.SgnBytes.Byte1;
    }

    /* ... Write the data to the CAN microcontroller and trigger the sending of the message */
    can_tp1_msg_send(can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19], 8, &(CAN_Msg[0]),
                     delayTime);
  }
}

/* Termination for function-call system: '<S2>/MonitorMessages' */
void Gyro_MonitorMessages_Term(void)
{
  /* Terminate for S-Function (rti_commonblock): '<S31>/S-Function1' */

  /* dSPACE RTICAN TX Message Block: "BBS_Data01" Id:24 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][0] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S56>/S-Function1' */

  /* dSPACE RTICAN TX Message Block: "BBS_Data02" Id:25 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][0] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }
}

/* Output and update for function-call system: '<S2>/SpinTiltMessages' */
void Gyro_SpinTiltMessages(void)
{
  /* S-Function (rti_commonblock): '<S65>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN TX Message Block: "AME_Data05" Id:688 */
  {
    UInt32 CAN_Msg[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    Float32 delayTime = 0.0;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read(can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0]->timestamp);
    }

    /* ... Encode Simulink signals of TX and RM blocks*/
    {
      rtican_Signal_t CAN_Sgn;

      /* ...... "AME_ValveSpdTgt" (0|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltSpeedRef - ( -3.2768 ) ) /
        0.0001 + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[7] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_SpinTgt" (16|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.SpinSpeedRef ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[5] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_CurrTgt" (32|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.SpinCurrentRef - ( 0 ) ) / 0.001
        + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[3] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_SpinStartStop" (48|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.SpinStartStop ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "AME_ModContSpin" (56|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.SpinModCont ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[0] |= CAN_Sgn.SgnBytes.Byte0;
    }

    /* ... Write the data to the CAN microcontroller and trigger the sending of the message */
    can_tp1_msg_send(can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0], 8, &(CAN_Msg[0]),
                     delayTime);
  }

  /* S-Function (rti_commonblock): '<S66>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN TX Message Block: "AME_Data06" Id:689 */
  {
    UInt32 CAN_Msg[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    Float32 delayTime = 0.0;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read(can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1]->timestamp);
    }

    /* ... Encode Simulink signals of TX and RM blocks*/
    {
      rtican_Signal_t CAN_Sgn;

      /* ...... "AME_Valve2OLTgt" (0|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltValvePerc2 - ( -100 ) ) /
        0.01 + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[7] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[6] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_ValvePosTgt" (16|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltPosRef - ( -3.2768 ) ) /
        0.0001 + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[5] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[4] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_Valve1OLTgt" (32|16, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltValvePerc1 - ( -100 ) ) /
        0.01 + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
      CAN_Msg[3] |= CAN_Sgn.SgnBytes.Byte0;
      CAN_Msg[2] |= CAN_Sgn.SgnBytes.Byte1;

      /* ...... "AME_TiltStartStop" (48|8, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltStartStop ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x000000FF;
      CAN_Msg[1] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "AME_ModContTil" (56|4, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.TiltModCont ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000000F;
      CAN_Msg[0] |= CAN_Sgn.SgnBytes.Byte0;

      /* ...... "AME_PumpCommand" (60|4, standard signal, unsigned int, motorola back.) */
      /* Add or substract 0.5 in order to round to nearest integer */
      CAN_Sgn.UnsignedSgn = (UInt32) (( Gyro_B.ComandoPompa ) + 0.5);
      CAN_Sgn.UnsignedSgn &= 0x0000000F;
      CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) << 4;
      CAN_Msg[0] |= CAN_Sgn.SgnBytes.Byte0;
    }

    /* ... Write the data to the CAN microcontroller and trigger the sending of the message */
    can_tp1_msg_send(can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1], 8, &(CAN_Msg[0]),
                     delayTime);
  }
}

/* Termination for function-call system: '<S2>/SpinTiltMessages' */
void Gyro_SpinTiltMessages_Term(void)
{
  /* Terminate for S-Function (rti_commonblock): '<S65>/S-Function1' */

  /* dSPACE RTICAN TX Message Block: "AME_Data05" Id:688 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][5] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S66>/S-Function1' */

  /* dSPACE RTICAN TX Message Block: "AME_Data06" Id:689 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][5] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }
}

/* Output and update for function-call system: '<S4>/Log1' */
void Gyro_Log1(void)
{
  /* DataTypeConversion: '<S71>/Data Type Conversion' */
  Gyro_B.DataTypeConversion_g = (real32_T)Gyro_B.GYRO_DriverStatus_g;

  /* S-Function (rti_commonblock): '<S74>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_DriverStatus_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion_g);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion9' */
  Gyro_B.DataTypeConversion9_p = (real32_T)Gyro_B.GYRO_ValveSpeed2_p;

  /* S-Function (rti_commonblock): '<S75>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValveSpeed2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion9_p);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion10' */
  Gyro_B.DataTypeConversion10 = (real32_T)Gyro_B.GYRO_ValveSpeed1_b;

  /* S-Function (rti_commonblock): '<S76>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValveSpeed1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion10);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion11' */
  Gyro_B.DataTypeConversion11_j = (real32_T)Gyro_B.GYRO_ValvePos2_o;

  /* S-Function (rti_commonblock): '<S77>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValvePos2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion11_j);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion12' */
  Gyro_B.DataTypeConversion12_i = (real32_T)Gyro_B.GYRO_ValvePos1_p;

  /* S-Function (rti_commonblock): '<S78>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValvePos1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion12_i);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion13' */
  Gyro_B.DataTypeConversion13_o = (real32_T)Gyro_B.GYRO_Current2_k;

  /* S-Function (rti_commonblock): '<S79>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_Current2_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion13_o);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion14' */
  Gyro_B.DataTypeConversion14_m = (real32_T)Gyro_B.GYRO_Current1_f;

  /* S-Function (rti_commonblock): '<S80>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_Current1_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion14_m);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion15' */
  Gyro_B.DataTypeConversion15 = (real32_T)Gyro_B.GYRO_SpinSpeed2_j;

  /* S-Function (rti_commonblock): '<S81>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_SpinSpeed2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion15);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion16' */
  Gyro_B.DataTypeConversion16_o = (real32_T)Gyro_B.GYRO_SpinSpeed1_o;

  /* S-Function (rti_commonblock): '<S82>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_SpinSpeed1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion16_o);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion17' */
  Gyro_B.DataTypeConversion17_k = (real32_T)Gyro_B.GYRO_PistonErr_d;

  /* S-Function (rti_commonblock): '<S83>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_PistonErr_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion17_k);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion18' */
  Gyro_B.DataTypeConversion18 = (real32_T)Gyro_B.GYRO_ValvePerc2_i;

  /* S-Function (rti_commonblock): '<S84>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValvePerc2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion18);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion1' */
  Gyro_B.DataTypeConversion1_i = (real32_T)Gyro_B.GYRO_TempMot2_m;

  /* S-Function (rti_commonblock): '<S85>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_TempMot2_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion1_i);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion19' */
  Gyro_B.DataTypeConversion19_h = (real32_T)Gyro_B.GYRO_ValvePerc1_l;

  /* S-Function (rti_commonblock): '<S86>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_ValvePerc1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion19_h);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion20' */
  Gyro_B.DataTypeConversion20 = (real32_T)Gyro_B.GYRO_PistonPress_f;

  /* S-Function (rti_commonblock): '<S87>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_PistonPress_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion20);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion21' */
  Gyro_B.DataTypeConversion21_n = (real32_T)Gyro_B.IMU_YawRate_b;

  /* S-Function (rti_commonblock): '<S88>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(IMU_YawRate_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion21_n);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion22' */
  Gyro_B.DataTypeConversion22 = (real32_T)Gyro_B.IMU_Ay_k;

  /* S-Function (rti_commonblock): '<S89>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(IMU_Ay_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion22);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion23' */
  Gyro_B.DataTypeConversion23_c = (real32_T)Gyro_B.IMU_RollRate_e;

  /* S-Function (rti_commonblock): '<S90>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(IMU_RollRate_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion23_c);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion24' */
  Gyro_B.DataTypeConversion24 = (real32_T)Gyro_B.IMU_Ax_b;

  /* S-Function (rti_commonblock): '<S91>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(IMU_Ax_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion24);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion25' */
  Gyro_B.DataTypeConversion25_k = (real32_T)Gyro_B.IMU_Az_k;

  /* S-Function (rti_commonblock): '<S92>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(IMU_Az_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion25_k);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion26' */
  Gyro_B.DataTypeConversion26 = (real32_T)Gyro_B.ABS_Roll_a;

  /* S-Function (rti_commonblock): '<S93>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(ABS_Roll_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion26);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion27' */
  Gyro_B.DataTypeConversion27 = (real32_T)Gyro_B.DSB_DTCLevel_a;

  /* S-Function (rti_commonblock): '<S94>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DSB_DTCLevel_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion27);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion28' */
  Gyro_B.DataTypeConversion28 = (real32_T)Gyro_B.DSB_Flash_c;

  /* S-Function (rti_commonblock): '<S95>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DSB_Flash_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion28);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion2' */
  Gyro_B.DataTypeConversion2 = (real32_T)Gyro_B.GYRO_TempMot1_e;

  /* S-Function (rti_commonblock): '<S96>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_TempMot1_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion2);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion29' */
  Gyro_B.DataTypeConversion29 = (real32_T)Gyro_B.DSB_RidingMode_k;

  /* S-Function (rti_commonblock): '<S97>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DSB_RidingMode_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion29);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion30' */
  Gyro_B.DataTypeConversion30 = (real32_T)Gyro_B.DBG_SpinSpeedRef;

  /* S-Function (rti_commonblock): '<S98>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_SpinSpeedRef_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion30);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion31' */
  Gyro_B.DataTypeConversion31 = (real32_T)Gyro_B.DBG_SpinCurrentRef;

  /* S-Function (rti_commonblock): '<S99>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_SpinCurrentRef_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion31);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion32' */
  Gyro_B.DataTypeConversion32 = (real32_T)Gyro_B.DBG_SpinStartStop;

  /* S-Function (rti_commonblock): '<S100>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_SpinStartStop_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion32);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion33' */
  Gyro_B.DataTypeConversion33 = (real32_T)Gyro_B.DBG_SpinModCont;

  /* S-Function (rti_commonblock): '<S101>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_SpinModCont_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion33);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion34' */
  Gyro_B.DataTypeConversion34 = (real32_T)Gyro_B.DBG_TiltSpeedRef;

  /* S-Function (rti_commonblock): '<S102>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedRef_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion34);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion35' */
  Gyro_B.DataTypeConversion35 = (real32_T)Gyro_B.DBG_TiltPosRef;

  /* S-Function (rti_commonblock): '<S103>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltPosRef_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion35);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion36' */
  Gyro_B.DataTypeConversion36 = (real32_T)Gyro_B.DBG_TiltModCont;

  /* S-Function (rti_commonblock): '<S104>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltModCont_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion36);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion37' */
  Gyro_B.DataTypeConversion37 = (real32_T)Gyro_B.DBG_TiltStartStop;

  /* S-Function (rti_commonblock): '<S105>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltStartStop_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion37);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion38' */
  Gyro_B.DataTypeConversion38 = (real32_T)Gyro_B.DBG_EnableTilt;

  /* S-Function (rti_commonblock): '<S106>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_EnableTilt_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion38);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion3' */
  Gyro_B.DataTypeConversion3 = (real32_T)Gyro_B.GYRO_DriverErr1_e;

  /* S-Function (rti_commonblock): '<S107>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_DriverErr1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion3);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion39' */
  Gyro_B.DataTypeConversion39 = (real32_T)Gyro_B.DBG_EnableSpin;

  /* S-Function (rti_commonblock): '<S108>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_EnableSpin_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion39);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion40' */
  Gyro_B.DataTypeConversion40 = (real32_T)Gyro_B.DBG_RollRate;

  /* S-Function (rti_commonblock): '<S109>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_RollRate_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion40);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion41' */
  Gyro_B.DataTypeConversion41 = (real32_T)Gyro_B.DBG_RollRate_GxGz;

  /* S-Function (rti_commonblock): '<S110>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_RollRate_GxGz_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion41);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion42' */
  Gyro_B.DataTypeConversion42 = (real32_T)Gyro_B.ABS_Pitch_p;

  /* S-Function (rti_commonblock): '<S111>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(ABS_Pitch_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion42);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion43' */
  Gyro_B.DataTypeConversion43 = (real32_T)Gyro_B.ABS_PitchRate_n;

  /* S-Function (rti_commonblock): '<S112>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(ABS_PitchRate_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion43);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion44' */
  Gyro_B.DataTypeConversion44 = (real32_T)Gyro_B.DBG_TiltSpeedEstimate1;

  /* S-Function (rti_commonblock): '<S113>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedEstimate1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion44);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion45' */
  Gyro_B.DataTypeConversion45 = (real32_T)Gyro_B.DBG_TiltSpeedEstimate2;

  /* S-Function (rti_commonblock): '<S114>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedEstimate2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion45);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion46' */
  Gyro_B.DataTypeConversion46 = (real32_T)Gyro_B.DBG_TiltPercRef1_PI;

  /* S-Function (rti_commonblock): '<S115>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltPercRef1_PI_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion46);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion47' */
  Gyro_B.DataTypeConversion47_k = (real32_T)Gyro_B.DBG_TiltPercRef2_PI;

  /* S-Function (rti_commonblock): '<S116>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltPercRef2_PI_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion47_k);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion48' */
  Gyro_B.DataTypeConversion48 = (real32_T)Gyro_B.DBG_TiltPercRef1;

  /* S-Function (rti_commonblock): '<S117>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltPercRef1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion48);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion4' */
  Gyro_B.DataTypeConversion4_m = (real32_T)Gyro_B.GYRO_DriverErr2_l;

  /* S-Function (rti_commonblock): '<S118>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_DriverErr2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion4_m);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion49' */
  Gyro_B.DataTypeConversion49_j = (real32_T)Gyro_B.DBG_TiltPercRef2;

  /* S-Function (rti_commonblock): '<S119>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltPercRef2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion49_j);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion50' */
  Gyro_B.DataTypeConversion50 = (real32_T)Gyro_B.DBG_ComandoPompa;

  /* S-Function (rti_commonblock): '<S120>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_ComandoPompa_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion50);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion51' */
  Gyro_B.DataTypeConversion51 = (real32_T)Gyro_B.DBG_ScalingFactor_PI;

  /* S-Function (rti_commonblock): '<S121>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_ScalingFactor_PI_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion51);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion52' */
  Gyro_B.DataTypeConversion52 = (real32_T)Gyro_B.DBG_TiltSpeedRef1;

  /* S-Function (rti_commonblock): '<S122>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedRef1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion52);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion53' */
  Gyro_B.DataTypeConversion53 = (real32_T)Gyro_B.DBG_TiltSpeedRef2;

  /* S-Function (rti_commonblock): '<S123>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedRef2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion53);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion54' */
  Gyro_B.DataTypeConversion54 = (real32_T)Gyro_B.DBG_FF1;

  /* S-Function (rti_commonblock): '<S124>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_FF1_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion54);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion55' */
  Gyro_B.DataTypeConversion55 = (real32_T)Gyro_B.DBG_FF2;

  /* S-Function (rti_commonblock): '<S125>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_FF2_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion55);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion56' */
  Gyro_B.DataTypeConversion56 = (real32_T)Gyro_B.DBG_Roll;

  /* S-Function (rti_commonblock): '<S126>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_Roll_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion56);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion57' */
  Gyro_B.DataTypeConversion57 = (real32_T)Gyro_B.DBG_TiltSpeedRefH2;

  /* S-Function (rti_commonblock): '<S127>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedRefH2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion57);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion58' */
  Gyro_B.DataTypeConversion58 = (real32_T)Gyro_B.DBG_ScalingFactor;

  /* S-Function (rti_commonblock): '<S128>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_ScalingFactor_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion58);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion5' */
  Gyro_B.DataTypeConversion5 = (real32_T)Gyro_B.GYRO_MotorTempErr1_i;

  /* S-Function (rti_commonblock): '<S129>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_MotorTempErr1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion5);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion59' */
  Gyro_B.DataTypeConversion59 = (real32_T)Gyro_B.DBG_TiltSpeedRefH1;

  /* S-Function (rti_commonblock): '<S130>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedRefH1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion59);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion60' */
  Gyro_B.DataTypeConversion60 = (real32_T)Gyro_B.DBG_RollSetPoint;

  /* S-Function (rti_commonblock): '<S131>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_RollSetPoint_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion60);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion61' */
  Gyro_B.DataTypeConversion61 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_1;

  /* S-Function (rti_commonblock): '<S132>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion61);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion62' */
  Gyro_B.DataTypeConversion62 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_6;

  /* S-Function (rti_commonblock): '<S133>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_6_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion62);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion63' */
  Gyro_B.DataTypeConversion63 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_1;

  /* S-Function (rti_commonblock): '<S134>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion63);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion64' */
  Gyro_B.DataTypeConversion64 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_6;

  /* S-Function (rti_commonblock): '<S135>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_6_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion64);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion65' */
  Gyro_B.DataTypeConversion65 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_2;

  /* S-Function (rti_commonblock): '<S136>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion65);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion66' */
  Gyro_B.DataTypeConversion66 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_3;

  /* S-Function (rti_commonblock): '<S137>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_3_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion66);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion67' */
  Gyro_B.DataTypeConversion67 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_4;

  /* S-Function (rti_commonblock): '<S138>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_4_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion67);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion68' */
  Gyro_B.DataTypeConversion68 = (real32_T)Gyro_B.DBG_TiltSpeedLQR62_5;

  /* S-Function (rti_commonblock): '<S139>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR62_5_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion68);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion6' */
  Gyro_B.DataTypeConversion6_i = (real32_T)Gyro_B.GYRO_MotorTempErr2_k;

  /* S-Function (rti_commonblock): '<S140>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_MotorTempErr2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion6_i);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion69' */
  Gyro_B.DataTypeConversion69 = (real32_T)Gyro_B.DBG_TiltSpeed2LQR6;

  /* S-Function (rti_commonblock): '<S141>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeed2LQR6_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion69);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion70' */
  Gyro_B.DataTypeConversion70 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_2;

  /* S-Function (rti_commonblock): '<S142>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion70);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion71' */
  Gyro_B.DataTypeConversion71 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_3;

  /* S-Function (rti_commonblock): '<S143>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_3_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion71);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion72' */
  Gyro_B.DataTypeConversion72 = (real32_T)Gyro_B.DBG_TiltSpeed1LQR6;

  /* S-Function (rti_commonblock): '<S144>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeed1LQR6_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion72);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion73' */
  Gyro_B.DataTypeConversion73 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_4;

  /* S-Function (rti_commonblock): '<S145>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_4_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion73);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion74' */
  Gyro_B.DataTypeConversion74 = (real32_T)Gyro_B.DBG_TiltSpeedLQR61_5;

  /* S-Function (rti_commonblock): '<S146>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_TiltSpeedLQR61_5_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion74);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion75' */
  Gyro_B.DataTypeConversion75 = (real32_T)Gyro_B.BBS_RSpeed_p;

  /* S-Function (rti_commonblock): '<S147>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(BBS_RSpeed_Index_UsbFlRec, (Float32)
      Gyro_B.DataTypeConversion75);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion76' */
  Gyro_B.DataTypeConversion76 = (real32_T)Gyro_B.DBG_EnableDaPilota;

  /* S-Function (rti_commonblock): '<S148>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_EnableDaPilota_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion76);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion77' */
  Gyro_B.DataTypeConversion77 = (real32_T)Gyro_B.DBG_MasterEnable;

  /* S-Function (rti_commonblock): '<S149>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(DBG_MasterEnable_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion77);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion7' */
  Gyro_B.DataTypeConversion7 = (real32_T)Gyro_B.GYRO_MotorVoltErr1_c;

  /* S-Function (rti_commonblock): '<S150>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_MotorVoltErr1_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion7);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }

  /* DataTypeConversion: '<S71>/Data Type Conversion8' */
  Gyro_B.DataTypeConversion8 = (real32_T)Gyro_B.GYRO_MotorVoltErr2_n;

  /* S-Function (rti_commonblock): '<S151>/S-Function1' */
  /* This comment workarounds a code generation problem */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE Group:START */
    UInt16 tempU;
    Int16 errorCode = DSFLREC_USB_NO_ERROR;
    static UInt16 SecFullWarFlag = 0;
    static UInt16 DataLostWarFlag = 0;

    /* call routine for writting data in flash memory */
    errorCode = dsflrec_usb_write_float32(GYRO_MotorVoltErr2_Index_UsbFlRec,
      (Float32)Gyro_B.DataTypeConversion8);
    switch (errorCode)
    {

#ifdef DEBUG_POLL

     case DSFLREC_USB_DATA_SECTION_FULL:
      if (SecFullWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_SECTION_FULL_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        SecFullWarFlag = 1;
      }
      break;

     case DSFLREC_USB_OVERRUN:
      if (DataLostWarFlag == 0) {
        /* warning dialog enable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_OKCANCEL);
        rti_msg_warning_set(RTI_USB_FLREC_DATA_LOST_WARNING);

        /* warning dialog disable */
        msg_default_dialog_set(MSG_MC_WARNING, MSG_DLG_NONE);
        DataLostWarFlag = 1;
      }
      break;

     case DSFLREC_USB_FLASH_TIME_OUT:
      rti_msg_error_set(RTI_USB_FLREC_FLASH_ERROR);
      exit (1);
      break;

     case DSFLREC_USB_WRITE_NOT_READY:
      rti_msg_error_set(RTI_USB_FLREC_WR_NOT_RDY_ERROR);
      exit (1);
      break;

#endif

     default:
      break;
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_10ms(void)
{
  /* During 'TASK_10ms': '<S67>:200' */
  if (Gyro_DW.is_TASK_10ms == Gyro_IN_Start_up_10) {
    /* During 'Start_up_10': '<S67>:201' */
    /* Transition: '<S67>:196' */
    Gyro_DW.is_TASK_10ms = Gyro_IN_State_10;
    Gyro_DW.temporalCounter_i2_h = 0U;

    /* Outputs for Function Call SubSystem: '<S2>/MonitorMessages' */
    /* Entry 'State_10': '<S67>:198' */
    /* Event: '<S67>:192' */
    Gyro_MonitorMessages();

    /* End of Outputs for SubSystem: '<S2>/MonitorMessages' */
  } else {
    /* During 'State_10': '<S67>:198' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i2_h >=
         (uint32_T)floor(0.01 / rtP_Ts))) {
      /* Transition: '<S67>:199' */
      Gyro_DW.is_TASK_10ms = Gyro_IN_State_10;
      Gyro_DW.temporalCounter_i2_h = 0U;

      /* Outputs for Function Call SubSystem: '<S2>/MonitorMessages' */
      /* Entry 'State_10': '<S67>:198' */
      /* Event: '<S67>:192' */
      Gyro_MonitorMessages();

      /* End of Outputs for SubSystem: '<S2>/MonitorMessages' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_20ms(void)
{
  /* During 'TASK_20ms': '<S67>:205' */
  if (Gyro_DW.is_TASK_20ms == Gyro_IN_Start_up_20) {
    /* During 'Start_up_20': '<S67>:204' */
    /* Transition: '<S67>:206' */
    Gyro_DW.is_TASK_20ms = Gyro_IN_State_20;
    Gyro_DW.temporalCounter_i3 = 0U;

    /* Entry 'State_20': '<S67>:208' */
    /* Event: '<S67>:226' */
  } else {
    /* During 'State_20': '<S67>:208' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i3 >=
         (uint32_T)floor(0.02 / rtP_Ts))) {
      /* Transition: '<S67>:207' */
      Gyro_DW.is_TASK_20ms = Gyro_IN_State_20;
      Gyro_DW.temporalCounter_i3 = 0U;

      /* Entry 'State_20': '<S67>:208' */
      /* Event: '<S67>:226' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_100ms(void)
{
  /* During 'TASK_100ms': '<S67>:225' */
  if (Gyro_DW.is_TASK_100ms == Gyro_IN_Start_up_100) {
    /* During 'Start_up_100': '<S67>:222' */
    /* Transition: '<S67>:221' */
    Gyro_DW.is_TASK_100ms = Gyro_IN_State_100;
    Gyro_DW.temporalCounter_i4 = 0U;

    /* Entry 'State_100': '<S67>:224' */
    /* Event: '<S67>:203' */
  } else {
    /* During 'State_100': '<S67>:224' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i4 >=
         (uint32_T)floor(0.1 / rtP_Ts))) {
      /* Transition: '<S67>:220' */
      Gyro_DW.is_TASK_100ms = Gyro_IN_State_100;
      Gyro_DW.temporalCounter_i4 = 0U;

      /* Entry 'State_100': '<S67>:224' */
      /* Event: '<S67>:203' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_200ms(void)
{
  /* During 'TASK_200ms': '<S67>:230' */
  if (Gyro_DW.is_TASK_200ms == Gyro_IN_Start_up_200) {
    /* During 'Start_up_200': '<S67>:231' */
    /* Transition: '<S67>:228' */
    Gyro_DW.is_TASK_200ms = Gyro_IN_State_200;
    Gyro_DW.temporalCounter_i5 = 0U;

    /* Entry 'State_200': '<S67>:229' */
    /* Event: '<S67>:227' */
  } else {
    /* During 'State_200': '<S67>:229' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i5 >=
         (uint32_T)floor(0.2 / rtP_Ts))) {
      /* Transition: '<S67>:233' */
      Gyro_DW.is_TASK_200ms = Gyro_IN_State_200;
      Gyro_DW.temporalCounter_i5 = 0U;

      /* Entry 'State_200': '<S67>:229' */
      /* Event: '<S67>:227' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_1tick(void)
{
  /* During 'TASK_1tick': '<S67>:235' */
  if (Gyro_DW.is_TASK_1tick == Gyro_IN_Start_up_1) {
    /* During 'Start_up_1': '<S67>:237' */
    /* Transition: '<S67>:239' */
    Gyro_DW.is_TASK_1tick = Gyro_IN_State_1;
    Gyro_DW.temporalCounter_i1_o = 0U;

    /* Entry 'State_1': '<S67>:240' */
    /* Event: '<S67>:241' */
  } else {
    /* During 'State_1': '<S67>:240' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i1_o >=
         (uint32_T)floor(0.001 / rtP_Ts))) {
      /* Transition: '<S67>:238' */
      Gyro_DW.is_TASK_1tick = Gyro_IN_State_1;
      Gyro_DW.temporalCounter_i1_o = 0U;

      /* Entry 'State_1': '<S67>:240' */
      /* Event: '<S67>:241' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_TASK_2ms(void)
{
  /* During 'TASK_2ms': '<S67>:242' */
  if (Gyro_DW.is_TASK_2ms == Gyro_IN_Start_up_2) {
    /* During 'Start_up_2': '<S67>:243' */
    /* Transition: '<S67>:247' */
    Gyro_DW.is_TASK_2ms = Gyro_IN_State_2;
    Gyro_DW.temporalCounter_i6 = 0U;

    /* Outputs for Function Call SubSystem: '<S2>/SpinTiltMessages' */
    /* Entry 'State_2': '<S67>:244' */
    /* Event: '<S67>:248' */
    Gyro_SpinTiltMessages();

    /* End of Outputs for SubSystem: '<S2>/SpinTiltMessages' */
  } else {
    /* During 'State_2': '<S67>:244' */
    if ((Gyro_DW.sfEvent == Gyro_CALL_EVENT_i) && (Gyro_DW.temporalCounter_i6 >=
         (uint32_T)floor(0.002 / rtP_Ts))) {
      /* Transition: '<S67>:246' */
      Gyro_DW.is_TASK_2ms = Gyro_IN_State_2;
      Gyro_DW.temporalCounter_i6 = 0U;

      /* Outputs for Function Call SubSystem: '<S2>/SpinTiltMessages' */
      /* Entry 'State_2': '<S67>:244' */
      /* Event: '<S67>:248' */
      Gyro_SpinTiltMessages();

      /* End of Outputs for SubSystem: '<S2>/SpinTiltMessages' */
    }
  }
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_10ms(void)
{
  /* Entry Internal 'TASK_10ms': '<S67>:200' */
  /* Transition: '<S67>:197' */
  Gyro_DW.is_TASK_10ms = Gyro_IN_Start_up_10;
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_20ms(void)
{
  /* Entry Internal 'TASK_20ms': '<S67>:205' */
  /* Transition: '<S67>:209' */
  Gyro_DW.is_TASK_20ms = Gyro_IN_Start_up_20;
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_100ms(void)
{
  /* Entry Internal 'TASK_100ms': '<S67>:225' */
  /* Transition: '<S67>:223' */
  Gyro_DW.is_TASK_100ms = Gyro_IN_Start_up_100;
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_200ms(void)
{
  /* Entry Internal 'TASK_200ms': '<S67>:230' */
  /* Transition: '<S67>:232' */
  Gyro_DW.is_TASK_200ms = Gyro_IN_Start_up_200;
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_1tick(void)
{
  /* Entry Internal 'TASK_1tick': '<S67>:235' */
  /* Transition: '<S67>:236' */
  Gyro_DW.is_TASK_1tick = Gyro_IN_Start_up_1;
}

/* Function for Chart: '<S28>/F_TASK_GENERATOR' */
static void Gyro_enter_internal_TASK_2ms(void)
{
  /* Entry Internal 'TASK_2ms': '<S67>:242' */
  /* Transition: '<S67>:245' */
  Gyro_DW.is_TASK_2ms = Gyro_IN_Start_up_2;
}

/* Model output function */
void Gyro_output(void)
{
  ZCEventType zcEvent;

  /* S-Function (rti_commonblock): '<S19>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "PLC_Data01" Id:432 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "PLC_Data01_CNT" (8|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Driver_STAT" (16|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o2 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_TempMot2" (24|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o3 = 0.01 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "PLC_TempMot1" (40|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o4 = 0.01 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "PLC_Mot_Drv1_Err" (56|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o5 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Mot_Drv2_Err" (57|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o6 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Mot_Temp1_Err" (58|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o7 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Mot_Temp2_Err" (59|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o8 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Mot_Volt1_Err" (60|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o9 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_Mot_Volt2_Err" (61|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o10 = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S21>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "PLC_Data03" Id:434 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "PLC_ValveSpd2" (0|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o1_a = -3.2768 + ( 0.0001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_ValveSpd1" (16|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o2_n = -3.2768 + ( 0.0001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_ValvePos2" (32|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o3_h = -3.2768 + ( 0.0001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_ValvePos1" (48|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o4_g = -3.2768 + ( 0.0001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S20>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "PLC_Data02" Id:433 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "PLC_Curr2" (0|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o1_b = -20 + ( 0.001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_Curr1" (16|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o2_h = -20 + ( 0.001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_Spin2" (32|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o3_n = -20000 + ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "PLC_Spin1" (48|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o4_e = -20000 + ( ((real_T) CAN_Sgn.UnsignedSgn) );
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S22>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "PLC_Data04" Id:435 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->processed) {
      can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "PLC_Piston_Err" (8|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "PLC_ValvePerc2" (16|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o2_n4 = -100 + ( 0.01 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_ValvePerc1" (32|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o3_m = -100 + ( 0.01 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "PLC_PistonPress" (48|16, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o4_m = 0.01 * ( ((real_T) CAN_Sgn.UnsignedSgn) );
        }
      }
    }
  }

  /* BusCreator: '<S9>/Bus Creator' */
  Gyro_B.GyroBoxBus_b.GYRO_DriverStatus = Gyro_B.SFunction1_o2;
  Gyro_B.GyroBoxBus_b.GYRO_TempMot2 = Gyro_B.SFunction1_o3;
  Gyro_B.GyroBoxBus_b.GYRO_TempMot1 = Gyro_B.SFunction1_o4;
  Gyro_B.GyroBoxBus_b.GYRO_DriverErr1 = Gyro_B.SFunction1_o5;
  Gyro_B.GyroBoxBus_b.GYRO_DriverErr2 = Gyro_B.SFunction1_o6;
  Gyro_B.GyroBoxBus_b.GYRO_MotorTempErr1 = Gyro_B.SFunction1_o7;
  Gyro_B.GyroBoxBus_b.GYRO_MotorTempErr2 = Gyro_B.SFunction1_o8;
  Gyro_B.GyroBoxBus_b.GYRO_MotorVoltErr1 = Gyro_B.SFunction1_o9;
  Gyro_B.GyroBoxBus_b.GYRO_MotorVoltErr2 = Gyro_B.SFunction1_o10;
  Gyro_B.GyroBoxBus_b.GYRO_ValveSpeed2 = Gyro_B.SFunction1_o1_a;
  Gyro_B.GyroBoxBus_b.GYRO_ValveSpeed1 = Gyro_B.SFunction1_o2_n;
  Gyro_B.GyroBoxBus_b.GYRO_ValvePos2 = Gyro_B.SFunction1_o3_h;
  Gyro_B.GyroBoxBus_b.GYRO_ValvePos1 = Gyro_B.SFunction1_o4_g;
  Gyro_B.GyroBoxBus_b.GYRO_Current2 = Gyro_B.SFunction1_o1_b;
  Gyro_B.GyroBoxBus_b.GYRO_Current1 = Gyro_B.SFunction1_o2_h;
  Gyro_B.GyroBoxBus_b.GYRO_SpinSpeed2 = Gyro_B.SFunction1_o3_n;
  Gyro_B.GyroBoxBus_b.GYRO_SpinSpeed1 = Gyro_B.SFunction1_o4_e;
  Gyro_B.GyroBoxBus_b.GYRO_PistonErr = Gyro_B.SFunction1_o1_p;
  Gyro_B.GyroBoxBus_b.GYRO_ValvePerc2 = Gyro_B.SFunction1_o2_n4;
  Gyro_B.GyroBoxBus_b.GYRO_ValvePerc1 = Gyro_B.SFunction1_o3_m;
  Gyro_B.GyroBoxBus_b.GYRO_PistonPress = Gyro_B.SFunction1_o4_m;

  /* S-Function (rti_commonblock): '<S23>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "IMU_Data01" Id:372 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "IMU_yawRate" (0|16, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o1_pi = -163.84 + ( 0.005 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "IMU_CLU_STAT" (16|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_a = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_yawRateSTAT" (20|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o3_e = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_AY1" (32|16, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o4_a = -4.1768 + ( 0.000127465 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "IMU_Data01_CNT" (48|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o5_b = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_AY1_STAT" (52|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o6_e = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_Data01_CRC" (56|8, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o7_a = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S24>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "IMU_Data02" Id:376 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "IMU_rollRate" (0|16, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o1_l = -163.84 + ( 0.005 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "IMU_CLU_STAT5" (16|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_rollRateSTAT" (20|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o3_ed = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_CLU_DIAG" (24|8, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o4_o = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_AX1" (32|16, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o5_j = -4.1768 + ( 0.000127465 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "IMU_Data02_CNT" (48|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o6_a = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_AX1_STAT" (52|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o7_j = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_Data02_CRC" (56|8, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o8_j = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S25>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "IMU_Data03" Id:380 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "IMU_AZ1" (32|16, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x0000FFFF;
          Gyro_B.SFunction1_o1_h = -4.1768 + ( 0.000127465 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "IMU_Data03_CNT" (48|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_f = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_AZ1_STAT" (52|4, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o3_o = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "IMU_Data03_CRC" (56|8, standard signal, unsigned int, little endian) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o4_f = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* BusCreator: '<S10>/Bus Creator4' */
  Gyro_B.IMUdataBus_m.IMU_YawRate = Gyro_B.SFunction1_o1_pi;
  Gyro_B.IMUdataBus_m.IMU_Ay = Gyro_B.SFunction1_o4_a;
  Gyro_B.IMUdataBus_m.IMU_RollRate = Gyro_B.SFunction1_o1_l;
  Gyro_B.IMUdataBus_m.IMU_Ax = Gyro_B.SFunction1_o5_j;
  Gyro_B.IMUdataBus_m.IMU_Az = Gyro_B.SFunction1_o1_h;

  /* S-Function (rti_commonblock): '<S11>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "ABS_Data01" Id:34 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "ABS_Data01_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_n = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_Data01_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_g4 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_RSpeedSTAT" (12|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o3_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_FSpeedSTAT" (14|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o4_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_FPressW_STAT" (16|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o5_a = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_FpressWheel" (18|6, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x0000003F;
          Gyro_B.SFunction1_o6_b = 0.5 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "ABS_RSpeedTime" (24|20, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte2 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x000FFFFF;
          Gyro_B.SFunction1_o7_k = 0.2384185791 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) );

          /* ...... "ABS_FSpeedTime" (44|20, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte2 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x000FFFFF;
          Gyro_B.SFunction1_o8_m = 0.2384185791 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) );
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S12>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "ABS_Data02" Id:35 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "ABS_Data02_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_k = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_Data02_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_l = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_LongAccSTAT" (16|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o3_l = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_LongAcc" (18|14, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00003FFF;
          Gyro_B.SFunction1_o4_c = -8.192 + ( 0.001 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "ABS_VHC_STAT" (32|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o5_b0 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_RPressM_STAT" (34|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o6_al = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_RPressW_STAT" (36|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o7_h = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_FPressM_STAT" (38|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o8_mz = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_RpressSys" (40|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o9_a = 0.25 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "ABS_RpressWheel" (48|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o10_o = 0.25 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "ABS_FpressMaster" (56|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o11 = 0.25 * ( ((real_T) CAN_Sgn.UnsignedSgn) );
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S13>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "ABS_Data03" Id:392 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "ABS_Data03_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_l0 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_Data03_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_CornSTAT" (14|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o3_a = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_PitchSTAT" (16|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o4_k = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_Pitch" (18|14, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00003FFF;
          Gyro_B.SFunction1_o5_bp = -449.9997 + ( 0.0549316 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "ABS_LeanSTAT" (32|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o6_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_Lean" (34|14, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00003FFF;
          Gyro_B.SFunction1_o7_o = -449.9997 + ( 0.0549316 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );

          /* ...... "ABS_PitchRatSTAT" (48|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o8_b = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "ABS_PitchRate" (50|14, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00003FFF;
          Gyro_B.SFunction1_o9_e = -163.84 + ( 0.02 * ( ((real_T)
            CAN_Sgn.UnsignedSgn) ) );
        }
      }
    }
  }

  /* BusCreator: '<S6>/Bus Creator1' */
  Gyro_B.ABSdataBus_i.ABS_SPDFrontTime = Gyro_B.SFunction1_o8_m;
  Gyro_B.ABSdataBus_i.ABS_SPDRearTime = Gyro_B.SFunction1_o7_k;
  Gyro_B.ABSdataBus_i.ABS_BRKFrontPressure = Gyro_B.SFunction1_o6_b;
  Gyro_B.ABSdataBus_i.ABS_BRKRearPressure = Gyro_B.SFunction1_o10_o;
  Gyro_B.ABSdataBus_i.ABS_BRKMasterFrontPressure = Gyro_B.SFunction1_o11;
  Gyro_B.ABSdataBus_i.ABS_BRKMasterRearPressure = Gyro_B.SFunction1_o9_a;
  Gyro_B.ABSdataBus_i.ABS_Ax = Gyro_B.SFunction1_o4_c;
  Gyro_B.ABSdataBus_i.ABS_Roll = Gyro_B.SFunction1_o7_o;
  Gyro_B.ABSdataBus_i.ABS_Pitch = Gyro_B.SFunction1_o5_bp;
  Gyro_B.ABSdataBus_i.ABS_PitchRate = Gyro_B.SFunction1_o9_e;

  /* S-Function (rti_commonblock): '<S16>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "DSB_Data01" Id:32 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "DSB_Data01_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_c = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_Data01_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_gu = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_RidingMode" (14|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o3_ii = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_GENERR" (16|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o4_cv = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_CruisSet" (20|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o5_c = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_CruisRes" (21|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o6_n = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Cruise" (22|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o7_b = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Hazard" (23|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o8_ja = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Claxon" (24|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o9_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Enter" (25|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o10_j = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_HI_LO" (26|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o11_c = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_IndRes" (27|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o12 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Left" (28|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o13 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Right" (29|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o14 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Down" (30|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o15 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_Up" (31|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o16 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_FogBeam" (32|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o17 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_But_HGrip" (33|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o18 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_DWCStartStop" (40|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o19 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_DwcLev" (41|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o20 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_DTCStartStop" (44|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o21 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_DtcLev" (45|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o22 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LeftTurn" (58|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o23 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_RightTurn" (59|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o24 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LCornBeam" (60|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o25 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_RCornBeam" (61|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o26 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_HighBeam" (62|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o27 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LowBeam" (63|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o28 = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* BusCreator: '<S8>/Bus Creator' */
  Gyro_B.DSBdataBus_i.DSB_DTCLevel = Gyro_B.SFunction1_o22;
  Gyro_B.DSBdataBus_i.DSB_Flash = Gyro_B.SFunction1_o27;
  Gyro_B.DSBdataBus_i.DSB_RidingMode = Gyro_B.SFunction1_o3_ii;
  Gyro_B.DSBdataBus_i.DSB_But_IndRes = Gyro_B.SFunction1_o12;

  /* S-Function (rti_commonblock): '<S14>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "BBS_Data01_Copy" Id:280 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "BBS_Data01_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_Data01_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_e = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_ExvlPot_ERR2" (12|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o3_k = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_ExvlPot_ERR1" (13|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o4_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DAVC_STAT" (14|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o5_o = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DWC_STAT" (22|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o6_m = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DWCLevACK" (24|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o7_au = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DTC_STAT" (27|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o8_b0 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DTCLevACK" (29|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o9_d = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_RSpeed" (32|13, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x00001FFF;
          Gyro_B.SFunction1_o10_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_RSpeedSTAT" (45|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o11_o = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_ExvlCheck" (47|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o12_n = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_ExvlMot_ERR" (48|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o13_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_FSpeedSTAT" (49|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o14_c = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_FSpeed" (51|13, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00001FFF;
          Gyro_B.SFunction1_o15_h = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S15>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "BBS_Data02_Copy" Id:281 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "BBS_Data02_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_cv = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_Data02_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_k = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqReqRel" (12|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o3_h5 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqReqSTAT" (13|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o4_ge = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_DTCwork" (14|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o5_i = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqRedFast" (24|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o6_f = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqRedSlow" (32|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o7_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqReqFast" (40|12, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x00000FFF;
          Gyro_B.SFunction1_o8_a = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "BBS_TorqReqSlow" (52|12, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000FFF;
          Gyro_B.SFunction1_o9_p = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* BusCreator: '<S7>/Bus Creator' */
  Gyro_B.BBSdataBus_n.BBS_Data01_CRC = Gyro_B.SFunction1_o1_g;
  Gyro_B.BBSdataBus_n.BBS_Data01_CNT = Gyro_B.SFunction1_o2_e;
  Gyro_B.BBSdataBus_n.BBS_ExvlPot_ERR2 = Gyro_B.SFunction1_o3_k;
  Gyro_B.BBSdataBus_n.BBS_ExvlPot_ERR1 = Gyro_B.SFunction1_o4_i;
  Gyro_B.BBSdataBus_n.BBS_DAVC_STAT = Gyro_B.SFunction1_o5_o;
  Gyro_B.BBSdataBus_n.BBS_DWC_STAT = Gyro_B.SFunction1_o6_m;
  Gyro_B.BBSdataBus_n.BBS_DWCLevACK = Gyro_B.SFunction1_o7_au;
  Gyro_B.BBSdataBus_n.BBS_DTC_STAT = Gyro_B.SFunction1_o8_b0;
  Gyro_B.BBSdataBus_n.BBS_DTCLevACK = Gyro_B.SFunction1_o9_d;
  Gyro_B.BBSdataBus_n.BBS_RSpeed = Gyro_B.SFunction1_o10_i;
  Gyro_B.BBSdataBus_n.BBS_RSpeedSTAT = Gyro_B.SFunction1_o11_o;
  Gyro_B.BBSdataBus_n.BBS_ExvlCheck = Gyro_B.SFunction1_o12_n;
  Gyro_B.BBSdataBus_n.BBS_ExvlMot_ERR = Gyro_B.SFunction1_o13_p;
  Gyro_B.BBSdataBus_n.BBS_FSpeedSTAT = Gyro_B.SFunction1_o14_c;
  Gyro_B.BBSdataBus_n.BBS_FSpeed = Gyro_B.SFunction1_o15_h;
  Gyro_B.BBSdataBus_n.BBS_Data02_CRC = Gyro_B.SFunction1_o1_cv;
  Gyro_B.BBSdataBus_n.BBS_Data02_CNT = Gyro_B.SFunction1_o2_k;
  Gyro_B.BBSdataBus_n.BBS_TorqReqRel = Gyro_B.SFunction1_o3_h5;
  Gyro_B.BBSdataBus_n.BBS_TorqReqSTAT = Gyro_B.SFunction1_o4_ge;
  Gyro_B.BBSdataBus_n.BBS_DTCwork = Gyro_B.SFunction1_o5_i;
  Gyro_B.BBSdataBus_n.BBS_TorqRedFast = Gyro_B.SFunction1_o6_f;
  Gyro_B.BBSdataBus_n.BBS_TorqRedSlow = Gyro_B.SFunction1_o7_p;
  Gyro_B.BBSdataBus_n.BBS_TorqReqFast = Gyro_B.SFunction1_o8_a;
  Gyro_B.BBSdataBus_n.BBS_TorqReqSlow = Gyro_B.SFunction1_o9_p;

  /* BusCreator: '<S1>/Bus Creator10' */
  Gyro_B.InputBus_d.GyroBoxBus = Gyro_B.GyroBoxBus_b;
  Gyro_B.InputBus_d.IMUdataBus = Gyro_B.IMUdataBus_m;
  Gyro_B.InputBus_d.ABSdataBus = Gyro_B.ABSdataBus_i;
  Gyro_B.InputBus_d.DSBdataBus = Gyro_B.DSBdataBus_i;
  Gyro_B.InputBus_d.BBSdataBus = Gyro_B.BBSdataBus_n;

  /* ModelReference: '<Root>/Main' */
  GyroMain(&Gyro_B.InputBus_d, &Gyro_B.GYRO_DriverStatus, &Gyro_B.GYRO_TempMot2,
           &Gyro_B.GYRO_TempMot1, &Gyro_B.GYRO_DriverErr1,
           &Gyro_B.GYRO_DriverErr2, &Gyro_B.GYRO_MotorTempErr1,
           &Gyro_B.GYRO_MotorTempErr2, &Gyro_B.GYRO_MotorVoltErr1,
           &Gyro_B.GYRO_MotorVoltErr2, &Gyro_B.GYRO_ValveSpeed2,
           &Gyro_B.GYRO_ValveSpeed1, &Gyro_B.GYRO_ValvePos2,
           &Gyro_B.GYRO_ValvePos1, &Gyro_B.GYRO_Current2, &Gyro_B.GYRO_Current1,
           &Gyro_B.GYRO_SpinSpeed2, &Gyro_B.GYRO_SpinSpeed1,
           &Gyro_B.GYRO_PistonErr, &Gyro_B.GYRO_ValvePerc2,
           &Gyro_B.GYRO_ValvePerc1, &Gyro_B.GYRO_PistonPress,
           &Gyro_B.IMU_YawRate, &Gyro_B.IMU_Ay, &Gyro_B.IMU_RollRate,
           &Gyro_B.IMU_Ax, &Gyro_B.IMU_Az, &Gyro_B.ABS_SPDFrontTime,
           &Gyro_B.ABS_SPDRearTime, &Gyro_B.ABS_BRKFrontPressure,
           &Gyro_B.ABS_BRKRearPressure, &Gyro_B.ABS_BRKMasterFrontPressure,
           &Gyro_B.ABS_BRKMasterRearPressure, &Gyro_B.ABS_Ax, &Gyro_B.ABS_Roll,
           &Gyro_B.ABS_Pitch, &Gyro_B.ABS_PitchRate, &Gyro_B.DSB_DTCLevel,
           &Gyro_B.DSB_Flash, &Gyro_B.DSB_RidingMode, &Gyro_B.DSB_But_IndRes,
           &Gyro_B.BBS_Data01_CRC, &Gyro_B.BBS_Data01_CNT,
           &Gyro_B.BBS_ExvlPot_ERR2, &Gyro_B.BBS_ExvlPot_ERR1,
           &Gyro_B.BBS_DAVC_STAT, &Gyro_B.BBS_DWC_STAT, &Gyro_B.BBS_DWCLevACK,
           &Gyro_B.BBS_DTC_STAT, &Gyro_B.BBS_DTCLevACK, &Gyro_B.BBS_RSpeed,
           &Gyro_B.BBS_RSpeedSTAT, &Gyro_B.BBS_ExvlCheck,
           &Gyro_B.BBS_ExvlMot_ERR, &Gyro_B.BBS_FSpeedSTAT, &Gyro_B.BBS_FSpeed,
           &Gyro_B.BBS_Data02_CRC, &Gyro_B.BBS_Data02_CNT,
           &Gyro_B.BBS_TorqReqRel, &Gyro_B.BBS_TorqReqSTAT, &Gyro_B.BBS_DTCwork,
           &Gyro_B.BBS_TorqRedFast, &Gyro_B.BBS_TorqRedSlow,
           &Gyro_B.BBS_TorqReqFast, &Gyro_B.BBS_TorqReqSlow,
           &Gyro_B.SpinSpeedRef, &Gyro_B.SpinCurrentRef, &Gyro_B.SpinStartStop,
           &Gyro_B.SpinModCont, &Gyro_B.TiltModCont, &Gyro_B.TiltStartStop,
           &Gyro_B.TiltValvePerc1, &Gyro_B.TiltValvePerc2, &Gyro_B.TiltPosRef,
           &Gyro_B.TiltSpeedRef, &Gyro_B.ComandoPompa, &Gyro_B.TiltPosPerc,
           &Gyro_B.DTCwork_Status, &Gyro_B.GYRO_DriverStatus_g,
           &Gyro_B.GYRO_TempMot2_m, &Gyro_B.GYRO_TempMot1_e,
           &Gyro_B.GYRO_DriverErr1_e, &Gyro_B.GYRO_DriverErr2_l,
           &Gyro_B.GYRO_MotorTempErr1_i, &Gyro_B.GYRO_MotorTempErr2_k,
           &Gyro_B.GYRO_MotorVoltErr1_c, &Gyro_B.GYRO_MotorVoltErr2_n,
           &Gyro_B.GYRO_ValveSpeed2_p, &Gyro_B.GYRO_ValveSpeed1_b,
           &Gyro_B.GYRO_ValvePos2_o, &Gyro_B.GYRO_ValvePos1_p,
           &Gyro_B.GYRO_Current2_k, &Gyro_B.GYRO_Current1_f,
           &Gyro_B.GYRO_SpinSpeed2_j, &Gyro_B.GYRO_SpinSpeed1_o,
           &Gyro_B.GYRO_PistonErr_d, &Gyro_B.GYRO_ValvePerc2_i,
           &Gyro_B.GYRO_ValvePerc1_l, &Gyro_B.GYRO_PistonPress_f,
           &Gyro_B.IMU_YawRate_b, &Gyro_B.IMU_Ay_k, &Gyro_B.IMU_RollRate_e,
           &Gyro_B.IMU_Ax_b, &Gyro_B.IMU_Az_k, &Gyro_B.ABS_SPDFrontTime_e,
           &Gyro_B.ABS_SPDRearTime_n, &Gyro_B.ABS_BRKFrontPressure_g,
           &Gyro_B.ABS_BRKRearPressure_f, &Gyro_B.ABS_BRKMasterFrontPressure_p,
           &Gyro_B.ABS_BRKMasterRearPressure_e, &Gyro_B.ABS_Ax_l,
           &Gyro_B.ABS_Roll_a, &Gyro_B.ABS_Pitch_p, &Gyro_B.ABS_PitchRate_n,
           &Gyro_B.DSB_DTCLevel_a, &Gyro_B.DSB_Flash_c, &Gyro_B.DSB_RidingMode_k,
           &Gyro_B.DSB_But_IndRes_l, &Gyro_B.BBS_Data01_CRC_l,
           &Gyro_B.BBS_Data01_CNT_o, &Gyro_B.BBS_ExvlPot_ERR2_e,
           &Gyro_B.BBS_ExvlPot_ERR1_m, &Gyro_B.BBS_DAVC_STAT_d,
           &Gyro_B.BBS_DWC_STAT_n, &Gyro_B.BBS_DWCLevACK_k,
           &Gyro_B.BBS_DTC_STAT_p, &Gyro_B.BBS_DTCLevACK_o, &Gyro_B.BBS_RSpeed_p,
           &Gyro_B.BBS_RSpeedSTAT_f, &Gyro_B.BBS_ExvlCheck_c,
           &Gyro_B.BBS_ExvlMot_ERR_i, &Gyro_B.BBS_FSpeedSTAT_i,
           &Gyro_B.BBS_FSpeed_l, &Gyro_B.BBS_Data02_CRC_p,
           &Gyro_B.BBS_Data02_CNT_n, &Gyro_B.BBS_TorqReqRel_a,
           &Gyro_B.BBS_TorqReqSTAT_k, &Gyro_B.BBS_DTCwork_h,
           &Gyro_B.BBS_TorqRedFast_b, &Gyro_B.BBS_TorqRedSlow_c,
           &Gyro_B.BBS_TorqReqFast_e, &Gyro_B.BBS_TorqReqSlow_h,
           &Gyro_B.DBG_SpinSpeedRef, &Gyro_B.DBG_SpinCurrentRef,
           &Gyro_B.DBG_SpinStartStop, &Gyro_B.DBG_SpinModCont,
           &Gyro_B.DBG_ScalingFactor_PI, &Gyro_B.DBG_TiltSpeedRef,
           &Gyro_B.DBG_TiltPosRef, &Gyro_B.DBG_EnableDaPilota,
           &Gyro_B.DBG_TiltPercRef1, &Gyro_B.DBG_TiltModCont,
           &Gyro_B.DBG_TiltStartStop, &Gyro_B.DBG_TiltPercRef2,
           &Gyro_B.DBG_ComandoPompa, &Gyro_B.DBG_TiltPercRef1_PI,
           &Gyro_B.DBG_TiltPercRef2_PI, &Gyro_B.DBG_FF1, &Gyro_B.DBG_FF2,
           &Gyro_B.DBG_TiltSpeedRef1, &Gyro_B.DBG_TiltSpeedRef2,
           &Gyro_B.DBG_MasterEnable, &Gyro_B.DBG_EnableTilt,
           &Gyro_B.DBG_EnableSpin, &Gyro_B.DBG_TiltSpeedEstimate1,
           &Gyro_B.DBG_TiltSpeedEstimate2, &Gyro_B.DBG_RollRate,
           &Gyro_B.DBG_RollRate_GxGz, &Gyro_B.DBG_Roll,
           &Gyro_B.DBG_TiltSpeedRefH2, &Gyro_B.DBG_ScalingFactor,
           &Gyro_B.DBG_TiltSpeedRefH1, &Gyro_B.DBG_TiltSpeedLQR61_1,
           &Gyro_B.DBG_TiltSpeedLQR61_6, &Gyro_B.DBG_TiltSpeedLQR62_1,
           &Gyro_B.DBG_TiltSpeedLQR62_6, &Gyro_B.DBG_TiltSpeedLQR62_2,
           &Gyro_B.DBG_TiltSpeedLQR62_3, &Gyro_B.DBG_TiltSpeedLQR62_4,
           &Gyro_B.DBG_TiltSpeedLQR62_5, &Gyro_B.DBG_TiltSpeed2LQR6,
           &Gyro_B.DBG_TiltSpeedLQR61_2, &Gyro_B.DBG_TiltSpeedLQR61_3,
           &Gyro_B.DBG_TiltSpeed1LQR6, &Gyro_B.DBG_TiltSpeedLQR61_4,
           &Gyro_B.DBG_TiltSpeedLQR61_5, &Gyro_B.DBG_RollSetPoint, &Gyro_B.A);

  /* Chart: '<S28>/F_TASK_GENERATOR' */

  /* Gateway: Output_Data/Tasks & priority/F_TASK_GENERATOR */
  Gyro_DW.sfEvent = Gyro_CALL_EVENT_i;
  if (Gyro_DW.temporalCounter_i1_o < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i1_o++;
  }

  if (Gyro_DW.temporalCounter_i2_h < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i2_h++;
  }

  if (Gyro_DW.temporalCounter_i3 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i3++;
  }

  if (Gyro_DW.temporalCounter_i4 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i4++;
  }

  if (Gyro_DW.temporalCounter_i5 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i5++;
  }

  if (Gyro_DW.temporalCounter_i6 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i6++;
  }

  /* During: Output_Data/Tasks & priority/F_TASK_GENERATOR */
  if (Gyro_DW.is_active_c1_Gyro == 0U) {
    /* Entry: Output_Data/Tasks & priority/F_TASK_GENERATOR */
    Gyro_DW.is_active_c1_Gyro = 1U;

    /* Entry Internal: Output_Data/Tasks & priority/F_TASK_GENERATOR */
    /* Entry Internal 'TASK_GENERATOR': '<S67>:11' */
    Gyro_enter_internal_TASK_10ms();
    Gyro_enter_internal_TASK_20ms();
    Gyro_enter_internal_TASK_100ms();
    Gyro_enter_internal_TASK_200ms();
    Gyro_enter_internal_TASK_1tick();
    Gyro_enter_internal_TASK_2ms();
  } else {
    /* During 'TASK_GENERATOR': '<S67>:11' */
    Gyro_TASK_10ms();
    Gyro_TASK_20ms();
    Gyro_TASK_100ms();
    Gyro_TASK_200ms();
    Gyro_TASK_1tick();
    Gyro_TASK_2ms();
  }

  /* End of Chart: '<S28>/F_TASK_GENERATOR' */

  /* Chart: '<S4>/TASK_LOG_GENERATOR' */

  /* Gateway: Save_Data/TASK_LOG_GENERATOR */
  if (Gyro_DW.temporalCounter_i1 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i1++;
  }

  if (Gyro_DW.temporalCounter_i2 < MAX_uint32_T) {
    Gyro_DW.temporalCounter_i2++;
  }

  /* During: Save_Data/TASK_LOG_GENERATOR */
  if (Gyro_DW.is_active_c4_Gyro == 0U) {
    /* Entry: Save_Data/TASK_LOG_GENERATOR */
    Gyro_DW.is_active_c4_Gyro = 1U;

    /* Entry Internal: Save_Data/TASK_LOG_GENERATOR */
    /* Entry Internal 'TASK_GENERATOR': '<S73>:11' */
    /* Entry Internal 'TASK_1': '<S73>:243' */
    /* Transition: '<S73>:197' */
    Gyro_DW.is_TASK_1 = Gyro_IN_Start_up;

    /* Entry Internal 'TASK_2': '<S73>:244' */
    /* Transition: '<S73>:245' */
    Gyro_DW.is_TASK_2 = Gyro_IN_Start_up;
  } else {
    /* During 'TASK_GENERATOR': '<S73>:11' */
    /* During 'TASK_1': '<S73>:243' */
    if (Gyro_DW.is_TASK_1 == Gyro_IN_Start_up) {
      /* During 'Start_up': '<S73>:201' */
      if (rtP_T_log1 > 0.0) {
        /* Transition: '<S73>:196' */
        Gyro_DW.is_TASK_1 = Gyro_IN_State_1;
        Gyro_DW.temporalCounter_i1 = 0U;

        /* Outputs for Function Call SubSystem: '<S4>/Log1' */
        /* Entry 'State_1': '<S73>:198' */
        /* Event: '<S73>:192' */
        Gyro_Log1();

        /* End of Outputs for SubSystem: '<S4>/Log1' */
      }
    } else {
      /* During 'State_1': '<S73>:198' */
      if (Gyro_DW.temporalCounter_i1 >= (uint32_T)ceil(rtP_T_log1 / rtP_Ts)) {
        /* Transition: '<S73>:199' */
        Gyro_DW.is_TASK_1 = Gyro_IN_State_1;
        Gyro_DW.temporalCounter_i1 = 0U;

        /* Outputs for Function Call SubSystem: '<S4>/Log1' */
        /* Entry 'State_1': '<S73>:198' */
        /* Event: '<S73>:192' */
        Gyro_Log1();

        /* End of Outputs for SubSystem: '<S4>/Log1' */
      }
    }

    /* During 'TASK_2': '<S73>:244' */
    if (Gyro_DW.is_TASK_2 == Gyro_IN_Start_up) {
      /* During 'Start_up': '<S73>:248' */
      if (rtP_T_log2 > 0.0) {
        /* Transition: '<S73>:247' */
        Gyro_DW.is_TASK_2 = Gyro_IN_State_1;
        Gyro_DW.temporalCounter_i2 = 0U;

        /* Entry 'State_1': '<S73>:249' */
        /* Event: '<S73>:251' */
      }
    } else {
      /* During 'State_1': '<S73>:249' */
      if (Gyro_DW.temporalCounter_i2 >= (uint32_T)ceil(rtP_T_log2 / rtP_Ts)) {
        /* Transition: '<S73>:246' */
        Gyro_DW.is_TASK_2 = Gyro_IN_State_1;
        Gyro_DW.temporalCounter_i2 = 0U;

        /* Entry 'State_1': '<S73>:249' */
        /* Event: '<S73>:251' */
      }
    }
  }

  /* End of Chart: '<S4>/TASK_LOG_GENERATOR' */
  /* S-Function (rti_commonblock): '<S17>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "DSB_Data02" Id:768 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "DSB_Data02_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_h1 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_Data02_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_c = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_AirTempERR_2" (12|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o3_lt = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_AirTempERR_1" (13|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o4_c5 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_Odo1" (14|18, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[5];
          CAN_Sgn.SgnBytes.Byte2 = CAN_Msg[4];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x0003FFFF;
          Gyro_B.SFunction1_o5_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_InhibHL" (32|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o6_pm = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_SwComp" (33|5, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 1;
          CAN_Sgn.UnsignedSgn &= 0x0000001F;
          Gyro_B.SFunction1_o7_m = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_ABS_Switch" (38|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[3];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o8_mv = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_ABS_Lev" (50|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o9_f = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_ApsLev" (52|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o10_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_FlagOdo" (54|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o11_d = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_AirTemp" (56|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o12_m = -40 + ( ((real_T) CAN_Sgn.UnsignedSgn) );
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S18>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE RTICAN RX Message Block: "DSB_Data03" Id:560 */
  {
    UInt32 *CAN_Msg;

    /* ... Read status and timestamp info (previous message) */
    can_tp1_msg_read_from_mem(can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]);

    /* Convert timestamp */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->processed) {
      can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->timestamp =
        rtk_dsts_time_to_simtime_convert
        (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->timestamp);
    }

    /* Messages with timestamp zero have been received in pause/stop state
       and must not be handled.
     */
    if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->timestamp > 0.0) {
      if (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->processed) {
        CAN_Msg = can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230]->data;

        /* ... Decode CAN Message */
        {
          rtican_Signal_t CAN_Sgn;

          /* ...... "DSB_Data03_CRC" (0|8, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[7];
          CAN_Sgn.UnsignedSgn &= 0x000000FF;
          Gyro_B.SFunction1_o1_f = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_Data03_CNT" (8|4, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn &= 0x0000000F;
          Gyro_B.SFunction1_o2_an = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LIN_node_ERR" (12|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o3_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LIN_mute_ERR" (13|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o4_aq = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LIN_CNT_ERR" (14|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o5_av = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LIN_CRC_ERR" (15|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[6];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o6_j = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_LoadLev" (16|2, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn &= 0x00000003;
          Gyro_B.SFunction1_o7_n = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_PreloadReq" (18|6, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[5];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x0000003F;
          Gyro_B.SFunction1_o8_l = 0.5 * ( ((real_T) CAN_Sgn.UnsignedSgn) );

          /* ...... "DSB_Odo2" (24|18, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[4];
          CAN_Sgn.SgnBytes.Byte1 = CAN_Msg[3];
          CAN_Sgn.SgnBytes.Byte2 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn &= 0x0003FFFF;
          Gyro_B.SFunction1_o9_p5 = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_FogERR_2" (42|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 2;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o10_b = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_FogERR_1" (43|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o11_e = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_lamp_D_N" (44|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 4;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o12_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_DAIR_chkLamp" (45|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 5;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o13_o = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_HgripERR_2" (46|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 6;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o14_f = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_HgripERR_1" (47|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[2];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o15_b = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_PreloadReset" (48|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o16_m = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_PINen" (55|1, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[1];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 7;
          CAN_Sgn.UnsignedSgn &= 0x00000001;
          Gyro_B.SFunction1_o17_p = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_RearSusp" (56|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o18_g = ((real_T) CAN_Sgn.UnsignedSgn);

          /* ...... "DSB_FrontSusp" (59|3, standard signal, unsigned int, motorola back.) */
          CAN_Sgn.SgnBytes.Byte0 = CAN_Msg[0];
          CAN_Sgn.UnsignedSgn = ((UInt32)CAN_Sgn.UnsignedSgn) >> 3;
          CAN_Sgn.UnsignedSgn &= 0x00000007;
          Gyro_B.SFunction1_o19_j = ((real_T) CAN_Sgn.UnsignedSgn);
        }
      }
    }
  }

  /* S-Function (rti_commonblock): '<S154>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* DataTypeConversion: '<S5>/Data Type Conversion' */
  Gyro_B.DataTypeConversion = Gyro_B.SFunction1_o2_cg;

  /* S-Function (rti_commonblock): '<S153>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* dSPACE I/O Board DS1401BASE #1 Unit:POWER */
  if (Gyro_B.DataTypeConversion == 1) {
    simState = 0;                      /* stop */
  }

  /* Outputs for Triggered SubSystem: '<S5>/USB_FLIGHT_REC_EJECT' incorporates:
   *  TriggerPort: '<S155>/Trigger'
   */
  zcEvent = rt_ZCFcn(RISING_ZERO_CROSSING,
                     &Gyro_PrevZCX.USB_FLIGHT_REC_EJECT_Trig_ZCE,
                     (Gyro_B.DataTypeConversion));
  if (zcEvent != NO_ZCEVENT) {
    /* S-Function (rti_commonblock): '<S155>/S-Function1' */
    /* This comment workarounds a code generation problem */
    dsflrec_usb_eject();
  }

  /* End of Outputs for SubSystem: '<S5>/USB_FLIGHT_REC_EJECT' */
  /* S-Function (rti_commonblock): '<S152>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* S-Function (rti_commonblock): '<S156>/S-Function1' */
  /* This comment workarounds a code generation problem */

  /* S-Function (rti_commonblock): '<S157>/S-Function1' */
  /* This comment workarounds a code generation problem */
}

/* Model update function */
void Gyro_update(void)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Gyro_M->Timing.clockTick0)) {
    ++Gyro_M->Timing.clockTickH0;
  }

  Gyro_M->Timing.taskTime0 = Gyro_M->Timing.clockTick0 *
    Gyro_M->Timing.stepSize0 + Gyro_M->Timing.clockTickH0 *
    Gyro_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void Gyro_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Gyro_M, 0,
                sizeof(RT_MODEL_Gyro_T));
  Gyro_M->Timing.stepSize0 = 0.001;

  /* block I/O */
  (void) memset(((void *) &Gyro_B), 0,
                sizeof(B_Gyro_T));

  /* states (dwork) */
  (void) memset((void *)&Gyro_DW, 0,
                sizeof(DW_Gyro_T));

  {
    /* user code (registration function declaration) */
    /*Initialize global TRC pointers. */
    Gyro_rti_init_trc_pointers();
  }

  /* Model Initialize function for ModelReference Block: '<Root>/Main' */
  GyroMain_initialize(rtmGetErrorStatusPointer(Gyro_M));

  /* Start for ModelReference: '<Root>/Main' */
  GyroMain_Start();

  /* user code (Start function Trailer) */

  /* Level 5: Final initialization of RPCU systems  */
  Gyro_PrevZCX.USB_FLIGHT_REC_EJECT_Trig_ZCE = UNINITIALIZED_ZCSIG;

  /* SystemInitialize for ModelReference: '<Root>/Main' */
  GyroMain_Init();
  Gyro_DW.sfEvent = Gyro_CALL_EVENT_i;
  Gyro_DW.is_TASK_100ms = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i4 = 0U;
  Gyro_DW.is_TASK_10ms = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i2_h = 0U;
  Gyro_DW.is_TASK_1tick = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i1_o = 0U;
  Gyro_DW.is_TASK_200ms = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i5 = 0U;
  Gyro_DW.is_TASK_20ms = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i3 = 0U;
  Gyro_DW.is_TASK_2ms = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i6 = 0U;
  Gyro_DW.is_active_c1_Gyro = 0U;

  /* SystemInitialize for Chart: '<S28>/F_TASK_GENERATOR' incorporates:
   *  SubSystem: '<S2>/MonitorMessages'
   */
  Gyro_MonitorMessages_Init();

  /* SystemInitialize for Chart: '<S4>/TASK_LOG_GENERATOR' */
  Gyro_DW.is_TASK_1 = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i1 = 0U;
  Gyro_DW.is_TASK_2 = Gyro_IN_NO_ACTIVE_CHILD_a;
  Gyro_DW.temporalCounter_i2 = 0U;
  Gyro_DW.is_active_c4_Gyro = 0U;

  /* Enable for ModelReference: '<Root>/Main' */
  GyroMain_Enable();
}

/* Model terminate function */
void Gyro_terminate(void)
{
  /* Terminate for S-Function (rti_commonblock): '<S19>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "PLC_Data01" Id:432 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][3] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S21>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "PLC_Data03" Id:434 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][4] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S20>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "PLC_Data02" Id:433 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][3] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S22>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "PLC_Data04" Id:435 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][4] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S23>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "IMU_Data01" Id:372 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][2] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S24>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "IMU_Data02" Id:376 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][2] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S25>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "IMU_Data03" Id:380 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][2] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S11>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "ABS_Data01" Id:34 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][0] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S12>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "ABS_Data02" Id:35 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][1] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S13>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "ABS_Data03" Id:392 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][3] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S16>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "DSB_Data01" Id:32 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][0] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S14>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "BBS_Data01_Copy" Id:280 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][1] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S15>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "BBS_Data02_Copy" Id:281 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][1] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for Chart: '<S28>/F_TASK_GENERATOR' incorporates:
   *  SubSystem: '<S2>/MonitorMessages'
   */
  Gyro_MonitorMessages_Term();

  /* Terminate for Chart: '<S28>/F_TASK_GENERATOR' incorporates:
   *  SubSystem: '<S2>/SpinTiltMessages'
   */
  Gyro_SpinTiltMessages_Term();

  /* Terminate for S-Function (rti_commonblock): '<S17>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "DSB_Data02" Id:768 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][5] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* Terminate for S-Function (rti_commonblock): '<S18>/S-Function1' */

  /* dSPACE RTICAN RX Message Block: "DSB_Data03" Id:560 */
  {
    /* ... Set the message into sleep mode */
    while ((rtican_type1_tq_error[0][4] = can_tp1_msg_sleep
            (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230])) ==
           DSMCOM_BUFFER_OVERFLOW) ;
  }

  /* user code (Terminate function Trailer) */

  /* Level 6: Final termination of RPCU systems  */
  if (Gyro_B.DataTypeConversion == 1) {
    ds1401_power_hold_off();           /* MABX power down */
  }
}
