/*
 * Gyro_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Gyro".
 *
 * Model version              : 1.805
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Oct 18 11:47:11 2019
 *
 * Target selection: rti1401.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Custom Processor->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Gyro_types_h_
#define RTW_HEADER_Gyro_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef DEFINED_TYPEDEF_FOR_ABSdataBus_
#define DEFINED_TYPEDEF_FOR_ABSdataBus_

typedef struct {
  real_T ABS_SPDFrontTime;
  real_T ABS_SPDRearTime;
  real_T ABS_BRKFrontPressure;
  real_T ABS_BRKRearPressure;
  real_T ABS_BRKMasterFrontPressure;
  real_T ABS_BRKMasterRearPressure;
  real_T ABS_Ax;
  real_T ABS_Roll;
  real_T ABS_Pitch;
  real_T ABS_PitchRate;
} ABSdataBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BBSdataBus_
#define DEFINED_TYPEDEF_FOR_BBSdataBus_

typedef struct {
  real_T BBS_Data01_CRC;
  real_T BBS_Data01_CNT;
  real_T BBS_ExvlPot_ERR2;
  real_T BBS_ExvlPot_ERR1;
  real_T BBS_DAVC_STAT;
  real_T BBS_DWC_STAT;
  real_T BBS_DWCLevACK;
  real_T BBS_DTC_STAT;
  real_T BBS_DTCLevACK;
  real_T BBS_RSpeed;
  real_T BBS_RSpeedSTAT;
  real_T BBS_ExvlCheck;
  real_T BBS_ExvlMot_ERR;
  real_T BBS_FSpeedSTAT;
  real_T BBS_FSpeed;
  real_T BBS_Data02_CRC;
  real_T BBS_Data02_CNT;
  real_T BBS_TorqReqRel;
  real_T BBS_TorqReqSTAT;
  real_T BBS_DTCwork;
  real_T BBS_TorqRedFast;
  real_T BBS_TorqRedSlow;
  real_T BBS_TorqReqFast;
  real_T BBS_TorqReqSlow;
} BBSdataBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_GyroBoxBus_
#define DEFINED_TYPEDEF_FOR_GyroBoxBus_

typedef struct {
  real_T GYRO_DriverStatus;
  real_T GYRO_TempMot2;
  real_T GYRO_TempMot1;
  real_T GYRO_DriverErr1;
  real_T GYRO_DriverErr2;
  real_T GYRO_MotorTempErr1;
  real_T GYRO_MotorTempErr2;
  real_T GYRO_MotorVoltErr1;
  real_T GYRO_MotorVoltErr2;
  real_T GYRO_ValveSpeed2;
  real_T GYRO_ValveSpeed1;
  real_T GYRO_ValvePos2;
  real_T GYRO_ValvePos1;
  real_T GYRO_Current2;
  real_T GYRO_Current1;
  real_T GYRO_SpinSpeed2;
  real_T GYRO_SpinSpeed1;
  real_T GYRO_PistonErr;
  real_T GYRO_ValvePerc2;
  real_T GYRO_ValvePerc1;
  real_T GYRO_PistonPress;
} GyroBoxBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_IMUdataBus_
#define DEFINED_TYPEDEF_FOR_IMUdataBus_

typedef struct {
  real_T IMU_YawRate;
  real_T IMU_Ay;
  real_T IMU_RollRate;
  real_T IMU_Ax;
  real_T IMU_Az;
} IMUdataBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DSBdataBus_
#define DEFINED_TYPEDEF_FOR_DSBdataBus_

typedef struct {
  real_T DSB_DTCLevel;
  real_T DSB_Flash;
  real_T DSB_RidingMode;
  real_T DSB_But_IndRes;
} DSBdataBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_InputBus_
#define DEFINED_TYPEDEF_FOR_InputBus_

typedef struct {
  GyroBoxBus GyroBoxBus;
  IMUdataBus IMUdataBus;
  ABSdataBus ABSdataBus;
  DSBdataBus DSBdataBus;
  BBSdataBus BBSdataBus;
} InputBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_OutputBus_
#define DEFINED_TYPEDEF_FOR_OutputBus_

typedef struct {
  InputBus InputBus;
  real_T SpinSpeedRef;
  real_T SpinCurrentRef;
  real_T SpinStartStop;
  real_T SpinModCont;
  real_T TiltModCont;
  real_T TiltStartStop;
  real_T TiltValvePerc1;
  real_T TiltValvePerc2;
  real_T TiltPosRef;
  real_T TiltSpeedRef;
  real_T ComandoPompa;
  real_T TiltPosPerc;
  real_T DTCwork_Status;
} OutputBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_SpinControl_
#define DEFINED_TYPEDEF_FOR_DebugBus_SpinControl_

typedef struct {
  real_T DBG_SpinSpeedRef;
  real_T DBG_SpinCurrentRef;
  real_T DBG_SpinStartStop;
  real_T DBG_SpinModCont;
} DebugBus_SpinControl;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_TiltControl_
#define DEFINED_TYPEDEF_FOR_DebugBus_TiltControl_

typedef struct {
  real_T DBG_ScalingFactor_PI;
  real_T DBG_TiltSpeedRef;
  real_T DBG_TiltPosRef;
  real_T DBG_EnableDaPilota;
  real_T DBG_TiltPercRef1;
  real_T DBG_TiltModCont;
  real_T DBG_TiltStartStop;
  real_T DBG_TiltPercRef2;
  real_T DBG_ComandoPompa;
  real_T DBG_TiltPercRef1_PI;
  real_T DBG_TiltPercRef2_PI;
  real_T DBG_FF1;
  real_T DBG_FF2;
  real_T DBG_TiltSpeedRef1;
  real_T DBG_TiltSpeedRef2;
  real_T DBG_MasterEnable;
} DebugBus_TiltControl;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_SuperVisor_
#define DEFINED_TYPEDEF_FOR_DebugBus_SuperVisor_

typedef struct {
  real_T DBG_EnableTilt;
  real_T DBG_EnableSpin;
} DebugBus_SuperVisor;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_DataProcessing_
#define DEFINED_TYPEDEF_FOR_DebugBus_DataProcessing_

typedef struct {
  real_T DBG_TiltSpeedEstimate1;
  real_T DBG_TiltSpeedEstimate2;
  real_T DBG_RollRate;
  real_T DBG_RollRate_GxGz;
  real_T DBG_Roll;
} DebugBus_DataProcessing;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_VehicleControl_
#define DEFINED_TYPEDEF_FOR_DebugBus_VehicleControl_

typedef struct {
  real_T DBG_TiltSpeedRefH2;
  real_T DBG_ScalingFactor;
  real_T DBG_TiltSpeedRefH1;
  real_T DBG_TiltSpeedLQR61_1;
  real_T DBG_TiltSpeedLQR61_6;
  real_T DBG_TiltSpeedLQR62_1;
  real_T DBG_TiltSpeedLQR62_6;
  real_T DBG_TiltSpeedLQR62_2;
  real_T DBG_TiltSpeedLQR62_3;
  real_T DBG_TiltSpeedLQR62_4;
  real_T DBG_TiltSpeedLQR62_5;
  real_T DBG_TiltSpeed2LQR6;
  real_T DBG_TiltSpeedLQR61_2;
  real_T DBG_TiltSpeedLQR61_3;
  real_T DBG_TiltSpeed1LQR6;
  real_T DBG_TiltSpeedLQR61_4;
  real_T DBG_TiltSpeedLQR61_5;
  real_T DBG_RollSetPoint;
} DebugBus_VehicleControl;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_DashBoard_
#define DEFINED_TYPEDEF_FOR_DebugBus_DashBoard_

typedef struct {
  real_T A;
} DebugBus_DashBoard;

#endif

#ifndef DEFINED_TYPEDEF_FOR_DebugBus_
#define DEFINED_TYPEDEF_FOR_DebugBus_

typedef struct {
  InputBus InputBus;
  DebugBus_SpinControl DebugBus_SpinControl;
  DebugBus_TiltControl DebugBus_TiltControl;
  DebugBus_SuperVisor DebugBus_SuperVisor;
  DebugBus_DataProcessing DebugBus_DataProcessing;
  DebugBus_VehicleControl DebugBus_VehicleControl;
  DebugBus_DashBoard DebugBus_DashBoard;
} DebugBus;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_dZjXwTiylTPloSUBFpnHJB_
#define DEFINED_TYPEDEF_FOR_struct_dZjXwTiylTPloSUBFpnHJB_

typedef struct {
  int32_T InputPortsWidth;
} struct_dZjXwTiylTPloSUBFpnHJB;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_isB4Cw3Ovpp8VfzP6RUqbD_
#define DEFINED_TYPEDEF_FOR_struct_isB4Cw3Ovpp8VfzP6RUqbD_

typedef struct {
  int32_T OutputPortsWidth;
} struct_isB4Cw3Ovpp8VfzP6RUqbD;

#endif

/* Parameters (default storage) */
typedef struct P_Gyro_T_ P_Gyro_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Gyro_T RT_MODEL_Gyro_T;

#endif                                 /* RTW_HEADER_Gyro_types_h_ */
