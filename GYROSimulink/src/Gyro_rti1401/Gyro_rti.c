/*********************** dSPACE target specific file *************************

   Include file Gyro_rti.c:

   Definition of functions and variables for the system I/O and for
   the hardware and software interrupts used.

   RTI1401 7.11 (02-Nov-2018)
   Fri Oct 18 11:47:11 2019

   Copyright 2019, dSPACE GmbH. All rights reserved.

 *****************************************************************************/

#if !(defined(__RTI_SIMENGINE__) || defined(RTIMP_FRAME))
# error This file may be included only by the RTI(-MP) simulation engine.
#endif

/* Include the model header file. */
#include "Gyro.h"
#include "Gyro_private.h"

/* Defines for block output and parameter structure existence */
#define RTI_rtB_STRUCTURE_EXISTS       1
#define RTI_rtP_STRUCTURE_EXISTS       1
#define RTB_STRUCTURE_NAME             Gyro_B
#define RTP_STRUCTURE_NAME             Gyro_P

/* dSPACE generated includes for header files */
#include <brtenv.h>
#include <rtkernel.h>
#include <rti_assert.h>
#include <rtidefineddatatypes.h>
#include <dsflrecusb.h>
#include <rti_msg_access.h>
#include <rtiusbflightrec_msg.h>
#include <rti_sim_engine_exp.h>
#include <rtican_ds1401.h>
#ifndef dsRtmGetNumSampleTimes
# define dsRtmGetNumSampleTimes(rtm)   1
#endif

#ifndef dsRtmSetTaskTime
#define dsRtmSetTaskTime(rtm, sti, val) (((rtm)->Timing.taskTime0) = (val))
#endif

/****** Definitions: task functions for timer tasks *********************/

/* Timer Task 1. (Base rate). */
static void rti_TIMERA(rtk_p_task_control_block task)
{
  /* Task entry code BEGIN */
  /* -- None. -- */
  /* Task entry code END */

  /* Task code. */
  baseRateService(task);

  /* Task exit code BEGIN */
  /* -- None. -- */
  /* Task exit code END */
}

/* ===== Declarations of RTI blocks ======================================== */

/* dSPACE I/O Board DSUSBFLIGHTREC #1 */
UInt16 GYRO_DriverStatus_Index_UsbFlRec;
UInt16 GYRO_ValveSpeed2_Index_UsbFlRec;
UInt16 GYRO_ValveSpeed1_Index_UsbFlRec;
UInt16 GYRO_ValvePos2_Index_UsbFlRec;
UInt16 GYRO_ValvePos1_Index_UsbFlRec;
UInt16 GYRO_Current2_Index_UsbFlRec;
UInt16 GYRO_Current1_Index_UsbFlRec;
UInt16 GYRO_SpinSpeed2_Index_UsbFlRec;
UInt16 GYRO_SpinSpeed1_Index_UsbFlRec;
UInt16 GYRO_PistonErr_Index_UsbFlRec;
UInt16 GYRO_ValvePerc2_Index_UsbFlRec;
UInt16 GYRO_TempMot2_Index_UsbFlRec;
UInt16 GYRO_ValvePerc1_Index_UsbFlRec;
UInt16 GYRO_PistonPress_Index_UsbFlRec;
UInt16 IMU_YawRate_Index_UsbFlRec;
UInt16 IMU_Ay_Index_UsbFlRec;
UInt16 IMU_RollRate_Index_UsbFlRec;
UInt16 IMU_Ax_Index_UsbFlRec;
UInt16 IMU_Az_Index_UsbFlRec;
UInt16 ABS_Roll_Index_UsbFlRec;
UInt16 DSB_DTCLevel_Index_UsbFlRec;
UInt16 DSB_Flash_Index_UsbFlRec;
UInt16 GYRO_TempMot1_Index_UsbFlRec;
UInt16 DSB_RidingMode_Index_UsbFlRec;
UInt16 DBG_SpinSpeedRef_Index_UsbFlRec;
UInt16 DBG_SpinCurrentRef_Index_UsbFlRec;
UInt16 DBG_SpinStartStop_Index_UsbFlRec;
UInt16 DBG_SpinModCont_Index_UsbFlRec;
UInt16 DBG_TiltSpeedRef_Index_UsbFlRec;
UInt16 DBG_TiltPosRef_Index_UsbFlRec;
UInt16 DBG_TiltModCont_Index_UsbFlRec;
UInt16 DBG_TiltStartStop_Index_UsbFlRec;
UInt16 DBG_EnableTilt_Index_UsbFlRec;
UInt16 GYRO_DriverErr1_Index_UsbFlRec;
UInt16 DBG_EnableSpin_Index_UsbFlRec;
UInt16 DBG_RollRate_Index_UsbFlRec;
UInt16 DBG_RollRate_GxGz_Index_UsbFlRec;
UInt16 ABS_Pitch_Index_UsbFlRec;
UInt16 ABS_PitchRate_Index_UsbFlRec;
UInt16 DBG_TiltSpeedEstimate1_Index_UsbFlRec;
UInt16 DBG_TiltSpeedEstimate2_Index_UsbFlRec;
UInt16 DBG_TiltPercRef1_PI_Index_UsbFlRec;
UInt16 DBG_TiltPercRef2_PI_Index_UsbFlRec;
UInt16 DBG_TiltPercRef1_Index_UsbFlRec;
UInt16 GYRO_DriverErr2_Index_UsbFlRec;
UInt16 DBG_TiltPercRef2_Index_UsbFlRec;
UInt16 DBG_ComandoPompa_Index_UsbFlRec;
UInt16 DBG_ScalingFactor_PI_Index_UsbFlRec;
UInt16 DBG_TiltSpeedRef1_Index_UsbFlRec;
UInt16 DBG_TiltSpeedRef2_Index_UsbFlRec;
UInt16 DBG_FF1_Index_UsbFlRec;
UInt16 DBG_FF2_Index_UsbFlRec;
UInt16 DBG_Roll_Index_UsbFlRec;
UInt16 DBG_TiltSpeedRefH2_Index_UsbFlRec;
UInt16 DBG_ScalingFactor_Index_UsbFlRec;
UInt16 GYRO_MotorTempErr1_Index_UsbFlRec;
UInt16 DBG_TiltSpeedRefH1_Index_UsbFlRec;
UInt16 DBG_RollSetPoint_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_1_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_6_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_1_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_6_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_2_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_3_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_4_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR62_5_Index_UsbFlRec;
UInt16 GYRO_MotorTempErr2_Index_UsbFlRec;
UInt16 DBG_TiltSpeed2LQR6_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_2_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_3_Index_UsbFlRec;
UInt16 DBG_TiltSpeed1LQR6_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_4_Index_UsbFlRec;
UInt16 DBG_TiltSpeedLQR61_5_Index_UsbFlRec;
UInt16 BBS_RSpeed_Index_UsbFlRec;
UInt16 DBG_EnableDaPilota_Index_UsbFlRec;
UInt16 DBG_MasterEnable_Index_UsbFlRec;
UInt16 GYRO_MotorVoltErr1_Index_UsbFlRec;
UInt16 GYRO_MotorVoltErr2_Index_UsbFlRec;

/* dSPACE I/O Board DS1_RTICAN #1 */

/* ...  definition of channel struct */
can_tp1_canChannel* can_type1_channel_M1_C1;

/* ...  definition of channel struct */
can_tp1_canChannel* can_type1_channel_M1_C2;

/* declare pointers to CAN message structures for support of TX-Custom code */
can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C1_STD;
can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C1_XTD;

/* declare pointers to CAN message structures for support of TX-Custom code */
can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C2_STD;
can_tp1_canMsg* CANTP1_TX_SPMSG_M1_C2_XTD;

/* ... definition of message variable for the RTICAN blocks */
can_tp1_canMsg* can_type1_msg_M1[CANTP1_M1_NUMMSG];

/* dSPACE I/O Board RTICAN_GLOBAL #0 */

/* ===== Definition of interface functions for simulation engine =========== */
#if GRTINTERFACE == 1
#ifdef MULTITASKING
# define dsIsSampleHit(RTM,sti)        rtmGetSampleHitPtr(RTM)[sti]
#else
# define dsIsSampleHit(RTM,sti)        1
#endif

#else
#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

# define dsIsSampleHit(RTM,sti)        rtmStepTask(RTM, sti)
#endif

#undef __INLINE
#if defined(_INLINE)
# define __INLINE                      static inline
#else
# define __INLINE                      static
#endif

/*Define additional variables*/
static time_T dsTFinal = -1.0;

#define dsGetTFinal(rtm)               (dsTFinal)

static time_T dsStepSize = 0.001;

# define dsGetStepSize(rtm)            (dsStepSize)

static void rti_mdl_initialize_host_services(void)
{
  ts_timestamp_type ts = { 0, 0 };

  host_service(1, &ts);
  DsDaq_Init(0, 32, 1);
}

static void rti_mdl_initialize_io_boards(void)
{
  /* Registering of RTI products and modules at VCM */
  {
    vcm_module_register(VCM_MID_RTI1401, (void *) 0,
                        VCM_TXT_RTI1401, 7, 11, 0,
                        VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);

    {
      vcm_module_descriptor_type* msg_mod_ptr;
      msg_mod_ptr = vcm_module_register(VCM_MID_MATLAB, (void *) 0,
        VCM_TXT_MATLAB, 9, 5, 0,
        VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
      vcm_module_register(VCM_MID_SIMULINK, msg_mod_ptr,
                          VCM_TXT_SIMULINK, 9, 2, 0,
                          VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
      vcm_module_register(VCM_MID_RTW, msg_mod_ptr,
                          VCM_TXT_RTW, 9, 0, 0,
                          VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
      vcm_module_register(VCM_MID_STATEFLOW, msg_mod_ptr,
                          VCM_TXT_STATEFLOW, 9, 2, 0,
                          VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
      vcm_module_register(VCM_MID_STATEFLOW_CODER, msg_mod_ptr,
                          VCM_TXT_STATEFLOW_CODER, 9, 0, 0,
                          VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
    }

    vcm_module_register(VCM_MID_RTICAN, (void *) 0,
                        VCM_TXT_RTICAN, 3, 4, 7,
                        VCM_VERSION_RELEASE, 0, 0, 0, VCM_CTRL_NO_ST);
  }

  /* dSPACE I/O Board DSUSBFLIGHTREC #1 */
  {
    /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:SETUP */
    /* This part of code has been generated from the block: */
    /* USB device flight recorder initialization function */

    /* if not already done, initialize the USB device flight recorder */
    dsflrec_usb_initialize(1, RTI_USB_FLREC_DEFAULT_BLOCK_SIZE);
  }

  /* dSPACE I/O Board DSUSBFLIGHTREC #1 Unit:WRITE */
  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_DriverStatus_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_DriverStatus_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValveSpeed2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValveSpeed2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValveSpeed1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValveSpeed1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValvePos2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValvePos2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValvePos1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValvePos1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_Current2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_Current2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_Current1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_Current1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_SpinSpeed2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_SpinSpeed2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_SpinSpeed1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_SpinSpeed1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_PistonErr_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_PistonErr_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValvePerc2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValvePerc2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_TempMot2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_TempMot2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_ValvePerc1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_ValvePerc1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_PistonPress_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_PistonPress_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "IMU_YawRate_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&IMU_YawRate_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "IMU_Ay_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&IMU_Ay_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "IMU_RollRate_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&IMU_RollRate_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "IMU_Ax_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&IMU_Ax_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "IMU_Az_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&IMU_Az_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "ABS_Roll_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&ABS_Roll_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DSB_DTCLevel_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DSB_DTCLevel_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DSB_Flash_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DSB_Flash_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_TempMot1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_TempMot1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DSB_RidingMode_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DSB_RidingMode_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_SpinSpeedRef_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_SpinSpeedRef_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_SpinCurrentRef_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_SpinCurrentRef_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_SpinStartStop_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_SpinStartStop_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_SpinModCont_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_SpinModCont_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedRef_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedRef_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltPosRef_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltPosRef_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltModCont_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltModCont_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltStartStop_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltStartStop_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_EnableTilt_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_EnableTilt_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_DriverErr1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_DriverErr1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_EnableSpin_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_EnableSpin_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_RollRate_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_RollRate_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_RollRate_GxGz_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_RollRate_GxGz_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "ABS_Pitch_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&ABS_Pitch_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "ABS_PitchRate_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&ABS_PitchRate_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedEstimate1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedEstimate1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedEstimate2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedEstimate2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltPercRef1_PI_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltPercRef1_PI_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltPercRef2_PI_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltPercRef2_PI_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltPercRef1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltPercRef1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_DriverErr2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_DriverErr2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltPercRef2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltPercRef2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_ComandoPompa_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_ComandoPompa_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_ScalingFactor_PI_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_ScalingFactor_PI_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedRef1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedRef1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedRef2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedRef2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_FF1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_FF1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_FF2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_FF2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_Roll_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_Roll_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedRefH2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedRefH2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_ScalingFactor_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_ScalingFactor_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_MotorTempErr1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_MotorTempErr1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedRefH1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedRefH1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_RollSetPoint_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_RollSetPoint_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_6_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_6_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_6_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_6_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_3_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_3_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_4_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_4_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR62_5_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR62_5_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_MotorTempErr2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_MotorTempErr2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeed2LQR6_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeed2LQR6_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_3_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_3_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeed1LQR6_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeed1LQR6_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_4_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_4_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_TiltSpeedLQR61_5_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_TiltSpeedLQR61_5_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "BBS_RSpeed_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&BBS_RSpeed_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_EnableDaPilota_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_EnableDaPilota_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "DBG_MasterEnable_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&DBG_MasterEnable_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_MotorVoltErr1_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_MotorVoltErr1_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  {
    /* dSPACE USB FLIGHT RECORDER WRITE */
    Int16 errorCode = 0;

    /* ----------------------------------------------------------------------  */
    /* |  prototype function                                                 | */
    /* ----------------------------------------------------------------------  */
    /* | Int16  dsflrec_usb_add_variable (char       *VarName,               | */
    /* |                                  UInt16    DataTypeId,              | */
    /* |                                  UInt16    *VarIndex);              | */
    /* ----------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------- */
    char variable_name[]= "GYRO_MotorVoltErr2_UsbFlRec";
    errorCode = dsflrec_usb_add_variable(&variable_name[0],
      DSFLREC_DATA_TYPE_FLOAT32,&GYRO_MotorVoltErr2_Index_UsbFlRec);

#ifdef DEBUG_INIT

    if (errorCode == DSFLREC_USB_VAR_ALREADY_EXIST)
      rti_msg_info_set(RTI_USB_FLREC_VARIABLE_ERROR);

#endif

    if (errorCode == DSFLREC_USB_INIT_TABLE_FULL)
      rti_msg_error_set(RTI_USB_FLREC_INIT_TABLE_ERROR);
  }

  /* dSPACE I/O Board DS1401BASEII #1 Unit:REMOTEIN */
  ds1401_remote_in_init();

  /* dSPACE I/O Board DS1_RTICAN #1 */
  /* Initialization of DS1501 board */
  can_tp1_communication_init(can_tp1_address_table[0].module_addr,
    CAN_TP1_INT_ENABLE);

  /* dSPACE RTICAN MASTER SETUP Block */
  /* ... Initialize the CAN communication: 500 kbit/s */
  can_type1_channel_M1_C1 = can_tp1_channel_init(can_tp1_address_table[0].
    module_addr, 0, (500 * 1000), CAN_TP1_STD, CAN_TP1_NO_SUBINT);
  can_tp1_channel_termination_set(can_type1_channel_M1_C1,
    CAN_TP1_TERMINATION_OFF);

  /* ... Initialize TX message structs for custom code  */
  CANTP1_TX_SPMSG_M1_C1_STD = can_tp1_msg_tx_register(can_type1_channel_M1_C1, 3,
    1050, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO | CAN_TP1_DELAYCOUNT_INFO |
    CAN_TP1_MSG_INFO, CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG,
    CAN_TP1_TIMEOUT_NORMAL);
  CANTP1_TX_SPMSG_M1_C1_XTD = can_tp1_msg_tx_register(can_type1_channel_M1_C1, 3,
    1100, CAN_TP1_EXT, CAN_TP1_TIMECOUNT_INFO | CAN_TP1_DELAYCOUNT_INFO |
    CAN_TP1_MSG_INFO, CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG,
    CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN MASTER SETUP Block */
  /* ... Initialize the Partial Networking Settings */

  /* dSPACE RTICAN TX Message Block: "BBS_Data01" Id:24 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18] = can_tp1_msg_tx_register
    (can_type1_channel_M1_C1, 0, 24, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO,
     CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG, CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN TX Message Block: "BBS_Data02" Id:25 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19] = can_tp1_msg_tx_register
    (can_type1_channel_M1_C1, 0, 25, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO,
     CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG, CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN RX Message Block: "IMU_Data01" Id:372 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 2, 372, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "IMU_Data02" Id:376 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 2, 376, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "IMU_Data03" Id:380 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 2, 380, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "ABS_Data01" Id:34 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 0, 34, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "ABS_Data02" Id:35 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 1, 35, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "ABS_Data03" Id:392 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 3, 392, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "DSB_Data01" Id:32 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 0, 32, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "BBS_Data01_Copy" Id:280 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 1, 280, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "BBS_Data02_Copy" Id:281 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 1, 281, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "DSB_Data02" Id:768 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 5, 768, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "DSB_Data03" Id:560 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C1, 4, 560, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN MASTER SETUP Block */
  /* ... Initialize the CAN communication: 500 kbit/s */
  can_type1_channel_M1_C2 = can_tp1_channel_init(can_tp1_address_table[0].
    module_addr, 1, (500 * 1000), CAN_TP1_STD, CAN_TP1_NO_SUBINT);
  can_tp1_channel_termination_set(can_type1_channel_M1_C2,
    CAN_TP1_TERMINATION_OFF);

  /* ... Initialize TX message structs for custom code  */
  CANTP1_TX_SPMSG_M1_C2_STD = can_tp1_msg_tx_register(can_type1_channel_M1_C2, 3,
    1050, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO | CAN_TP1_DELAYCOUNT_INFO |
    CAN_TP1_MSG_INFO, CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG,
    CAN_TP1_TIMEOUT_NORMAL);
  CANTP1_TX_SPMSG_M1_C2_XTD = can_tp1_msg_tx_register(can_type1_channel_M1_C2, 3,
    1100, CAN_TP1_EXT, CAN_TP1_TIMECOUNT_INFO | CAN_TP1_DELAYCOUNT_INFO |
    CAN_TP1_MSG_INFO, CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG,
    CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN MASTER SETUP Block */
  /* ... Initialize the Partial Networking Settings */

  /* dSPACE RTICAN TX Message Block: "AME_Data05" Id:688 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0] = can_tp1_msg_tx_register
    (can_type1_channel_M1_C2, 5, 688, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO,
     CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG, CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN TX Message Block: "AME_Data06" Id:689 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1] = can_tp1_msg_tx_register
    (can_type1_channel_M1_C2, 5, 689, CAN_TP1_STD, CAN_TP1_TIMECOUNT_INFO,
     CAN_TP1_NO_SUBINT, 0, CAN_TP1_TRIGGER_MSG, CAN_TP1_TIMEOUT_NORMAL);

  /* dSPACE RTICAN RX Message Block: "PLC_Data01" Id:432 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C2, 3, 432, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "PLC_Data03" Id:434 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C2, 4, 434, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "PLC_Data02" Id:433 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C2, 3, 433, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE RTICAN RX Message Block: "PLC_Data04" Id:435 */
  /* ... Register message */
  can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3] = can_tp1_msg_rx_register
    (can_type1_channel_M1_C2, 4, 435, CAN_TP1_STD, (CAN_TP1_DATA_INFO|
      CAN_TP1_TIMECOUNT_INFO), CAN_TP1_NO_SUBINT);

  /* dSPACE I/O Board RTICAN_GLOBAL #0 */

  /* Optional initialization of DS4121 systems */
  /* Level 1: Global initialization of RPCU systems */
  ds1401_power_hold_on();              /* MABX ignore KL15 signal */

  /* Level 2: Initialization of RPCU timer units    */
  /* Level 2: Initialization of RPCU DASM units     */
  /* Level 3: Initialization of RPCU IO units       */

  /* Level 4: Start of RPCU IO units                */
}

/* Function rti_mdl_slave_load() is empty */
#define rti_mdl_slave_load()

/* Function rti_mdl_rtk_initialize() is empty */
#define rti_mdl_rtk_initialize()

static void rti_mdl_initialize_io_units(void)
{
  /* dSPACE I/O Board DS1_RTICAN #1 */
  /* Start CAN controller */
  can_tp1_channel_start(can_type1_channel_M1_C1, CAN_TP1_INT_DISABLE);

  /* Start CAN controller */
  can_tp1_channel_start(can_type1_channel_M1_C2, CAN_TP1_INT_DISABLE);

  /* Set the type1CAN error level */
  rtican_type1_error_level = 0;

  /* ... Reset all taskqueue-specific error variables */
  rtican_type1_tq_err_all_reset(0);

  /* ... Clear all message data buffers */
  can_tp1_all_data_clear(can_tp1_address_table[0].module_addr);

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][0] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X18])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][0] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_TX_STD_0X19])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][2] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X174])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][2] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X178])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][2] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X17C])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][0] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X22])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][1] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X23])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][3] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X188])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][0] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X20])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][1] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X118])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][1] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X119])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][5] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X300])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][4] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C1_RX_STD_0X230])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][5] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B0])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][5] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_TX_STD_0X2B1])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][3] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B0])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][4] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B2])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][3] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B1])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }

  {
    static UInt32 numInit = 0;
    if (numInit != 0) {
      /* ... Wake message up */
      while ((rtican_type1_tq_error[0][4] = can_tp1_msg_wakeup
              (can_type1_msg_M1[CANTP1_M1_C2_RX_STD_0X1B3])) ==
             DSMCOM_BUFFER_OVERFLOW) ;
    }

    ++numInit;
  }
}

/* Function rti_mdl_acknowledge_interrupts() is empty */
#define rti_mdl_acknowledge_interrupts()

/* Function rti_mdl_timetables_register() is empty */
#define rti_mdl_timetables_register()

/* Function rti_mdl_timesync_simstate() is empty */
#define rti_mdl_timesync_simstate()

static void rti_mdl_background(void)
{
  /* DsDaq background call */
  DsDaq_Background(0);

  /* dSPACE I/O Board DS1_RTICAN #1 */
  {
    real_T bg_code_exec_time;
    static real_T bg_code_last_exec_time = 0.0;
    bg_code_exec_time = RTLIB_TIC_READ();
    if ((bg_code_exec_time - bg_code_last_exec_time) > 0.25 ||
        (bg_code_exec_time - bg_code_last_exec_time) < 0) {
      /* ... Check taskqueue-specific error variables */
      rtican_type1_tq_err_all_chk(can_tp1_address_table[0].module_addr, 0);
      bg_code_last_exec_time = bg_code_exec_time;
    }
  }

  /* copy DPMEM - buffers in background */
  {
    /* call update function for CAN Tp1 CAN interface (module number: 1) */
    can_tp1_msg_copy_all_to_mem(can_tp1_address_table[0].module_addr);
  }
}

__INLINE void rti_mdl_sample_input(void)
{
  /* Calls for base sample time: [0.001, 0.0] */
  /* dSPACE I/O Board DS1401BASEII #1 Unit:REMOTEIN */
  {
    UInt16 inputValue = ds1401_remote_in_read();
    if (SYS1401_REMOTE_IN_CONNECTED == inputValue) {
      Gyro_B.SFunction1_o1_am = (boolean_T) (SYS1401_REMOTE_IN_CONNECTED);
      Gyro_B.SFunction1_o2_cg = (boolean_T) (SYS1401_REMOTE_IN_NOT_CONNECTED);
    } else {
      Gyro_B.SFunction1_o1_am = (boolean_T) (SYS1401_REMOTE_IN_NOT_CONNECTED);
      Gyro_B.SFunction1_o2_cg = (boolean_T) (SYS1401_REMOTE_IN_CONNECTED);
    }
  }

  /* dSPACE I/O Board DS1_RTICAN #1 Unit:DEFAULT */
  /* call update function for CAN Tp1 CAN interface (module number: 1) */
  can_tp1_msg_copy_all_to_mem(can_tp1_address_table[0].module_addr);
}

static void rti_mdl_daq_service()
{
  /* dSPACE Host Service */
  host_service(1, rtk_current_task_absolute_time_ptr_get());
  DsDaq_Service(0, 0, 1, (DsDaqSTimestampStruct *)
                rtk_current_task_absolute_time_ptr_get());
}

#undef __INLINE

/****** [EOF] ****************************************************************/
