function init_VehicleControl()
%% Init
BlockName = mfilename;
BlockName = BlockName(min(strfind(BlockName,'_'))+1:end);

%% Parameters in excel file
read_param_excel_file('GyroParameters.xlsx',BlockName)

%% Parameters from command line
% - derivator parameters
% [num_der,den_der]=discrete_derivator(Td_PD,N_PD);

% lowpass 0.1HZ
wn=2*pi*0.1;
damp=0.7;
Ts=0.001;
s = tf('s');
G=1/(1+s*2*damp/wn+s^2/wn^2);
G = c2d(G,Ts,'tustin');
LP01_num = G.num{1};
LP01_den = G.den{1};
clearvars G wn damp s Ts

%% Send parameters to base workspace
s = who;
s = s(~ismember(s,'cbinfo')); 
s = s(~ismember(s,'BlockName'));
arrayfun(@(x)assign_and_check_param(x{:},evalin('caller',x{:}),'chk'),s);
clear s

%% Model check
model_ref_check_standalone(BlockName)