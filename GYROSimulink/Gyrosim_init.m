% clear all
% clear all
% clc

% Usally "clc, clear all, close all" commands are not included in the simulation init
% so that it can be called from another function

%% Parameters in excel file
read_param_excel_file('GyroParameters.xlsx','Gyro')

%% Parameters from command line
tend=t(end);
dt=0.001;
Ts=0.001;
ABS_SPDFrontTime=zeros(length(t),1);
ABS_SPDRearTime=zeros(length(t),1);
ABS_BRKFrontPressure=zeros(length(t),1);
ABS_BRKRearPressure=zeros(length(t),1);
ABS_BRKMasterFrontPressure=zeros(length(t),1);
ABS_BRKMasterRearPressure=zeros(length(t),1);
%% Init & check framework
framework_init('Gyrosim.slx','ConfigSetObj_simulation.mat')

%% Display save workspace
disp('================= SAVE WORKSPACE!!!! ===========================')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LIST OF REQUIRED INPUT VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	ABS_SPDFrontTime
%	ABS_SPDRearTime
%	ABS_BRKFrontPressure
%	ABS_BRKRearPressure
%	ABS_BRKMasterFrontPressure
%	ABS_BRKMasterRearPressure
%	ABS_Ax
%	ABS_Roll
%	ABS_Pitch
%	ABS_PitchRate
%	DSB_DTCStatus
%	DSB_DTCLevel
%	DSB_Flash
%	DSB_RidingMode
%	GYRO_DriverStatus
%	GYRO_TempMot2
%	GYRO_TempMot1
%	GYRO_DriverErr1
%	GYRO_DriverErr2
%	GYRO_MotorTempErr1
%	GYRO_MotorTempErr2
%	GYRO_MotorVoltErr1
%	GYRO_MotorVoltErr2
%	GYRO_ValveSpeed2
%	GYRO_ValveSpeed1
%	GYRO_ValvePos2
%	GYRO_ValvePos1
%	GYRO_Current2
%	GYRO_Current1
%	GYRO_SpinSpeed2
%	GYRO_SpinSpeed1
%	GYRO_PistonErr
%	GYRO_ValvePerc2
%	GYRO_ValvePerc1
%	GYRO_PistonPress
%	IMU_YawRate
%	IMU_Ay
%	IMU_RollRate
%	IMU_Ax
%	IMU_Az
