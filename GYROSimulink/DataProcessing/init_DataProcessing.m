function init_DataProcessing()
%% Init
BlockName = mfilename;
BlockName = BlockName(min(strfind(BlockName,'_'))+1:end);

%% Parameters in excel file
read_param_excel_file('GyroParameters.xlsx',BlockName)

%% Parameters from command line
[num_der_Roll,den_der_Roll]=discrete_derivator_w1(w_dRoll);
%notch filter
Ts=0.001;
wn=2*pi*7;
damp=0.6;
s=tf('s');
G=(s^2+wn^2)/(s^2+2*damp*wn*s+wn^2);
G = c2d(G,Ts,'tustin');
num_Notch = G.num{1};
den_Notch = G.den{1};
clearvars G wn damp s Ts
% filtro derivativo per calcolo velocit� tilt
s=tf('s');
damp=0.6;
wn=2*pi*15;
dt=0.001;
G_der=s/(1+s*2*damp/wn+s^2/wn^2);
der_tilt = c2d(G_der,dt,'tustin');
num_der_Tilt = cell2mat(der_tilt.num);
den_der_Tilt = cell2mat(der_tilt.den);
clearvars G_der der_tilt dt wn damp s

% lowpass 20HZ
wn=2*pi*20;
damp=0.7;
Ts=0.001;
s = tf('s');
G=1/(1+s*2*damp/wn+s^2/wn^2);
G = c2d(G,Ts,'tustin');
LP20_num = G.num{1};
LP20_den = G.den{1};
clearvars G wn damp s Ts


% notch filter

notch_fmax = 45;                % [Hz] Attenzione al tempo di campionamento e nyquist
notch_fmin = 5;                 % [Hz]

% Mappa di correzione frequenza notch. Attenzione va rigenerata ogni cambio
% di Ts!!
notch_win_correction_map = 2*pi*linspace(notch_fmin,notch_fmax,30);
Ts=0.001;
switch Ts
    case{0.001}
        notch_wout_correction_map = [31.5417    40.2429     48.9440     57.6452     66.3464 ...
                                     75.0475    83.7487     92.4499     101.1510    109.8522 ...
                                     118.5533   127.2545    135.9557    144.6568    153.3580 ...
                                     162.0592   170.7603    179.4615    188.1627    196.8638 ...
                                     205.5650   214.2661    222.9673    231.6685    240.3696 ...
                                     249.0708   257.7720    266.4731    275.1743    283.8754];                              
    case{0.005}
        notch_wout_correction_map = [31.5417    40.2429     48.9440     57.6452     66.9748 ...
                                     75.7584	84.5419     93.3255     103.0671    111.9331 ...
                                     121.9220   130.8704    141.1065    150.1374    160.6207 ...
                                     171.2689   180.4646    191.3600    202.4202    213.6453 ...
                                     225.0352   236.5900    248.3095    260.1939    272.2432 ...
                                     286.8163   299.2776    311.9037    327.3010    340.3392];                           
    case{0.01}
        notch_wout_correction_map = [31.542     40.624      49.871      59.283      68.86 ...
                                     78.602     88.508      99.455      109.77      121.3 ...
                                     134.27     146.54      160.42      176.17      191.12 ...
                                     208.11     227.37      249.15      271.93      297.55 ...
                                     328.23     362.42      402.47      451.1       509.02 ...
                                     584.06     677.71      804.07      984.1       1262.6];                     
    otherwise
        notch_wout_correction_map = [31.542     40.624      49.871      59.283      68.86 ...
                                     78.602     88.508      99.455      109.77      121.3 ...
                                     134.27     146.54      160.42      176.17      191.12 ...
                                     208.11     227.37      249.15      271.93      297.55 ...
                                     328.23     362.42      402.47      451.1       509.02 ...
                                     584.06     677.71      804.07      984.1       1262.6]; 
        warning('La mappa per la correzione del filtro notch � stata calcolata per Ts = 0.01, ma il Ts sembra essere differente')
end

%% Send parameters to base workspace
s = who;
s = s(~ismember(s,'cbinfo')); 
s = s(~ismember(s,'BlockName'));
arrayfun(@(x)assign_and_check_param(x{:},evalin('caller',x{:}),'chk'),s);
clear s

%% Model check
model_ref_check_standalone(BlockName)