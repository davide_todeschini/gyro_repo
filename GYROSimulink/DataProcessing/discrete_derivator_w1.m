function [num_der,den_der]=discrete_derivator_w1(w_polo)
%%%%%%%%%%%%%%%%%%%%%  regolatore posizione %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=0.001;
%discretizzazione blocco per blocco 
der_d = c2d(tf([1/w_polo^2+1 0], [1/w_polo 1]),dt,'tustin');

num_der = cell2mat(der_d.num);
den_der = cell2mat(der_d.den);
