function init_DashBoard()
%% Init
BlockName = mfilename;
BlockName = BlockName(min(strfind(BlockName,'_'))+1:end);

%% Parameters in excel file
read_param_excel_file('GyroParameters.xlsx',BlockName)

%% Parameters from command line

%% Send parameters to base workspace
s = who;
s = s(~ismember(s,'cbinfo')); 
s = s(~ismember(s,'BlockName'));
arrayfun(@(x)assign_and_check_param(x{:},evalin('caller',x{:}),'chk'),s);
clear s

%% Model check
model_ref_check_standalone(BlockName)