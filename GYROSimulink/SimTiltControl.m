clc
clear
close all	
%% assign value to input of block
% load('D:\OneDrive - Politecnico di Milano\universita\progetti\Gyro\GyroDucati\data\20190613_validazioneControlloVelocitÓ\3_LQRProva2\20190614_provaLQR2__part5');
load('D:\OneDrive - Politecnico di Milano\universita\progetti\Gyro\GyroDucati\data\20190613_validazioneControlloVelocitÓ\4_LQR_splittato\20190619_LQR_split__part1');
addpath('TiltControl')
run('TiltControlsim_init.m')

ValveSpeed1=GYRO_ValveSpeed1;
ValveSpeed2=GYRO_ValveSpeed2;
ValvePos1=GYRO_ValvePos1;
ValvePos2=GYRO_ValvePos2;
PistonPressure=GYRO_PistonPress;
Roll=ABS_Roll*pi/180;
RollRate=DBG_RollRate;
SpinSpeed1=GYRO_SpinSpeed1;
SpinSpeed2=GYRO_SpinSpeed2;

%% simulation
sim('TiltControlsim')

%% plot
close all

FigureColors
SpeedFilt1=filtrofiltro_lp(GYRO_ValveSpeed1,10,0.001);
SpeedFilt2=filtrofiltro_lp(GYRO_ValveSpeed2,10,0.001);
f(1)=figure;
    ax(1)=subplot(2,3,1);
        plot(t,DBG_RollSetPoint*180/pi)
        hold on
        plot(t,ABS_Roll)
        ylabel('Roll [deg]')
    ax(2)=subplot(2,3,2);
        plot(t,DBG_RollRate_GxGz)
        ylabel('RollRate [deg/s]')
    ax(3)=subplot(2,3,3);
        plot(t,DBG_SpinSpeedRef,'--k');
        plot(t,-GYRO_SpinSpeed1);
        hold on
        plot(t,GYRO_SpinSpeed2);
        ylabel('SpinSpeed [rpm]')
        ylim([0 20000])
    ax(4)=subplot(2,3,4);
        plot(t,DSB_Flash,'--k');
%         plot(t,-GYRO_SpinSpeed1);
%         hold on
%         plot(t,GYRO_SpinSpeed2);
        ylabel('Flash []')
linkaxes(ax,'x')
% plot only gyro quantities

    f(2)=figure('Name','Tilt');
    ax1(3)=subplot(2,3,3);
        plot(t,GYRO_PistonPress)
        ylim([0 100])
        ylabel('Pressure [Bar]')
    ax1(4)=subplot(2,3,4);   
        plot(t,DBG_TiltSpeedRef1,'DisplayName','TiltSpeed1 Ref','LineStyle','--','Color',col(1,:))
        hold on
%         plot(t,DBG_TiltSpeedRef2,'DisplayName','TiltSpeed2 Ref','LineStyle','--','Color',col(2,:))
        plot(SimSaveData.DBG_TiltSpeedRef1,'DisplayName','TiltSpeed1 sim','LineStyle','-','Color',col(1,:))
%         plot(SimSaveData.DBG_TiltSpeedRef2,'DisplayName','TiltSpeed 2','LineStyle','-','Color',col(2,:))
        ylabel('TiltSpeed [deg/s]')
        ylim([-2 2])
        legend
    ax1(5)=subplot(2,3,5);
        plot(t,GYRO_ValvePos1)
        hold on
        plot(t,GYRO_ValvePos2)
        ylabel('TiltPos [rad]')
        ylim([-0.7 0.7])
    ax1(6)=subplot(2,3,6);
        plot(t,DBG_TiltPercRef1,'LineStyle','--','Color',col(1,:),'DisplayName','Tiltperc1 Ref')
        hold on
        plot(t,DBG_TiltPercRef2,'LineStyle','--','Color',col(2,:))
        plot(SimSaveData.DBG_TiltPercRef1,'LineStyle','-','Color',col(1,:),'DisplayName','Tiltperc1 sim')
        plot(SimSaveData.DBG_TiltPercRef2,'LineStyle','-','Color',col(2,:))
        ylabel('Valve Opening [%]')
        ylim([-100 100])
        legend
linkaxes(ax1,'x')
moveFont(-2,f,'grid','on','NRows',4,'NCols',1)
moveArrangeOnScreen('NRows',1)
linkfigaxes