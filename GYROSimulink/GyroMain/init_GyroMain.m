function init_GyroMain()

%% Init
BlockName = mfilename;
BlockName = BlockName(min(strfind(BlockName,'_'))+1:end);

%% Parameters in excel file
read_param_excel_file('ExampleParameters.xlsx',BlockName)

%% Parameters from command line                      
  
%% Send to base workspace
s = who;
s = s(~ismember(s,'cbinfo'));
s = s(~ismember(s,'BlockName'));
arrayfun(@(x) assignin('base',x{:},evalin('caller',x{:})),s)
clear s

%% Model check
main_check_standalone(BlockName)