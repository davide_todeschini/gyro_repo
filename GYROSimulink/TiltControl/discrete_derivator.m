function [num_der,den_der]=discrete_derivator(Td,N)
%%%%%%%%%%%%%%%%%%%%%  regolatore posizione %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=0.001;
%discretizzazione blocco per blocco 
der_d = c2d(tf([Td 0], [Td/N 1]),dt,'tustin');

num_der = cell2mat(der_d.num);
den_der = cell2mat(der_d.den);
