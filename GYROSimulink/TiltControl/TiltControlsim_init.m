% clc
% clear all
% close all

%% Load configuration data (usually ConfigSetObj_simulation.mat)
load('D:\GYRO_Repo\GYROSimulink\configuration files\ConfigSetObj_simulation.mat');
%% Load simulation signals (typically, model inputs)
%% Init model
run('init_TiltControl.m')

%% Custom parameters

%% Simulation ("tend" and "dt" to be defined!)
tend=t(end);
Ts=0.001;
dt=0.001;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LIST OF REQUIRED INPUT VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
