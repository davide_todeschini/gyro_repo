close all
clear all
clc

%% Parameters in excel file
read_param_excel_file('GyroParameters.xlsx','Gyro')

%% Parameters from command line

%% Init & check framework
framework_init('Gyro.slx','ConfigSetObj_dSpace.mat')

%% Display save workspace
disp('================= SAVE WORKSPACE!!!! ===========================')
